import unittest

from apscheduler.triggers.cron import CronTrigger

from sjtoolkit.utils.utils import parse_cron


class TestUtils(unittest.TestCase):
    def test_parse_cron(self):
        cron_list = [
            '0 0 * * * * Europe/Berlin',
            '0 0 4 20 * * America/New_York',
            '0 0 4 20 */2 fri America/New_York',
            '* */30 * * * * UTC',
            '* * * last_fri * * UTC',
            '* * * 3rd_sat * * UTC'
        ]

        for cron_str in cron_list:
            CronTrigger(**parse_cron(cron_str))
