import unittest
import pandas as pd
from sjtoolkit.expressions.namespace import DataFrameWithFields, SeriesWithFields, IntWithFields, FloatWithFields


class TestObjectWithFields(unittest.TestCase):
    def test_object_with_fields(self):
        IntWithFields(6) + IntWithFields(6)
        swf = SeriesWithFields([1, 2, 3], index=pd.date_range('2023-01-01', periods=3), name='swf')
        swf.fields = {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}}
        swf2 = SeriesWithFields([1, 2, 3], index=pd.date_range('2023-01-01', periods=3), name='swf2')
        swf2.fields = {'swf2': {'same': 'same', 'diff': 'diff2'}}
        dfwf = DataFrameWithFields({"ser1": [1, 2, 3]}, index=pd.date_range('2023-01-01', periods=3))
        dfwf.fields = {'ser1': {'same': 'same', 'diff': 'diff3'}}
        dfwf2 = DataFrameWithFields({"ser1": [1, 2, 3], "ser2": [1, 2, 3]}, index=pd.date_range('2023-01-01', periods=3))
        dfwf2.fields = {'ser1': {'same': 'same', 'diff': 'diff3'}, 'ser2': {'same': 'same', 'diff': 'diff4'}}

        res = swf + swf2
        self.assertEqual(res.fields, {'name_not_available': {'added': 'added', 'same': 'same'}})

        res = swf.add(swf2)
        self.assertEqual(res.fields, {'name_not_available': {'added': 'added', 'same': 'same'}})

        res = swf + dfwf
        self.assertEqual(res.fields, {'name_not_available': {'added': 'added', 'same': 'same'}})

        res = swf2.sum()
        self.assertTrue(isinstance(res, IntWithFields))
        self.assertEqual(res.fields, {'name_not_available': {'same': 'same', 'diff': 'diff2'}})

        res = swf2.sum() + swf
        self.assertEqual(res.fields, {'name_not_available': {'added': 'added', 'same': 'same'}})

        # special case, must return int
        res = len(swf2)
        self.assertTrue(isinstance(res, int))

        res = dfwf.sum().sum()
        self.assertEqual(res.fields, {'name_not_available': {'same': 'same', 'diff': 'diff3'}})

        res = swf2.sum() + dfwf.sum().sum()
        self.assertEqual(res.fields, {'name_not_available': {'same': 'same'}})

        # test complicated pandas
        res = swf.resample('H').ffill()
        self.assertEqual(res.fields, {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}})

        res = swf.rolling('72h').mean()
        self.assertEqual(res.fields, {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}})

        res = dfwf.groupby(dfwf.index.hour).sum()
        self.assertEqual(res.fields, {'ser1': {'same': 'same', 'diff': 'diff3'}})

        res = swf.expanding().sum()
        self.assertEqual(res.fields, {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}})

        res = swf.ewm(span=7, adjust=False).mean()
        self.assertEqual(res.fields, {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}})
        res = dfwf.ewm(span=7, adjust=False).mean()
        self.assertEqual(res.fields, {'ser1': {'same': 'same', 'diff': 'diff3'}})

        # test carrying on fields
        res = swf + swf2
        res = res + swf2
        self.assertEqual(res.fields, {'name_not_available': {'added': 'added', 'same': 'same'}})

        # test broadcast
        res = dfwf2.add(swf, axis='index')
        self.assertEqual(res.fields, {'ser1': {'same': 'same', 'added': 'added'}, 'ser2': {'same': 'same', 'added': 'added'}})

        # Ensure fields are unchanged
        self.assertEqual(swf.fields, {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}})
        self.assertEqual(swf2.fields, {'swf2': {'same': 'same', 'diff': 'diff2'}})
        self.assertEqual(dfwf.fields, {'ser1': {'same': 'same', 'diff': 'diff3'}})

        # test inplace
        dfwf2 = DataFrameWithFields(swf2)
        dfwf2.fields = swf2.fields.copy()
        dfwf.update(dfwf2)
        self.assertEqual(dfwf.fields, {'ser1': {'same': 'same'}})

    def test_underlying_fields(self):
        swf = SeriesWithFields([1, 2, 3], index=pd.date_range('2023-01-01', periods=3), name='swf')
        swf.fields = {'swf': {'same': 'same', 'diff': 'diff1', 'added': 'added'}}
        swf2 = SeriesWithFields([1, 2, 3], index=pd.date_range('2023-01-01', periods=3), name='swf2')
        swf2.fields = {'swf2': {'same': 'same', 'diff': 'diff2'}}
        dfwf = DataFrameWithFields({"ser1": [1, 2, 3]}, index=pd.date_range('2023-01-01', periods=3))
        dfwf.fields = {'ser1': {'same': 'same', 'diff': 'diff3'}}
        dfwf2 = DataFrameWithFields({"ser1": [1, 2, 3], "ser2": [1, 2, 3]},
                                    index=pd.date_range('2023-01-01', periods=3))
        dfwf2.fields = {'ser1': {'same': 'same', 'diff': 'diff3'}, 'ser2': {'same': 'same', 'diff': 'diff4'}}

        res = swf + swf2
        self.assertEqual(res.get_underlying_series(), {'name_not_available': ['swf', 'swf2']})

        # test broadcast
        res = dfwf2.add(swf, axis='index')
        self.assertEqual(res.get_underlying_series(), {'ser1': ['ser1', 'swf'], 'ser2': ['ser2', 'swf']})
