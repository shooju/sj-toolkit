import base64
import json
import os
import unittest
import mock

import flask

from sjtoolkit.exceptions import ExpressionException
from sjtoolkit.expressions.remote import EXPRESSION_ACCOUNT_HOST_HEADER


class ExpressionRunnerSetup(object):
    def setUp(self):
        env = self.get_env()
        os.environ.update(env)

        patcher = mock.patch('shooju.Connection')
        self.mock_connection = patcher.start()
        self.addCleanup(patcher.stop)

        self.mock_connection().scroll.return_value = []

        from sjtoolkit.bin.expression_runner import app
        self.app = app
        self.app.testing = True
        self.client = app.test_client()

    @staticmethod
    def basic_auth_header(user, password):
        valid_credentials = base64.b64encode(b'test_user:test_password').decode('ascii')
        return {'Authorization': 'Basic ' + valid_credentials}



class TestExpressionRunner(ExpressionRunnerSetup, unittest.TestCase):
    def get_env(self):
        self.sj_accounts = [
            {'account_host': 'http://localhost:5001', 'user': 'test_user1', 'api_key': '123'},
            {'account_host': 'http://localhost:5002', 'user': 'test_user2', 'api_key': '456'},
            {'account_host': 'http://localhost:5003', 'host_override': 'http://localhost:7003', 'user': 'test_user2', 'api_key': '456'},
        ]

        env = {
            'EXP_RUNNER_ACCOUNTS_TOTAL': str(len(self.sj_accounts))
        }
        def set_env(name, i, key):
            if key in it:
                env["{}_{}".format(name, i)] = it[key]

        for i, it in enumerate(self.sj_accounts, 1):
            set_env('EXP_RUNNER_ACCOUNT', i, 'account_host')
            set_env('EXP_RUNNER_USER', i, 'user')
            set_env('EXP_RUNNER_API_KEY', i, 'api_key')
            set_env('EXP_RUNNER_HOST_OVERRIDE', i, 'host_override')

        return env

    def test_accounts_loaded(self):
        # Check connections was created for each account on python script load
        calls = [mock.call(it.get('host_override') or it['account_host'], it['user'], it['api_key']) for it in self.sj_accounts]
        self.mock_connection.assert_has_calls(calls, any_order=True)

    def test_ping_request_returns_ok(self):
        r = self.client.get('/ping')
        self.assertEqual(r.get_data(as_text=True), "OK")

    def test_exec_request_without_account_uses_default_account(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_accounts[0]['account_host'])

    def test_exec_request_with_account_uses_correct_account(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            headers[EXPRESSION_ACCOUNT_HOST_HEADER] = self.sj_accounts[0]['account_host']
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_accounts[0]['account_host'])

            headers[EXPRESSION_ACCOUNT_HOST_HEADER] = self.sj_accounts[1]['account_host']
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_accounts[1]['account_host'])

    def test_exec_request_with_account_uses_correct_account_override(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            headers[EXPRESSION_ACCOUNT_HOST_HEADER] = self.sj_accounts[2]['account_host']
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_accounts[2]['host_override'])

    def test_exec_request_with_invalid_account_fails(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            headers[EXPRESSION_ACCOUNT_HOST_HEADER] = 'does-not-exist'
            with self.app.test_request_context('/autocomplete', headers=headers):
                with self.assertRaises(ExpressionException):
                    self.app.preprocess_request()


class TestLegacyExpressionRunner(ExpressionRunnerSetup, unittest.TestCase):
    def get_env(self):
        self.sj_account = 'http://localhost:5001'
        self.sj_user = 'test_user1'
        self.sj_api_key = '123'

        env = {
            'EXP_RUNNER_ACCOUNT': self.sj_account,
            'EXP_RUNNER_USER': self.sj_user,
            'EXP_RUNNER_API_KEY': self.sj_api_key,
        }
        return env

    def test_exec_request_without_account_uses_default_account(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_account)

    def test_exec_request_with_account_uses_correct_account(self):
        headers = self.basic_auth_header('test_user', 'test_password')
        with mock.patch('sjtoolkit.bin.expression_runner.ACCOUNTS_MANAGER') as mock_loader:
            mock_loader.get_account_importers().return_value = {'test_user': {}}
            headers[EXPRESSION_ACCOUNT_HOST_HEADER] = self.sj_account
            with self.app.test_request_context('/autocomplete', headers=headers):
                self.app.preprocess_request()
                self.assertEqual(flask.g.account, self.sj_account)
