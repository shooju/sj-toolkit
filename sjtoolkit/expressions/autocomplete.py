import jedi
import six
from flask import g

from sjtoolkit.expressions.utils import replace_sid_in_expression

jedi.settings.case_insensitive_completion = False
jedi.settings.add_dot_after_module = True  # weird!
jedi.settings.add_bracket_after_function = True  # weird!
jedi.settings.dynamic_array_additions = True
jedi.settings.dynamic_params = True
jedi.settings.dynamic_flow_information = True
jedi.settings.additional_dynamic_modules = []
jedi.settings.auto_import_modules = [
    "hashlib",  # setattr
]


def handle_series_by_query(query, starter=None, stopper=None):
    """Use starter/stopped to modify default handler logic of using first char as starter/stopper."""
    add_starter = query is None
    if starter is None:
        starter = query[0] if query else 'ur"'
        stopper = query[0] if query else '"'
        query = query[1:] if query else ""
    opts = handle_series_autocomplete(query).get("options", [])
    options = [
        {
            "complete": u"{}{}".format(starter, o["complete"])
            if add_starter
            else o["complete"],
            "complete_endings": o.get("complete_endings", []),
            "complete_del_chars": o.get("complete_del_chars", 0),
            "value": o["value"],
            "hint": u"{} series".format(o["count"]),
            "type": o["type"],
            "count": o.get("count"),
        }
        for o in opts
    ]
    return {
        "options": options,
    }


CALLNAME_PARAM_TO_HANDLER = {
    ("sjs", "series_query"): lambda x: handle_series_by_query(x),
    ("sjdf", "query"): lambda x: handle_series_by_query(x),
    ("sjsordf", "query"): lambda x: handle_series_by_query(x),
}


def handle_series_autocomplete(query, types="field,fieldvalue", size=50):
    res = g.sj.raw.get(
        "/series/autocomplete", params={"query": query, "types": types, "size": size}
    )
    return res


def autocomplete(
    expression, line=None, column=None, sys_path=None, loaded_processors=None
):
    """Returns JSON autocomplete structure for expression result.
    {
        "call": { // optional if there is a call being called
            "params": [
                {
                    "name": "", // name of param
                    "active": "" // optional; true if param is active (only one can be true); missing implies false
                    "default": "" // optional; default value; missing implies required
                }
            ],
            "name": "", // name of the function/class/etc
            "docs": "", // function documentation; may have \n
            "type": "", // function/class
        },
        "hint": "", // one-line main user hint
        "options": [ //optional
            {
                "value":"", // autocomplete value
                "hint":"" // hint for the value
            },
            ...
        ]
    }"""
    res = {"options": [], "hint": ""}
    loaded_processors = loaded_processors or []
    current_cut_line = get_cut_line_from_expression(expression, line, column)

    # if we're in an expression sid {{ }}, do autocomplete based on that
    expr_sid = get_current_sid_in_expression(current_cut_line)
    if expr_sid is not None:
        return handle_series_by_query(expr_sid, starter="{{", stopper="}}")

    # are we in a literal?
    inside_literal = get_current_quoted(expression)
    if inside_literal:
        res["in_literal"] = inside_literal

    expression = replace_sid_in_expression(expression)

    # seems Script + FUNC_STRING just works better than Interpreter + namespaces

    code_str = u"{}\n{}".format(FUNC_STR, expression)

    imports = [p.upper() for p in loaded_processors]
    code_str = u"{}\n{}".format(
        "\n".join(["import {}".format(i) for i in imports]), code_str
    )
    project = jedi.Project(".", sys_path=sys_path)
    s = jedi.Script(code_str.encode("utf-8"), project=project)

    # jedi.set_debug_function(notices=False)#warnings=False,

    # step 1: function
    call_sigs = s.get_signatures(line, column)
    if call_sigs:
        callname_param = None
        if len(call_sigs) > 1:
            pass  # TODO sentry if len(call_sigs)>1 with the expression...
        cs = call_sigs[-1]
        params = []
        for i, pin in enumerate(cs.params):
            name_parts = pin.description.split("=", 2)
            name_parts[0] = name_parts[0].split(" ", 2)[-1]
            pout = {
                "name": name_parts[0],
                # TODO add docs (parse docs to get?)
            }
            if len(name_parts) == 2:
                pout["default"] = name_parts[1]
            if cs.index == i:
                pout["active"] = True
                callname_param = (cs.name, name_parts[0])
            params.append(pout)

        hint_params = ", ".join(
            [
                "*{}{}{}*".format(
                    p["name"], "=" if "default" in p else "", p.get("default", "")
                )
                if p.get("active")
                else "{}{}{}".format(
                    p["name"], "=" if "default" in p else "", p.get("default", "")
                )
                for p in params
            ]
        )
        res["function"] = {
            "name": cs.name,
            "params": params,
            "docs": cs.docstring(raw=True),
            "type": cs.type,
            # 'full_name': cs.full_name,
            "hint": "{}({})".format(cs.name, hint_params),
        }
        if callname_param in CALLNAME_PARAM_TO_HANDLER:
            res.update(CALLNAME_PARAM_TO_HANDLER[callname_param](inside_literal))
            return res

    # we're inside a literal... no point in showing completions
    if inside_literal:
        return res

    # step 2: completions
    can_see_sjpoints = False
    for c in s.complete():
        if "sjs" == c.name:
            can_see_sjpoints = True
        if c.complete.startswith("_") or c.complete.startswith("test"):
            continue
        if c.is_keyword or c.in_builtin_module():
            continue
        res["options"].append(
            {
                "complete": str(c.complete),
                "type": c.type,
                "hint": c.docstring().strip(),
                "value": "{}{}".format(
                    c.name,
                    {
                        "function": "()",
                    }.get(c.type, ""),
                ),
            }
        )
    if can_see_sjpoints:
        res["options"].append(
            {
                "complete": "{" if expression and expression[-1] == "{" else "{{",
                "type": "series",
                "hint": "{{}}-enclosed Shooju series id",
                "value": "{{",
            }
        )
    return res


AUTOCOMPLETE_SYSPATH_ROOT = "/tmp/autocomplete_syspath"
FUNC_STR = '''
import pandas as pd

def sjdf(query, max_series=10, key_field='series_id', df='MIN', dt='MAX', max_points=10):
    """Returns a DataFrame based on a Shooju query.

    Parameters
    ----------
    max_series : int, default 10
        Maximum number of series to return as column in the dataframe
    key_field : string, default sid
        Field to use as the column header in the returned DataFrame
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    Returns
    -------
    DataFrame
    """
    return pd.DataFrame()


def sjsordf(query, max_series=10, key_field='series_id', df='MIN', dt='MAX', max_points=10):
    """Returns a DataFrame or Series (if is a single series in result) based on a Shooju query.

    Parameters
    ----------
    max_series : int, default 10
        Maximum number of series to return as column in the dataframe
    key_field : string, default sid
        Field to use as the column header in the returned DataFrame
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    Returns
    -------
    Series
    """
    return pd.Series()


def sjs(series_query, df=None, dt=None, max_points=10):
    """Returns a Series based on a Shooju series_id or $-query.

    series_query : string, required
        Series query. Must result with a single series.
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    Returns
    -------
    Series
    """
    return pd.Series()

def dt(year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0):
    """Returns a GMT datetime."""
    from datetime import datetime
    return datetime(year, month, day, hour, minute, second, microsecond)
'''


def get_cut_line_from_expression(expression, line=None, column=None):
    """Returns cut line (i.e. before cursor) in expression."""
    expression_lines = expression.split(u"\n")
    line = expression_lines[-1 if line is None else line]
    if column is not None:
        line = line[0:column]
    return line


def get_current_sid_in_expression(current_line):
    """Returns the part after the current {{ or None if we are not in a {{ }}.

    TODO this fails to notice if opener or closer are in quotes.
    """
    by_opener = current_line.split(u"{{")  # TODO what if this {{ was in quotes?
    if len(by_opener) == 1:
        return
    last_segment = by_opener[-1]
    if u"}}" not in last_segment:
        return last_segment


def get_current_quoted(current_line):
    """returns the current quoted string, or none if we're not in a quoted string.def

    todo this is a hack mostly because of using unexposed api
         jedi must have a better way to do this!
         ignores multiline quotes
    """
    import parso

    for t in list(
            parso.python.tokenize.tokenize(
                six.text_type(current_line), version_info=parso.utils.version_info()
            )
    ):
        if t.type.name == "ERRORTOKEN" and t.string[0] in "\"'":
            return current_line[t.start_pos[1]:]


