import re

import numpy
import numpy as np
import pandas
import pytz

from sjtoolkit.exceptions import ExpressionException
from sjtoolkit.utils.utils import render_date, parse_date

SID_REPLACER_RE = re.compile("\{\{(((?!\}{2}).)*)\}\}", flags=re.MULTILINE)


def replace_sid_in_expression(expression):
    """Replaces {{ }} in expressions

    warning: DOES NOT SUPPORT ANY WAY OF ESCAPING }} inside of {{ }}"""

    def repl(m):
        escaped_sid = m.group(1).replace(u"\\", u"\\\\").replace(u'"', u'\\"')
        return u'sjsordf("{}")'.format(escaped_sid)

    return SID_REPLACER_RE.sub(repl, expression)


def validate_pre_post_chains(chain):
    """
    Makes sure operators chain with @pre/post used conforms the following restrictions:
    - @pre/post operators can be used just once
    - regular operators must follow @pre/post operators
    """
    chain = list(map(str.lower, chain))

    chain_types = {"pre", "post"}

    if not chain:
        return

    if chain[0] not in chain_types:
        raise ExpressionException(
            "operators chain must start with @pre/@post if any of these operators used"
        )

    for chain_type in chain_types:
        if chain.count(chain_type) > 1:
            raise ExpressionException(
                "@{} operator cannot occur more than once".format(chain_type)
            )


def operators_chain_rebuild(expression_operators, suffix_pre_operators):
    """
    Merges operators inside ={{....}} with operators that are placed in the expression suffix.
    """
    operators = expression_operators + suffix_pre_operators
    chain_types = {"pre", "post"}
    # nothing to do in the cases below
    if not set(operators).intersection(chain_types):
        return operators

    if not expression_operators:
        return suffix_pre_operators

    if not suffix_pre_operators:
        return expression_operators

    # this part must be valid before we merge
    if expression_operators and set(expression_operators).intersection(chain_types):
        validate_pre_post_chains(expression_operators)

    pre_chain, post_chain = [], []
    current_chain = pre_chain

    for op in operators:
        if op == "pre":
            current_chain = pre_chain
            continue
        elif op == "post":
            current_chain = post_chain
            continue
        current_chain.append(op)

    if pre_chain:
        pre_chain.insert(0, "pre")

    if post_chain:
        post_chain.insert(0, "post")

    return pre_chain + post_chain


def pandas_series_to_milli_points(
    ts, add_job_id=None, add_timestamp=None, tz=None, must_be_localized=False
):
    """
    Converts pandas.Series to points array in [milli, val] format
    """
    if ts.dtype.name == "bool":  # convert bool type to int
        ts = ts.astype(numpy.int64)
    elif (
        ts.dtype.name == "object"
    ):  # convert mixed type to float (probably should not happen)
        ts = ts.astype(numpy.float64)
    ts = ts.dropna()  # remove nans

    jobs_and_ts = list()
    if add_job_id:
        jobs_and_ts.append([add_job_id] * len(ts))
    if add_timestamp:
        jobs_and_ts.append([add_timestamp] * len(ts))

    # in case user converted timezone we should make sure we are in correct tz before make points naive
    if tz is not None and ts.index.tzinfo is not None and ts.index.tzinfo.zone != tz:
        ts = ts.tz_convert(tz)
    elif must_be_localized and (
        not hasattr(ts.index, "tzinfo") or ts.index.tzinfo is None
    ):
        raise ExpressionException(
            'expression result is "naive" however @localize operator was used'
        )

    try:
        if isinstance(ts.index, pandas.DatetimeIndex):
            dates = ts.index.tz_localize(None).astype(numpy.int64) / 1000000
        else:  # we didn't use @localize parameter
            dates = ts.index.astype(numpy.int64) / 1000000
    except (TypeError, ValueError) as e:
        raise ExpressionException(
            "Unexpected series index '{index_type}': {error}".format(
                index_type=ts.index.dtype, error=e
            )
        )

    return list(zip(dates, ts, *jobs_and_ts))


def array_convert_tz(a, from_tz, to_tz):
    """
    Convert list of milli tuples or numpy
    array from one timezone to another

    :param a: List tuples or numpy array representing array of points
    :param from_tz: Current timezone of the points
    :param to_tz: Target timezone of the points
    """
    from_tz = pytz.timezone(from_tz)
    to_tz = pytz.timezone(to_tz)
    is_numpy = isinstance(a, np.ndarray)

    occurrence = set()  # this is to handle dst
    res = list()

    for pt in a.tolist() if is_numpy else a:
        milli = pt[0]
        dst = milli not in occurrence
        dt = from_tz.localize(render_date(milli, "datetime"), is_dst=dst)
        res.append(
            (parse_date(dt.astimezone(to_tz).replace(tzinfo=None), "datetime"),)
            + pt[1:]
        )
        occurrence.add(milli)

    if is_numpy:
        res = np.array(res, dtype=a.dtype)

    return res