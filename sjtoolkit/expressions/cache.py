from diskcache import Cache


class DiskCacheBackend(object):
    """DiskCacheBackend is a disk cache backend for CachedRemoteExpressionCalculator."""

    def __init__(self, directory, size_mb):
        self._cache = Cache(directory, size_limit=size_mb * 1024 * 1024)

    def set(self, key, data, meta, ttl):
        self._cache.set(key, [data, meta], expire=ttl)

    def get(self, key):
        r, ttl = self._cache.get(key, expire_time=True)
        if r is None:
            return None, None, None

        data, meta = r
        return data, meta, ttl

    def reset_ttl(self, key, seconds):
        r = self.get(key)
        if r is None:
            return
        data, meta = r
        self.set(key, data, meta, seconds)
        # TODO: update diskcache version to use touch method
        # self._cache.touch(key, expire=seconds)

    def drop_cache(self):
        self._cache.clear()
