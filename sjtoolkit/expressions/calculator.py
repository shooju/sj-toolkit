import json
import sys
import traceback
from collections import defaultdict
from datetime import datetime
from functools import reduce

import pandas
import six
from flask import g
from werkzeug.local import LocalProxy

from sjtoolkit.exceptions import (
    ExpressionSyntaxException,
    ExpressionException,
)
from sjtoolkit.expressions.namespace import (
    Functions,
    ExpressionsSjUtils,
    ThreadLocalDict,
    LazyModule,
    SjDf,
    SjOrDf,
    Sjs,
    ImportedShoojuProcessor,
    UTC,
    SeriesWithFields,
    DataFrameWithFields,
    IntWithFields,
    FloatWithFields
)
from sjtoolkit.series.args_parse import parse_series_id
from sjtoolkit.expressions.utils import (
    replace_sid_in_expression,
)
from sjtoolkit.utils.series import fields_post_processor
from sjtoolkit.utils.utils import (
    extract_field,
    unflattify,
    MIN_PYTHON_DATE,
    MAX_PYTHON_DATE,
)
from sjtoolkit.utils.sentry import capture_message

MAX_USED_SERIES_IDS_NUM = 10


DEFAULT_ID_FIELD = 'series_id'

def extract_xpr_fields(fields_set, source):
    """
    Extracts fields by given set of fields.

    :param fields_set: array of fields names to extract (fieldA.fieldB, '*', fieldA.* etc)
    :param source: mapping with data to extract
    """
    fields = {}
    dont_return_with_all = ['sid', '_var', 'meta']

    for user_field in fields_set:
        field = user_field
        if user_field == "*":
            user_field = None
        elif user_field.startswith("="):  # jinja operator
            field = "*"
        else:
            field = user_field.split("@", 1)[0]

        val = extract_field(source, field)
        if val is not None:
            if user_field is not None:
                fields[user_field] = val
            else:
                fields.update({k: v for k, v in val.items() if k not in fields})

    for to_remove in dont_return_with_all:
        if to_remove not in fields_set:
            fields.pop(to_remove, None)
    return fields


class ExpressionCalculator(object):
    GLOBALS = {  # global variables available in expressions
        "pd": pandas,
        "__show_tb__": True,
    }

    SHOOJU_AGG_PERIOD_TO_PANDAS = {
        "d": "D",
        "y": "AS",
        "q": "QS",
        "M": "MS",
        "w": "W-MON",
        "h": "H",
        "m": "MIN",
    }

    def __init__(self, accounts_manager):
        self.ctx = None
        self.sjutils = None
        self._accounts_manager = accounts_manager

    def calculate(
        self,
        expression,
        df=None,
        dt=None,
        max_points=0,
        user_namespace=None,
        caller_user=None,
        g_expression=None,
        pre_operators=None,
        series_fields=None,
        include_job=False,
        include_timestamp=False,
        max_series=100,
        sort=None,
    ):
        """Calculates expression result.  Does not type check output.

        args:
            max_points -- provides default for sj* funcs
            df -- provides default for sj* funcs
            dt -- provides default for sj* funcs
            nodata -- if True, does not actually pull any data, instead just instantiates data structures; useful for autocomplete
            user_namespace -- Mapping with user namespace. Actually contains importer which user has access to.
            fields -- Comma delimited list of fields
            include_job -- Include job id
            include_timestamp -- Include timestamp
        """
        expression = replace_sid_in_expression(expression.strip())

        reverse = False
        if df and dt and df > dt:
            reverse = True

        fields_to_read = list(series_fields)
        if any(f.startswith("=") for f in fields_to_read):
            fields_to_read.append("*")

        ns = self._build_namespace(
            df,
            dt,
            max_points,
            user_namespace,
            caller_user,
            g_expression,
            pre_operators,
            fields_to_read,
            include_job,
            include_timestamp,
            max_series,
            sort,
            expression
        )
        loc = {}
        try:
            res = eval(expression, ns, loc)
        except SyntaxError as e:  # don't send expression syntax error to sentry
            raise ExpressionSyntaxException(generate_expression_traceback())
        except Exception as e:
            if (
                isinstance(e, ExpressionException) and not g.debug
            ):  # we don't show traceback for such errors
                raise

            exc_type, _, _ = sys.exc_info()
            raise exc_type(generate_expression_traceback())

        res = self._process_default_res(res, expression, ns, loc)

        return self._process_results(expression, res, series_fields, reverse, caller_user)

    def _process_default_res(self, res, expression, ns, loc):
        """ Function to process res without fields """
        if isinstance(res, pandas.Series) and not isinstance(res, SeriesWithFields):
            res.__class__ = SeriesWithFields
            res.fields = self.ctx.fields_by_sids
            res.process_fields()
            capture_message('Expression returned pandas.Series', {
                'res': res,
                'expression': expression,
                'ns': ns,
                'loc': loc
            })  # log expressions returning pandas.Series, to later add to fields engine
        if isinstance(res, pandas.DataFrame) and not isinstance(res, DataFrameWithFields):
            res.__class__ = DataFrameWithFields
            res.fields = self.ctx.fields_by_sids
            res.process_fields()
            capture_message('Expression returned pandas.DataFrame', {
                'res': res,
                'expression': expression,
                'ns': ns,
                'loc': loc
            })
        if isinstance(res, int) and not isinstance(res, IntWithFields):  # =2 or @expressionable
            res = IntWithFields(res)
            res.fields = self.ctx.fields_by_sids
            res.process_fields()
        elif isinstance(res, float) and not isinstance(res, FloatWithFields):
            res = FloatWithFields(res)
            res.fields = self.ctx.fields_by_sids
            res.process_fields()

        return res

    def _process_results(self, expression, res, series_fields, reverse, caller_user):
        self._validate_res(res)

        # remove operators from sids
        query_by_sid = {
            parse_series_id(k)[0]: v for k, v in six.iteritems(self.ctx.query_by_sid)
        }

        def _remove_ops(s):
            return parse_series_id(s)[0]

        fields_by_sids = None
        if hasattr(res, 'fields'):  # res can be None
            fields_by_sids = res.fields
        fields_by_sids = fields_by_sids or self.ctx.fields_by_sids or {}

        underlying_series = self.ctx.underlying_series
        if hasattr(res, 'fields'):
            underlying_series = underlying_series or res.get_underlying_series()
        underlying_series = underlying_series or {'name_not_available': self.ctx.all_queries}

        def _get_underlying_series_queries(key, el):
            if self.ctx.underlying_series_queries.get(el):
                el = self.ctx.underlying_series_queries.get(el)
            elif self.ctx.underlying_series_queries.get(_remove_ops(el)):
                el = self.ctx.underlying_series_queries.get(_remove_ops(el))

            return el

        underlying_series_queries = {}
        for key, arr in underlying_series.items():
            underlying_series_queries[key] = list()
            for x in arr:
                usq = _get_underlying_series_queries(key, x)
                if isinstance(usq, list):
                    underlying_series_queries[key].extend(usq)
                else:
                    underlying_series_queries[key].append(usq)

        fields_by_sids = {
            _remove_ops(k): v for k, v in fields_by_sids.items()
        }

        all_series_ids = self.ctx.all_series_ids or set()

        all_queries = self.ctx.all_queries
        max_job_ids = self.ctx.max_job_ids

        dont_apply_breaking_change_for = self._accounts_manager.get_account_config(g.account).get('tmp_20230517') or []
        # filter fields
        if series_fields:
            for sid, fields in fields_by_sids.items():
                fields = extract_xpr_fields(series_fields, unflattify(fields))
                fields_post_processor(fields)
                fields_by_sids[sid] = fields
        elif caller_user not in dont_apply_breaking_change_for and '*' not in dont_apply_breaking_change_for:  # TODO replace by simple `else`
            fields_by_sids = dict()

        if reverse and isinstance(res, (pandas.Series, pandas.DataFrame)):
            res.sort_index(ascending=False, inplace=True)

        return (
            res,
            query_by_sid,
            fields_by_sids,
            list(sorted(all_series_ids)[:MAX_USED_SERIES_IDS_NUM]),
            list(sorted(all_queries)),
            max(max_job_ids) if max_job_ids else None,
            underlying_series,
            underlying_series_queries
        )

    def _validate_res(self, res):
        if isinstance(res, pandas.DataFrame):
            for ix, (series_id, ts) in enumerate(res.iteritems()):
                if not isinstance(series_id, str):
                    raise ExpressionException('DataFrame column must be str got {}'.format(type(series_id)))


    def _fetch_series_fields(self, columns, fields_to_read):
        # for some reason we could not read fields (some custom function used)
        # but we assume columns are series ids. let's try to fetch these fields separately
        mget = g.sj.mget()
        for s in columns:
            mget.get_fields(s, fields_to_read)
        fields_by_sids = {s: f for s, f in zip(columns, mget.fetch()) if f}
        return fields_by_sids, set(fields_by_sids)

    def _build_namespace(
        self,
        df,
        dt,
        max_points,
        user_namespace,
        caller_user,
        g_expression,
        pre_operators,
        series_fields,
        include_job,
        include_timestamp,
        max_series,
        sort,
        expression
    ):
        account_settings = self._accounts_manager.get_account_config(g.account)

        ns = {
            "sjclient": LocalProxy(lambda: g.user_ns_sj),
        }
        ns.update(
            Functions(self._accounts_manager.get_account_config(g.account)).to_dict()
        )
        ns.update(self.GLOBALS)

        id_field = DEFAULT_ID_FIELD
        if account_settings.get('id_field') and account_settings['id_field'] not in ('sid', 'series_id'):
            id_field = account_settings['id_field']

        self.ctx = ExpressionContext(g.sj, debug=g.debug, trace=g.trace)
        self.sjdf = SjDf(
            self.ctx,
            series_fields,
            include_job,
            id_field=id_field,
            max_series=max_series,
            key_field=id_field,
            df=df,
            dt=dt,
            max_points=max_points,
            operators=pre_operators,
            fields=None,
            sort=sort
        )
        self.sjs = Sjs(
            self.ctx,
            series_fields,
            include_job,
            id_field=id_field,
            df=df,
            dt=dt,
            max_points=max_points,
            operators=pre_operators,
        )
        self.sjsordf = SjOrDf(
            self.ctx,
            series_fields,
            include_job,
            id_field=id_field,
            max_series=max_series,
            key_field=id_field,
            df=df,
            dt=dt,
            max_points=max_points,
            operators=pre_operators,
            fields=None,
            sort=sort
        )

        self.sjutils = user_namespace.setdefault(
            "sjutils",
            ExpressionsSjUtils(
                self._accounts_manager,
                ns,
                self._accounts_manager.get_account_config(g.account),
            ),
        )
        self.sjutils.expression_ctx = self.ctx
        self.sjutils.expression_operators = pre_operators
        self.sjutils.sjsordf = self.sjsordf
        self.sjutils.sjs = self.sjs
        self.sjutils.sjdf = self.sjdf

        try:
            dmin = datetime.fromtimestamp(df / 1000)
        except (OverflowError, OSError, ValueError, TypeError):
            dmin = None
        try:
            dmax = datetime.fromtimestamp(dt / 1000)
        except (OverflowError, OSError, ValueError, TypeError):
            dmax = None
        if dmin is not None and dmax is not None:
            reversed = dmin > dmax
        else:
            reversed = False

        stgs = user_namespace.setdefault("settings", ThreadLocalDict())
        stgs.clear()
        stgs.update(
            {
                "request_params": {
                    "df": df,
                    "dt": dt,
                    "parsed_dates": {
                        "dmin": dmin,
                        "dmax": dmax,
                        "reversed": reversed
                    },
                    "max_points": max_points,
                    "fields": series_fields,
                    "include_job": include_job,
                    "include_timestamp": include_timestamp,
                    "sort": sort,
                    "max_series": max_series,
                    "operators": [
                        o.strip().split(":", 1)
                        for o in pre_operators.split("@")
                        if o.strip()
                    ]
                    if pre_operators
                    else [],
                    "expression": '=' + expression
                }
            }
        )

        ns.update(
            {
                "sjdf": self.sjdf,
                "sjs": self.sjs,
                "sjsordf": self.sjsordf,
                "num_to_series": lambda val, freq: self._num_to_series(
                    val, freq, max_points, df, dt
                ),
                "caller_user": caller_user,
                "print": LocalProxy(lambda: g.trace.print_func),
            }
        )
        # TODO NEED TO MAKE SETTINGS THREAD SAFE!
        self._recursively_inject_global_in_namespace(user_namespace, "settings", stgs)
        self._recursively_inject_global_in_namespace(
            user_namespace, "__show_tb__", True
        )
        if caller_user is not None:
            self._recursively_inject_global_in_namespace(
                user_namespace, "caller_user", caller_user
            )
        ns.update(user_namespace or dict())

        # in case init expression provided let's exec it and update the namespace
        if g_expression:
            if 'SJEXTENSIONS' in ns and 'XPRGModule' in ns['SJEXTENSIONS'].__dict__:
                kls = ns['SJEXTENSIONS'].__dict__['XPRGModule']
            else:
                kls = LazyModule

            ns["G"] = kls("G", self.ctx, g_expression=g_expression, ns=ns, dmin=dmin, dmax=dmax)

        return ns

    @classmethod
    def _num_to_series(cls, val, freq, size, start, end):
        """Generates array from single numeric value"""
        if start == MIN_PYTHON_DATE:
            raise ExpressionException(
                "num_to_series function requires from_date to be set"
            )
        if freq not in cls.SHOOJU_AGG_PERIOD_TO_PANDAS:
            raise ExpressionException(
                "frequency must be one of: {}".format(
                    ", ".join(cls.SHOOJU_AGG_PERIOD_TO_PANDAS)
                )
            )
        start, end = datetime.utcfromtimestamp(
            start / 1000.0
        ), datetime.utcfromtimestamp(end / 1000.0)
        dt_range_kwargs = {
            "periods": size,
            "freq": cls.SHOOJU_AGG_PERIOD_TO_PANDAS[freq],
        }
        if start > end:
            dt_range_kwargs["end"] = start
        else:
            dt_range_kwargs["start"] = start

        s = pandas.Series([val] * size, pandas.date_range(**dt_range_kwargs))

        # we need to change order if from_date is greater than to_date
        slice_args = []
        min_date = min(start, end)
        max_date = max(start, end)

        if min_date != datetime.utcfromtimestamp(MIN_PYTHON_DATE / 1000.0):
            slice_args.append(min_date)
        else:
            slice_args.append(None)
        if max_date != datetime.utcfromtimestamp(MAX_PYTHON_DATE / 1000.0):
            slice_args.append(max_date)
        else:
            slice_args.append(None)

        if start > end:
            s = s.reindex(index=s.index[::-1])[slice(*list(reversed(slice_args)))]
        else:
            s = s[slice(*slice_args)]
        return s

    @classmethod
    def _recursively_inject_global_in_namespace(cls, ns, global_name, global_value):
        """Recursively updates settings in all loaded custom functions including nested ones"""
        for mod in ns.values():
            if isinstance(mod, ImportedShoojuProcessor):
                setattr(mod, global_name, global_value)
                cls._recursively_inject_global_in_namespace(
                    mod.__dict__, global_name, global_value
                )  # update internal imported sub modules


class ExpressionContext(object):
    def __init__(self, sjclient, debug, trace):
        self.fields_by_sids = dict()
        self.all_series_ids = set()
        self.all_queries = set()  # this is to fetch facets
        self.query_by_sid = dict()
        self.all_timezones = set()
        self.max_job_ids = list()
        self.sjclient = sjclient
        self.points_scrolled = 0
        self.debug = debug
        self.trace = trace
        self.underlying_series = dict()
        self.underlying_series_queries = dict()

    def assert_single_timezone(self):
        if len(self.all_timezones) > 1:
            raise ExpressionException(
                "got series in different timezones: %s" % ", ".join(self.all_timezones)
            )

    def get_result_timezone(self):
        tzs = [s for s in self.all_timezones if s != "UTC"]
        if not tzs:
            return

        return list(tzs)[0]

    def update(self, query, points, flds, series_ids, max_job_id):
        self.all_queries.add(query)
        if flds:
            self.fields_by_sids.update(flds)

        self.all_series_ids.update(series_ids)

        if max_job_id is not None:
            self.max_job_ids.append(max_job_id)

        # empty means query returns nothing
        is_empty_data_frame = (
            isinstance(points, pandas.DataFrame) and points.columns.empty
        )

        if (
            isinstance(points, (pandas.Series, pandas.DataFrame))
            and points.index.tzinfo is not None
            and not is_empty_data_frame
        ):
            tzinfo = points.index.tzinfo or UTC
            self.all_timezones.add(tzinfo.zone)

        self.assert_single_timezone()


def generate_expression_traceback():
    exc_type, exc_value, exc_traceback = sys.exc_info()

    def extract_tb(
        tb,
    ):
        res = []
        n = 0
        while tb is not None:
            f = tb.tb_frame
            if not f.f_globals.get("__show_tb__"):
                tb = tb.tb_next
                continue
            lineno = tb.tb_lineno
            co = f.f_code
            filename = co.co_filename
            name = co.co_name
            res.append((filename, lineno, name, None))
            tb = tb.tb_next
            n = n + 1
        return res

    terms = traceback.format_exception(exc_type, exc_value, exc_traceback.tb_next)

    if exc_type is SyntaxError: # special case
        return f'Invalid syntax (line {exc_value.lineno}, offset {exc_value.offset}): {exc_value.text[:exc_value.offset]} <-'

    error_text = terms[-1].strip("\n")
    error_stack = []
    for filename, lineno, name, _ in extract_tb(exc_traceback):
        error_stack.append(
            "{} line:{} {}".format(
                filename if filename != "<string>" else "<expression>", lineno, name
            )
        )

    error_stack.append(error_text)
    return u"\n".join(error_stack)

