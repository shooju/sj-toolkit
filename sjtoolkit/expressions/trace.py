import time

import shooju
from shooju.hooks.base import BaseHook, Request, Response

from sjtoolkit.exceptions import ExpressionException
from sjtoolkit.utils.trace import UpstreamRequestContext
from sjtoolkit.utils.metrics import metrics


class XPRTimerMixin:
    def __init__(self):
        self._s = None
        self.duration = 0.0

    def start_timer(self):
        self._s = time.time()

    def finish_timer(self):
        self.duration += time.time() - self._s

    def __enter__(self):
        self.start_timer()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.finish_timer()


class XPRQueryTraceDebug(XPRTimerMixin):
    """
    Collects debug info for single query read in expression.
    """

    def __init__(self, query: str):
        self.query = query
        self.results = list()
        self.results_num = 0
        super(XPRQueryTraceDebug, self).__init__()

    def handle_api_response(self, resp):
        """Extracts recursive xpr calls trace info and re-raises an error if API thrown an error"""
        if "error" in resp:
            xpr = (
                    resp.get(
                        "xpr",
                    )
                    or dict()
            )
            trace = xpr.get("trace") or dict()
            if (
                    not trace and not xpr
            ):  # this is regular series query failed, not some expression
                trace["error"] = resp["description"]
            self.results.append(trace)
            raise shooju.ShoojuApiError(resp["description"])

        series = resp.get("series") or []
        for ser in series:
            self.handle_series_level_response(ser)

    def handle_series_level_response(self, resp, raise_on_not_found=False):
        """Extracts recursive xpr calls trace info from series level responses"""
        if (
                raise_on_not_found
                and "error" in resp
                and resp["error"] == "series_not_found"
        ):
            raise shooju.ShoojuApiError("series not found")

        if resp.get("xpr", {}).get("trace"):
            self.add_xpr_result(resp)
        elif 'series_id' in resp:
            self.add_sid_result(resp)

        if "error" in resp:  # re-raise api error
            raise shooju.ShoojuApiError(resp.get("description") or resp['error'])

    def add_sid_result(self, res):
        self.results.append({"sid": res["series_id"]})
        self.results_num += 1

    def add_xpr_result(self, res):
        self.results.append(res["xpr"]["trace"])
        self.results_num += 1

    def to_json(self):
        """Generate query debug info json representation"""
        return {
            "query": self.query,
            "results": self.results,
            "duration": self.duration,
            'results_num': self.results_num,
        }

    def __hash__(self):
        # needed for @memoize
        return self.query.__hash__()


class XPRQueryTrace(XPRQueryTraceDebug):
    """
    Collects debug info for single query read in expression.
    It doesn't collect a list of series returned by query.
    """
    def add_sid_result(self, res):
        self.results_num += 1


class XPRQueryDoNothing(XPRQueryTraceDebug):
    def handle_api_response(self, resp):
        pass


class XPRTracer(XPRTimerMixin):
    """
    Collects expression debug information (errors, query reads and its timing).
    """

    def __init__(self, xpr_query_class=XPRQueryTraceDebug, collect_prints=False):
        self._pre_operators = None
        self._post_operators = None
        self._expression = None
        self._g_expression = None
        self._reads = list()
        self._xpr_query_class = xpr_query_class
        self._prints = list()
        self._collect_prints = collect_prints
        super(XPRTracer, self).__init__()

    def start(self, expression, pre_operators, post_operators, g_expression):
        """Associates current tracer instance with particular expression"""
        self._expression = expression
        self._pre_operators = pre_operators
        self._post_operators = post_operators
        self._g_expression = g_expression

    def start_query_trace(self, query: str) -> XPRQueryTraceDebug:
        """Start query tracing"""
        query = self._xpr_query_class(query)
        self._reads.append(query)
        return query

    def print_func(self, *args):
        self._prints.append(' '.join(map(str, args))) if self._collect_prints else print

    def to_json(self):
        """Generate xpr trace info json representation"""
        t = {
            "expression": self._expression,
            "duration": self.duration,
            "reads": [q.to_json() for q in self._reads],
            "pre_operators": self._pre_operators,
            "post_operators": self._post_operators,
        }
        if self._g_expression:
            t['g_expression'] = self._g_expression

        if self._prints:
            t['prints'] = self._prints

        return t

class TraceApiHook(BaseHook):
    """
    An API hook that intercepts Shooju API calls and collects xpr trace data.
    """

    def __init__(self, tracer) -> None:
        super().__init__()
        self.tracer: XPRTracer = tracer
        self.query = None
        self.method = None
        self.queries = list()

    def before_request(self, request: Request) -> Request:
        if not request.url.endswith('/series'):
            return request

        queries = []
        method = request.method.lower()
        if method == "get":
            queries = self._extract_queries_from_url_params(request.params)
        elif method == "post" and request.data_json:
            queries = self._extract_queries_from_payload(request.data_json)
        queries = queries or ["<undefined>"]
        queries = [self.tracer.start_query_trace(q) for q in queries]
        self.query = queries[0]
        self.method = method
        self.queries = queries
        if not request.params:
            request.params = dict()
        request.params.update({'debug': 'y'})

        return request

    def after_request(self, response: Response) -> Response:
        if not response.json or not self.query:
            return response

        json_response = response.json

        if "error" in json_response or self.method == "get":
            self.query.handle_api_response(json_response)
        elif "series" in json_response and len(json_response["series"]) == len(self.queries):
            for sub_resp, sub_query in zip(json_response["series"], self.queries):
                sub_query.handle_series_level_response(sub_resp)
        return response

    def _extract_queries_from_url_params(self, params):
        return [params["query"]] if "query" in params else []

    def _extract_queries_from_payload(self, data):
        queries = list()
        if not data:
            return queries

        for q in data.get("series_queries") or []:
            if isinstance(q, str):
                queries.append(q)
            elif isinstance(q, dict) and "query" in q:
                queries.append(q["query"])
        return queries


class LimitsCheckHook(BaseHook):
    """
    Before making any api calls it check if request reached its deadline
    """

    def __init__(self, upstream_request_context: UpstreamRequestContext):
        self.upstream_request_context = upstream_request_context

    def before_request(self, request: Request) -> Request:
        self.upstream_request_context.assert_limits_not_exceeded()
        return super(LimitsCheckHook, self).before_request(request)

    def after_request(self, response: Response) -> Response:
        return super(LimitsCheckHook, self).after_request(response)


class SimpleApiCallMetricsHook(BaseHook):
    """Hook that collects timing metrics for api calls (service to service requests)"""

    def __init__(self, destination: str) -> None:
        super().__init__()
        self.timer = None
        self.destination = destination

    def before_request(self, request: Request) -> Request:
        self.timer = metrics.timer('s2s_request', destination=self.destination)
        return request

    def after_request(self, response: Response) -> Response:
        t = self.timer
        if t is not None:
            t.stop()
            self.timer = None
        return response



