import copy
import typing
from collections import OrderedDict, defaultdict
from datetime import datetime
import hashlib
from functools import reduce
from multiprocessing.dummy import Pool

import requests
import six
import sjts
from werkzeug.utils import cached_property

from sjtoolkit.exceptions import ExpressionException, UnexpectedException
from sjtoolkit.expressions.utils import array_convert_tz, validate_pre_post_chains
from sjtoolkit.series.args_parse import parse_series_id
from sjtoolkit.series.suffix_processors import (
    operators_to_functions, adjust_dates, AGGREGATE_LEVELS, BaseOperator,
    LocalizeOperator, post_process_points
)
from sjtoolkit.utils.metrics import metrics
from sjtoolkit.utils.sentry import capture_message
from sjtoolkit.utils.trace import UpstreamRequestContext
from sjtoolkit.utils.utils import MsgPack, PERIODS_MAP, parse_date, MIN_LONG, MAX_LONG


def autocomplete_expr_runner(runner_host, auth, expression, account, line=None, column=None):
    resp = requests.get('{}/autocomplete'.format(runner_host), auth=auth,
                        headers={EXPRESSION_ACCOUNT_HOST_HEADER: account},
                        params={
                            'expression': expression, 'line': line, 'column': column,
                        })
    resp = MsgPack.loads(resp.content)
    if not resp['success']:
        raise ExpressionException(resp['description'])
    return resp


EXPRESSION_ACCOUNT_HOST_HEADER = "Sj-Account-Host"
EXPRESSION_API_TIMEOUT = 300


class ExpressionPointsBatchReader:
    def __init__(self,
                 calculator,
                 expr_runner_url,
                 account_host,
                 auth,
                 account_id=None,
                 datatype="series",
                 upstream_request_context: UpstreamRequestContext = None,
                 unavailable_error="Expression Runner not available; please contact support if this persists",
                 timeout=EXPRESSION_API_TIMEOUT,
                 account_settings=None,
                 debug=False,
        ):
        """
        Bulk expressions executor

        :param calculator: Instance of RemoteExpressionCalculator or its subclasses
        :param expr_runner_url: Expression runner URL
        :param account_host: Current account URL
        :param auth: User's credentials
        :param account_id: Account id
        :param datatype: Type of result - 'series' for single series, 'dataframe' - for multiple series result
        :param upstream_request_context: UpstreamRequestContext instance
        :param unavailable_error: Error message for timeout errors
        :param timeout: Expression request timeout
        :param account_settings: Account settings
        :param debug: will return xpr trace if True
        """
        self.expr_runner_url = expr_runner_url
        self.account_host = account_host
        self.auth = auth
        self.datatype = datatype
        self.upstream_request_context = upstream_request_context
        self.unavailable_error = unavailable_error
        self.timeout = timeout
        self.account_settings = account_settings
        self.debug = debug
        self.account_id = account_id
        self.calculator = calculator

        self._requests = defaultdict(list)
        self._results = {}  # dict hashed by uid or tuple of (uid, df, dt) for special cases

    def add_request(self,
                    expression,
                    df="MIN",
                    dt="MAX",
                    date_format="milli",
                    max_points=0,
                    g_expression=None,
                    operators=None,
                    fields=None,
                    include_job=False,
                    include_timestamp=False,
                    is_xpr_series=False,
                    debug=False,
                    sid=None,
                    series_body=None):
        """

        :param expression: Shooju expression to execute
        :param df: Points date from
        :param dt: Points date to
        :param date_format: only used to parse df, dt parameters. Result is always milli
        :param max_points: number of points to return
        :param g_expression: G expression code
        :param operators: List of PRE operators
        :param fields: List of fields to return with expression
        :param include_job: Includes points job ids if true
        :param include_timestamp: Includes points timestamp if true
        :param is_xpr_series: Indicates if request is for XPR series
        :param sid: Series ID of XPR series (makes sense only if is_xpr_series=True)
        :param series_body: Body if XPR series (makes sense only if is_xpr_series=True)
        :param debug: Return debug trace
        """

        df, dt = self._parse_dates(df, dt, date_format)
        self._requests[(expression, g_expression, tuple(operators), is_xpr_series, sid)].append(
            _XPRPointsRequest(
                df,
                dt,
                max_points,
                include_timestamp,
                include_job,
                debug,
                reversed=dt < df,
                fields=fields,
                series_body=series_body,
            )
        )
        return True

    def get_response(self, expression,
                     df="MIN",
                     dt="MAX",
                     date_format="milli",
                     max_points=0,
                     g_expression=None,
                     operators=None,
                     fields=None,
                     include_job=False,
                     include_timestamp=False,
                     is_xpr_series=False,
                     sid=None,
                     debug=False, ):
        """
        :param expression: Shooju expression to execute
        :param df: Points date from
        :param dt: Points date to
        :param date_format: only used to parse df, dt parameters. Result is always milli
        :param max_points: number of points to return
        :param g_expression: G expression code
        :param operators: List of PRE operators
        :param fields: List of fields to return with expression
        :param include_job: Includes points job ids if true
        :param include_timestamp: Includes points timestamp if true
        :param is_xpr_series: Indicates if request is for XPR series
        :param sid: Series ID of XPR series (makes sense only if is_xpr_series=True)
        :param debug: Return debug trace"""

        res = self._results.get((expression, g_expression, tuple(operators), is_xpr_series, sid))
        if  not res or isinstance(res, Exception) or not res.get('series'):
            return res

        ser = res['series'][0]
        points = ser.get('points')
        if points is None:
            return res

        points, points_stats = post_process_points(
            points,
            [],
            'milli',
            include_timestamp=include_timestamp,
            include_job=include_job,
            df=df,
            dt=dt,
        )
        processed_points = points[:max_points] if max_points != -1 else points
        res_copy = copy.deepcopy(res)
        if len(processed_points) == 0:
            res_copy['series'][0].pop('points', None)
        else:
            res_copy['series'][0]['points'] = processed_points

        return res_copy

    def fetch(self):
        """
        Fetches all added expression requests
        """
        if not self._requests:
            return 0

        tasks_pool = ExecutableTaskPool()

        for (xpr, g_expression, operators, is_xpr_series, sid), requests in six.iteritems(self._requests):
            max_points = max(r.size for r in requests) if len(requests) == 1 else -1
            min_dt = min(min(r.dt for r in requests), min(r.df for r in requests))
            max_dt = max(max(r.dt for r in requests), max(r.df for r in requests))
            min_dt = max(MIN_LONG, min_dt)
            max_dt = min(MAX_LONG, max_dt)
            include_job = any(r.include_job for r in requests)
            include_timestamp = any(r.include_timestamp for r in requests)
            debug = any(r.debug for r in requests)
            reversed_flags = set(r.reversed for r in requests)
            fields = list(set(reduce(lambda a, b: a + b, [r.fields for r in requests])))
            non_empty_series_body = [r.series_body for r in requests if r.series_body]
            series_body = None if not is_xpr_series or not non_empty_series_body else non_empty_series_body[0]

            # flip min_dt/max_dt if this is a single request with reversed df/dt
            if len(requests) == 1 and any(reversed_flags) and max_points != -1:
                min_dt, max_dt = max_dt, min_dt

            min_dt = self._special_dt_value(min_dt)
            max_dt = self._special_dt_value(max_dt)

            expression_calculator = self.calculator(
                xpr[1:],
                self.expr_runner_url,
                self.account_host,
                datatype=self.datatype,
                auth=self.auth,
                df=min_dt, dt=max_dt,
                date_format='milli',
                g_expression=g_expression,
                max_points=max_points,
                include_job=include_job,
                include_timestamp=include_timestamp,
                operators=operators,
                upstream_request_context=self.upstream_request_context,
                account_settings=self.account_settings,
                account_id=self.account_id,
                is_xpr_series=is_xpr_series,
                debug=debug,
                fields=fields,
                series_body=series_body,
            )
            tasks_pool.add((xpr, g_expression, operators, is_xpr_series, sid), expression_calculator)

        with metrics.timer('bulk_xpr_fetch'):
            for key, (res, exception) in six.iteritems(tasks_pool.execute()):
                self._results[key] = res if not exception else exception

        return len(self._results)

    def _parse_dates(self, df, dt, date_format):
        date_from = parse_date(df, date_format)
        date_to = parse_date(dt, date_format)
        return date_from, date_to

    @staticmethod
    def _special_dt_value(dt):
        if dt == MIN_LONG:
            return 'MIN'
        if dt == MAX_LONG:
            return 'MAX'
        return dt


class _XPRPointsRequest(typing.NamedTuple):
    """
    Internal class for ExpressionPointsBatchReader purposes
    """
    df: str
    dt: str
    size: int
    include_timestamp: int
    include_job: int
    debug:bool
    reversed: bool
    fields: typing.List[str]
    series_body: None


class RemoteExpressionCalculator(object):
    def __init__(
        self,
        expression,
        runner_host,
        account_host,
        auth,
        datatype="series",
        df="MIN",
        dt="MAX",
        date_format="milli",
        max_points=0,
        g_expression=None,
        operators=None,
        fields=None,
        request_id=None,
        upstream_request_context: UpstreamRequestContext=None,
        include_job=False,
        include_timestamp=False,
        max_series=None,
        sort=None,
        unavailable_error="Expression Runner not available; please contact support if this persists",
        timeout_error=f"Expression execution took longer than {EXPRESSION_API_TIMEOUT}s",
        timeout=EXPRESSION_API_TIMEOUT,
        account_settings=None,
        series_body=None,
        is_xpr_series=None,
        debug=False,
        extra_headers=None,
        repdate_s3=False,
    ):
        """Calculates expression result against against runner_host using auth.

        :param expression: Shooju expression to execute
        :param runner_host: Expression runner url
        :param account_host: Shooju host for current account
        :param auth: User credentials that will be used for expression runner calls
        :param datatype: Sets expression context: series or dataframe
        :param df: provides default for sj* funcs
        :param dt: provides default for sj* funcs
        :param date_format: only used to parse df, dt parameters. Result is always milli
        :param max_points: provides default for sj* funcs
        :param g_expression: Global expression code
        :param fields: List of fields to return with expression result
        :param request_id: Api request id
        :param upstream_request_context: UpstreamReqeustContext object
        :param include_job: Includes points job ids if true
        :param include_timestamp: Includes points timestamp if true
        :param max_series: Maximum number of series returned by underlying data read process
        :param sort: Underlying /series?query= sort order
        :param unavailable_error: Default error text when expression runner is unavailable
        :param timeout_error: Default error text when expression execution timed out
        :param timeout: Expression runner api call timeout
        :param series_body: Series body (if XPR series). Needed to apply operators.x
        :param is_xpr_series: Indicates if this is a xpr series. This defines default behaviour of @post/@pre operators.
        :param debug: Indicates expression should return its debug traceback.
        :param repdate_s3: Flag indicating reported points uses S3 backend as a storage.

        """
        assert is_xpr_series is not None  # just to make sure we didn't miss any cases
        self._original_expression = expression
        self._original_operators = operators
        expression, expression_suffix_operators = parse_series_id(
            expression, skip_validate=True
        )
        self.expression = expression
        self.runner_host = runner_host
        self.account_host = account_host
        self.auth = auth
        self.datatype = datatype
        self.df = df
        self.dt = dt
        self.date_format = date_format
        self.max_points = max_points
        self.g_expression = g_expression
        self.is_xpr_series = is_xpr_series
        self.request_id = request_id
        self.repdate_s3 = repdate_s3
        if not is_xpr_series:
            self.pre_operators, self.post_operators = self._parse_expression_operators(
                list(expression_suffix_operators or []) + list(operators or []),
                pre_is_default=True,
            )
        else:
            # means we applying on xpr series result unless explicitly asked for different
            self.pre_operators, self.post_operators = self._parse_expression_operators(
                operators, pre_is_default=False
            )
            # but inside expression we follow common rules where pre is default
            (
                exp_suff_pre_operators,
                exp_suff_post_operators,
            ) = self._parse_expression_operators(
                expression_suffix_operators, pre_is_default=True
            )

            if (
                self.pre_operators and self.pre_operators[0] == "pre"
            ):  # try place it after explicit pre
                self.pre_operators = (
                    self.pre_operators[:1]
                    + [e for e in exp_suff_pre_operators if e != "pre"]
                    + self.pre_operators[1:]
                )
            else:
                self.pre_operators = self.pre_operators = (
                    exp_suff_pre_operators + self.pre_operators
                )

            if (
                self.post_operators and self.post_operators[0] == "post"
            ):  # try place it after explicit post
                self.post_operators = (
                    self.post_operators[:1]
                    + [e for e in exp_suff_post_operators if e != "pre"]
                    + self.post_operators[1:]
                )
            else:
                self.post_operators = exp_suff_post_operators + self.post_operators

        self.fields = fields
        self.series_body = series_body

        self.upstream_request_context = upstream_request_context
        self.include_job = include_job
        self.include_timestamp = include_timestamp
        self.max_series = max_series
        self.sort = sort
        self.unavailable_error = unavailable_error
        self.timeout_error = timeout_error
        self.timeout = timeout
        self.account_settings = account_settings
        self.debug = debug
        self.extra_headers = extra_headers

    def execute(self):
        """Executes remote call. Flask g can't be used inside this function when executed in a tasks pool."""
        metrics.count('xpr_exec')
        url = "{}/exec/{}".format(self.runner_host.rstrip("/"), self.datatype)
        headers = {EXPRESSION_ACCOUNT_HOST_HEADER: self.account_host}
        if self.extra_headers is not None and type(self.extra_headers) is dict:
            headers.update(self.extra_headers)
        post_operators_functions = operators_to_functions(
            self.post_operators or [],
            self.series_body,
            account_settings=self.account_settings,
        )
        max_points = self.max_points
        if post_operators_functions:
            df_adj, dt_adj = adjust_dates(
                self.df, self.dt, post_operators_functions, self.date_format
            )
            date_format = "milli"
            max_points = (
                -1
            )  # we have to read all points in adjusted date range to correctly apply operators
        else:
            df_adj, dt_adj = self.df, self.dt
            date_format = self.date_format

        # final check before make a call
        self.upstream_request_context.assert_limits_not_exceeded()

        try:
            resp = requests.post(
                url,
                auth=self.auth,
                headers=headers,
                json={
                    "expression": self.expression,
                    "df": df_adj,
                    "dt": dt_adj,
                    "max_points": max_points,
                    "date_format": date_format,
                    "pre_operators": "@".join(self.pre_operators)
                    if self.pre_operators
                    else None,
                    "post_operators": "@".join(self.post_operators)
                    if self.post_operators
                    else None,
                    "include_job": self.include_job,
                    "include_timestamp": self.include_timestamp,
                    "context": self.datatype,
                    "g_expression": self.g_expression,
                    "main_request_id": self.request_id,
                    "max_series": self.max_series,
                    "sort": self.sort,
                    "fields": ",".join(self.fields) if self.fields else None,
                    "debug": self.debug,
                    "repdate_s3": self.repdate_s3,
                },
                timeout=self.timeout,
                params={"upstream_request_context": self.upstream_request_context.raw}
            )
        except requests.exceptions.Timeout as e:
            raise e.__class__(self.timeout_error)
        except requests.exceptions.RequestException as e:
            raise e.__class__(self.unavailable_error)
        if resp.status_code != requests.status_codes.codes.ok:
            raise UnexpectedException(f"expression runner failure {resp.status_code}; contact support")
        elif not resp.content.startswith(b"SJTS"):
            raise UnexpectedException("malformed expression runner response")

        resp = sjts.loads(resp.content, use_numpy=True)
        self._apply_operators(
            resp, df_adj, dt_adj, date_format, post_operators_functions
        )
        if not resp["success"]:
            raise ExpressionException(resp["description"], resp.get("trace") if self.debug else None)

        return resp

    def _apply_operators(
        self, resp, df_adj, dt_adj, date_format, post_operators_functions
    ):
        if not self.post_operators or not resp["success"]:
            return
        localize_ops = post_operators_functions.get_by_name("localize") or []
        force_tz_ops = post_operators_functions.get_by_name("forcetz") or []
        global_ops_tzs = [o.get_timezone() for o in localize_ops]
        global_ops_tz_names = {tz.zone for tz in global_ops_tzs if tz}
        force_tz_ops_names = {op.get_timezone().zone for op in force_tz_ops if op.get_timezone()}

        if len(global_ops_tz_names) > 1 or len(force_tz_ops_names) > 1:
            raise ExpressionException(
                "got series in different timezones: %s"
                % ", ".join(sorted(global_ops_tz_names))
            )

        global_tz = list(global_ops_tz_names)[0] if global_ops_tz_names else None
        force_tz = list(force_tz_ops_names)[0] if force_tz_ops_names else None
        over_hourly_agg = False
        # treat it's as a naive if aggregating to daily, monthly, etc
        if any(
            [
                o.agg_level > AGGREGATE_LEVELS["h"]
                for o in post_operators_functions
                if isinstance(o, BaseOperator) and o.agg_level
            ]
        ):
            over_hourly_agg = True

        for s in resp["series"]:
            points = s.get("points")
            s['series_id'] = '{}@{}'.format(s['series_id'], '@'.join(self.post_operators))

            result_tz = s.get("tz", {}).get("timezone")
            target_tz = global_tz or result_tz

            if points is None or len(points) == 0:
                s.pop("tz", None)
                if force_tz:
                    s["tz"] = {"timezone": force_tz}
                elif target_tz and not over_hourly_agg:
                    s["tz"] = {"timezone": target_tz}
                continue

            ops_to_apply = post_operators_functions
            if result_tz and result_tz != "UTC":
                # we will re-apply @localize operator (needed to correctly apply some agg operators)
                # convert to UTC first
                ops_to_apply = [
                    o
                    for o in post_operators_functions
                    if not isinstance(o, LocalizeOperator)
                ]
                points = array_convert_tz(points, result_tz, "UTC")
                # either back to original tz, or to target global @localize tz (of was applied)
                raw_operators = "".join(
                    ["localize:{}".format(target_tz)] + list(map(str, ops_to_apply))
                )
                ops_to_apply = operators_to_functions(
                    raw_operators.split("@"), {}, self.account_settings
                )

                if result_tz and global_tz:  # Double localize. need to send warning to user.
                    s['warnings'] = ["series has been localized twice"]

            s["points"], _ = post_process_points(
                points,
                ops_to_apply,
                date_format,
                self.include_timestamp,
                self.include_job,
                df_adj,
                dt_adj,
            )

            s.pop("tz", None)
            if force_tz:
                s["tz"] = {"timezone": force_tz}
            elif target_tz and not over_hourly_agg:
                s["tz"] = {"timezone": target_tz}

    def _parse_expression_operators(self, operators, pre_is_default=True):
        """
        Splits array of operators to pre and post lists of operators
        """
        operators = operators or []

        pre_chain, post_chain = [], []
        current_chain = pre_chain if pre_is_default else post_chain

        pre_or_post_used = False
        for op in operators:
            op_lower = op.lower()
            if current_chain is post_chain:
                self._validate_can_be_used_in_post_chain(op)
            # Uncomment when communicated to the clients, disable @pre@repfdate on xpr series
            # elif current_chain is pre_chain:
            #    self._validate_can_be_used_in_pre_chain(op)

            if op_lower == "pre" and post_chain:
                raise ExpressionException("@pre operator must be applied before @post")

            if op_lower == "pre":
                current_chain = pre_chain
                pre_or_post_used = True
            elif op_lower == "post":
                current_chain = post_chain
                pre_or_post_used = True
            current_chain.append(op)

        if pre_or_post_used and pre_chain:
            validate_pre_post_chains(pre_chain)
        if pre_or_post_used and post_chain:
            validate_pre_post_chains(post_chain)

        return pre_chain, post_chain

    def _validate_can_be_used_in_post_chain(self, op):
        op, op_params = BaseOperator.split_op(op)
        op = op.lower()
        disallowed_for_any = {"asof", "repdate", "repfdate", "reppdate"}
        if self.is_xpr_series:
            disallowed_for_any.remove("repfdate")
        # we allow asof for xpr series. In this case we use snapshotted xpr.expression as a formula
        if op in disallowed_for_any:
            capture_message(
                "@{} operator applied as post-operator".format(op),
                extra={
                    "expression": self._original_expression,
                    "operators": self._original_operators,
                    "request_id": self.request_id,
                },
            )

            raise ExpressionException(
                "@{} can be applied only after @pre in expressions".format(op)
            )
        elif not self.is_xpr_series and op == "localize" and not op_params:
            raise ExpressionException(
                "must set explicit timezone if using @localize after @post"
            )
        elif op == 'filter' and op_params and (op_params[0].startswith('job') or op_params[0].startswith('ts')):
            raise ExpressionException(
                "can't use filter on job or timestamps after @post"
            )

    def _validate_can_be_used_in_pre_chain(self, op):
        op, op_params = BaseOperator.split_op(op)
        op = op.lower()
        if self.is_xpr_series:
            disallowed = {"repfdate"}

            if op in disallowed:
                capture_message(
                    "@{} operator applied as pre-operator".format(op),
                    extra={
                        "expression": self._original_expression,
                        "operators": self._original_operators,
                        "request_id": self.request_id,
                    },
                )

                raise ExpressionException(
                    "@{} can be applied only after @post in xpr series,"
                    " use underlying expression if you want reported fields from underlying series".format(op)
                )


class CachedRemoteExpressionCalculator(RemoteExpressionCalculator):
    def __init__(self, backend, *args, **kwargs):
        cache_ttl_limit = kwargs.pop("cache_ttl_limit", None)
        super(CachedRemoteExpressionCalculator, self).__init__(*args, **kwargs)

        params_set = set()

        # if @cached used in @pre context it will go thru all consequent expression calls
        self.post_operators = self._extract_cached_operators(
            self.post_operators, params_set
        )
        self.pre_operators = self._extract_cached_operators(
            self.pre_operators, params_set
        )

        read_only, ttl, reset_ttl, invalidate = self._parse_operator_params(params_set)

        if cache_ttl_limit and ttl:
            ttl = min(ttl, cache_ttl_limit)

        self._cache_read_only = read_only
        self._cache_ttl = ttl
        self._cache_invalidate = invalidate
        self._cache_reset_ttl = reset_ttl

        self._backend = backend

    @classmethod
    def _parse_operator_params(cls, params):
        read_only = None in params
        reset_ttl = 'resetTtl' in params
        invalidate = 'invalidate' in params

        params.discard(None)
        params.discard('resetTtl')
        params.discard('invalidate')

        if len(params) > 1 or params and read_only:
            raise ExpressionException(
                "Cached operator is used with different TTL parameters"
            )

        ttl = None
        if params:
            ttl = cls._parse_ttl_param(params.pop())

        return read_only, ttl, reset_ttl, invalidate

    @staticmethod
    def _parse_ttl_param(ttl):
        if not ttl:
            raise ExpressionException("Empty cache TTL param value")

        value, units = ttl[:-1], ttl[-1]

        if not value:
            value = 1

        relative_delta = PERIODS_MAP.get(units)
        if relative_delta:
            t = datetime(1970, 1, 1)
            seconds = int((t - (t - relative_delta)).total_seconds())
        else:
            raise ExpressionException("Unexpected cache TTL value {}".format(ttl))

        return int(value) * seconds

    @staticmethod
    def _extract_cached_operators(ops, params_set):
        if not ops:
            return ops
        res = []
        for o in ops:
            parts = o.split(":")
            if parts[0].lower() == "cached":
                if len(parts) > 2:
                    raise ExpressionException(
                        "invalid parameter format for cached operator"
                    )
                if len(parts) == 2:
                    has_hours = False
                    for value in parts[1].split(','):
                        if value not in ['resetTtl', 'invalidate']:
                            has_hours = True
                        params_set.add(value)
                    if not has_hours:
                        params_set.add(None)

                else:
                    params_set.add(None)
            res.append(o)
        return res

    def execute(self):
        if not any([self._cache_ttl, self._cache_read_only, self._cache_invalidate, self._cache_reset_ttl]):
            return super(CachedRemoteExpressionCalculator, self).execute()

        resp, df, dt, fields, ttl, ttl_left = self._get_cached()

        if not self._cache_invalidate:
            if resp is not None and self._df_dt_in_range(df, dt) and set(fields) == set(self.fields):
                # can use cached data
                cache_hit = True
                series_ttl = ttl_left
                if self._cache_reset_ttl:
                    series_ttl = self._cache_ttl or ttl
                    if series_ttl:
                        self._backend.reset_ttl(self._cache_key, series_ttl)

                resp = self._cut_response(
                    resp, self.max_points, self.include_job, self.include_timestamp
                )
                return self._set_series_xpr(resp, series_ttl, cache_hit)

        orig_max_points = self.max_points
        orig_include_job = self.include_job
        orig_include_timestamp = self.include_timestamp

        self.max_points = -1
        self.include_job = True
        self.include_timestamp = True
        resp = super(CachedRemoteExpressionCalculator, self).execute()
        cache_hit = False
        if not resp["success"]:
            return resp

        if self._cache_invalidate:
            if self._cache_reset_ttl:
                ttl = self._cache_ttl
            else:
                ttl = ttl_left
        else:
            ttl = self._cache_ttl

        if ttl:
            self._backend.set(
                self._cache_key,
                resp,
                {
                    "df": parse_date(self.df, self.date_format),
                    "dt": parse_date(self.dt, self.date_format),
                    "fields": self.fields,
                    "ttl": ttl,
                },
                ttl,
            )

        resp = self._cut_response(
            resp, orig_max_points, orig_include_job, orig_include_timestamp
        )
        return self._set_series_xpr(resp, ttl, cache_hit)

    @staticmethod
    def _set_series_xpr(resp, ttl, cache_hit):
        # cache_hit
        if cache_hit:
            for s in resp.get('series', []):
                xpr = s.get('xpr', {})
                if 'trace' not in xpr:
                    continue
                xpr['trace']['cache_hit'] = cache_hit
                s['xpr'] = xpr

        # ttl ( if not cached operator ttl if cached ttl_left )
        if ttl:
            for s in resp.get('series', []):
                xpr = s.get('xpr', {})
                xpr['cached'] = ttl
                s['xpr'] = xpr

        return resp

    def _cut_response(self, resp, max_points, include_job, include_timestamp):
        for s in resp["series"]:
            points = s.get("points")
            if points is None:
                continue

            post_request_operators_functions = operators_to_functions(
                self.post_operators or [],
                self.series_body,
                account_settings=self.account_settings,
            )
            if post_request_operators_functions:
                df_adj, dt_adj = adjust_dates(
                    self.df, self.dt, post_request_operators_functions, self.date_format
                )
                date_format = "milli"
            else:
                df_adj = parse_date(self.df, self.date_format)
                dt_adj = parse_date(self.dt, self.date_format)
                date_format = "milli"

            if not include_job or not include_timestamp:
                columns = list(points.dtype.names)
                if not include_job and "jobs" in columns:
                    columns.remove("jobs")
                if not include_timestamp and "ts" in columns:
                    columns.remove("ts")
                points = points[columns]

            s["points"], _ = post_process_points(
                points, [], date_format, include_timestamp, include_job, df_adj, dt_adj
            )

            if max_points == -1:
                s["points"] = points
            else:
                s["points"] = points[:max_points]

        return resp

    @cached_property
    def _cache_key(self):
        h = hashlib.md5((self.expression or '').encode())
        h.update((self.g_expression or '').encode())

        for op in sorted(o for o in self.pre_operators if not o.startswith('cached')):
            h.update(op.encode())
        for op in sorted(o for o in self.post_operators if not o.startswith('cached')):
            h.update(op.encode())

        return h.hexdigest()

    def _df_dt_in_range(self, df, dt):
        req_df = parse_date(self.df, self.date_format)
        req_dt = parse_date(self.dt, self.date_format)
        if df < dt:
            return df <= req_df and dt >= req_dt
        return df >= req_df and dt <= req_dt

    def _get_cached(self):
        resp, meta, ttl_left = self._backend.get(self._cache_key)
        if meta is None or resp is None:
            return None, None, None, None, None, None

        df, dt, fields, ttl = meta["df"], meta["dt"], meta["fields"], meta["ttl"]
        return resp, df, dt, fields, ttl, ttl_left


class ExecutableTaskPool(object):
    def __init__(self, max_threads=4):
        """Tasks pool. Executes tasks in parallel and keeps order of results corresponding to enqueuing order.
        :param max_threads: maximum number of threads to use in the pool.
        """
        self._max_threads = max_threads
        self._queue = OrderedDict()

    def add(self, key, executable):
        """Add new exeutable task to the pool's queue. Task should provide method 'execute'.

        :param key: unique identifier for the task
        :param executable: task instance
        """
        self._queue[key] = executable

    def execute(self):
        """Executes all tasks in the queue.
        :return: ordered dictionary with results by key mapping
        """
        results = OrderedDict()
        if not self._queue:
            return results

        pool = Pool(min(len(self._queue), self._max_threads))

        unordered_results = {
            key: (res, exception)
            for key, res, exception in pool.imap(
                lambda x: self._fetch(*x), six.iteritems(self._queue)
            )
        }
        for key in six.iterkeys(self._queue):
            results[key] = unordered_results[key]

        pool.close()
        pool.join()

        self._requests = OrderedDict()
        return results

    def _fetch(self, key, executable):
        try:
            res = executable.execute()
            exc = None
        except Exception as e:
            exc = e
            res = None

        return key, res, exc
