import copy
import imp
import logging
import os
import sys
import tempfile
import threading
import time
import types
import json
from collections import defaultdict
from datetime import datetime, date
from functools import partial, reduce, wraps
from typing import MutableMapping

import numpy
import pandas
import pandas as pd
import pytz
import shooju
import six
from flask import g
from shooju import NANOSECONDS
from shooju.utils.convert import milli_tuple_naive_to_pandas_tz_aware
from werkzeug.local import LocalProxy
import re

from sjtoolkit.exceptions import ExpressionException
from sjtoolkit.expressions.autocomplete import AUTOCOMPLETE_SYSPATH_ROOT
from sjtoolkit.expressions.utils import (
    operators_chain_rebuild,
    pandas_series_to_milli_points,
)
from sjtoolkit.series.args_parse import parse_series_query
from sjtoolkit.series.suffix_processors import (
    AggOperator,
    WeightedAverageOperator,
    TrailingOperator,
    LagOperator,
    FilterOperator,
    NoOp,
    operators_to_functions,
    reorder_operators,
    BaseOperator,
    AGGREGATE_LEVELS,
    AGG_DTYPE,
)
from sjtoolkit.utils.decorators import memoize
from sjtoolkit.utils.sjutils import BaseSJUtils
from sjtoolkit.utils.utils import millis_utc_now

logger = logging.getLogger("expression_runner")

UTC = pytz.utc
MAX_POINTS_IN_DF = 15 * 1000000
DF_SCROLL_BATCH_SIZE = 200
KEY_RE = re.compile(r'\{([\w\d\.]+)}')

ACCOUNT_CONFIG_RENEW_EVERY = 60 * 10


class ImporterException(Exception):
    pass


class ThreadLocalDict(MutableMapping):
    """The dict whose values are local to current thread."""

    def __init__(self) -> None:
        self.__local = threading.local()

    def __setitem__(self, k, v) -> None:
        return self.__local_dict.__setitem__(k, v)

    def clear(self):
        return self.__local_dict.clear()

    def __delitem__(self, v):
        return self.__local_dict.__delitem__(v)

    def pop(self, *args, **kwargs):
        return self.__local_dict.pop(*args, **kwargs)

    def popitem(self):
        return self.__local_dict.popitem()

    def setdefault(self, *args, **kwargs):
        return self.__local_dict.setdefault(*args, **kwargs)

    def update(self, *args, **kwargs):
        return self.__local_dict.update(*args, **kwargs)

    def __getitem__(self, k):
        return self.__local_dict.__getitem__(k)

    def get(self, *args, **kwargs):
        return self.__local_dict.get(*args, **kwargs)

    def items(self):
        return self.__local_dict.items()

    def keys(self):
        return self.__local_dict.keys()

    def values(self):
        return self.__local_dict.values()

    def __contains__(self, o):
        return self.__local_dict.__contains__(o)

    def __iter__(self):
        return self.__local_dict.__iter__()

    def __len__(self):
        return self.__local_dict.__len__()

    def __repr__(self):
        return self.__local_dict.__repr__()

    def __str__(self):
        return self.__local_dict.__str__()

    @property
    def __local_dict(self):
        if not hasattr(self.__local, "dict"):
            self.__local.dict = dict()
        return self.__local.dict


class LazyModule(types.ModuleType):
    """Python module that initializes its namespaces lazily on attribute access"""

    def __init__(self, name, xpr_ctx, g_expression, ns, dmin, dmax):
        self.__xpr_ctx = xpr_ctx
        self.__module = None
        self.__dmin = dmin
        self.__dmax = dmax
        self.__g_expression = g_expression
        self.__ns = ns
        self.__ns["G"] = object()
        super(LazyModule, self).__init__(name)
        self._g_funcs = {
            'dfFromVars': self._df_from_vars,
            'df': self._df_from_vars
        }
    def __getattr__(self, item):
        if item in self._g_funcs:
            return self._g_funcs[item]
        self.__init_module()
        return getattr(self.__module.__dict__["G"], item)

    def __init_module(self):
        if self.__module is None:
            self.__module = imp.new_module(self.__name__)
            self.__module.__dict__.update(self.__ns)
            proc_code = compile(self.__g_expression, self.__name__, "exec")
            exec(proc_code, self.__module.__dict__)

    def _df_from_vars(self, underscore_to_space=True, ignore_series_not_found_errors=False):
        self.__ns['sjs'].set_ignore_series_not_found(ignore_series_not_found_errors)
        if self.__module:
            raise ExpressionException(
                'G.df() cannot be used with other G.* functions/attributes within the same expression'
            )

        self.__init_module()
        data = {}
        fields = {}
        constants = {}
        # series Series.name here is same as series query it was read by
        series_names_to_variables = dict()

        xpr_ctx = self.__xpr_ctx
        underlying_series = {}

        for name, df_or_series in self.__module.__dict__.items():
            if name.startswith('_'):
                continue
            if underscore_to_space:
                name = name.replace('_', ' ')

            if isinstance(df_or_series, pandas.DataFrame):
                if hasattr(df_or_series, 'fields') and df_or_series.fields is not None:
                    df_underlying_series = df_or_series.get_underlying_series()
                else:
                    df_underlying_series = {}
                for col, ser in df_or_series.items():
                    data[f'{name}: {col}'] = ser
                    series_names_to_variables[ser.name] = f'{name}: {col}'
                    if hasattr(df_or_series, 'fields'):  # if DataFrameWithFields
                        fields[f'{name}: {col}'] = copy.deepcopy(
                            df_or_series.fields.get(ser.name, df_or_series.fields.get('name_not_available', {}))
                        )
                        underlying_series[f'{name}: {col}'] = df_underlying_series.get(
                            col, df_underlying_series.get('name_not_available', []))
                    else:  # if for some reason returned pandas.DataFrame
                        fields[f'{name}: {col}'] = xpr_ctx.fields_by_sids.get(ser.name) or dict()
            elif isinstance(df_or_series, pandas.Series):
                if hasattr(df_or_series, 'name') and df_or_series.name == "series_not_found":
                    continue
                data[name] = df_or_series
                series_names_to_variables[f'${df_or_series.name}'] = name
                if hasattr(df_or_series, 'fields') and df_or_series.fields is not None:
                    ser_underlying_series = df_or_series.get_underlying_series()
                    fields[name] = copy.deepcopy(
                        df_or_series.fields.get(f'${df_or_series.name}',
                                                df_or_series.fields.get(df_or_series.name,  # with id_field
                                                                        df_or_series.fields.get('name_not_available',
                                                                                                {})))
                    )
                    underlying_series[name] = ser_underlying_series.get(
                        f'${df_or_series.name}', ser_underlying_series.get(
                            df_or_series.name, ser_underlying_series.get('name_not_available', [])))
                else:
                    fields[name] = xpr_ctx.fields_by_sids.get(df_or_series.name) or dict()
            elif type(df_or_series) in (int, float):
                constants[name] = df_or_series
                if hasattr(df_or_series, 'fields'):
                    fields[name] = df_or_series.fields.get('name_not_available', {})
                    underlying_series[name] = df_or_series.get_underlying_series().get('name_not_available', [])

        df = DataFrameWithFields(data)
        df.fields = fields or dict()
        self.__xpr_ctx.underlying_series = underlying_series
        index_for_constants = self._make_index_for_constants(df)
        if len(xpr_ctx.fields_by_sids) > 1:
            # replace Series.name (which is series queries) by var names to map fields correctly
            for series_query, fields in dict(xpr_ctx.fields_by_sids).items():
                var_name = series_names_to_variables.get(series_query)
                if var_name is not None:
                    xpr_ctx.fields_by_sids[var_name] = fields
                    xpr_ctx.fields_by_sids.pop(series_query)
                    xpr_ctx.all_series_ids.add(var_name)
                    xpr_ctx.all_series_ids.discard(series_query)

        for name, value in constants.items():
            df[name] = pandas.Series(
                [value] * len(index_for_constants),
                index=index_for_constants,
            )
            df.fields[name] = {}
            self.__xpr_ctx.underlying_series[name] = []

        # special req for g.df()
        for name, value in df.fields.items():
            value['_var'] = name

        return df

    def _make_index_for_constants(self, df):
        dmin, dmax = self.__dmin, self.__dmax

        if dmin and isinstance(df.index, pd.DatetimeIndex) and df.index.tzinfo is not None:
            dmin = dmin.astimezone(pytz.utc)

        if dmax and isinstance(df.index, pd.DatetimeIndex) and df.index.tzinfo is not None:
            dmax = dmax.astimezone(pytz.utc)

        if len(df) != 0:
            dmin = max(df.index.min(), dmin) if dmin else df.index.min()
            dmax = min(df.index.max(), dmax) if dmax else df.index.max()

        now = datetime.now()

        if dmin is None:
            dmin = now

        if dmax is None:
            dmax = now

        if dmin.tzinfo != pytz.utc:
            dmin = dmin.astimezone(pytz.utc)
        if dmax.tzinfo != pytz.utc:
            dmax = dmax.astimezone(pytz.utc)

        unique_dfdt = sorted({dmin, dmax})
        return df.index.__class__(data=unique_dfdt)


class AccountsManager(object):
    def __init__(self, accounts):
        self._loaders = {
            it["account_host"]: ImportersLoader(
                it["account_host"], (it["user"], it["api_key"]), self
            )
            for it in accounts
        }
        self._account_config_last_updated = defaultdict(int)
        self._configs = {
            it["account_host"]: self._load_config(
                it["account_host"],
                (it["user"], it["api_key"]),
            )
            for it in accounts
        }

        self._accounts_creds = {
            it["account_host"]: it for it in accounts
        }

        self._global_functions = dict()
        for account_host, acc_config in six.iteritems(self._configs):
            self._global_functions[account_host] = Functions(
                account_settings=acc_config
            )

    def get_account_importers(self, account):
        """Returns account data (loaded libs, processors etc) by its id"""
        return self._loaders[account]

    def load_namespaces(self, full=False):
        """Reloads accounts namespaces"""
        for it in six.itervalues(self._loaders):
            it.load_namespaces(full)

    def get_account_config(self, account):
        """
        Return publicly available account settings
        """
        if time.time() - self._account_config_last_updated[account] > ACCOUNT_CONFIG_RENEW_EVERY:
            try:
                # this also updates last updated time
                self._configs[account] = self._load_config(
                    account,
                    (self._accounts_creds[account]['user'], self._accounts_creds[account]['api_key']),
                )
            except Exception:
                logger.exception("failed to renew account config, keep using old one")

        return self._configs[account]

    def _load_config(self, account, auth):
        # we don't check user at public endpoints, but Connection doesn't like empty user/pass

        cfg = shooju.Connection(account, *auth).raw.get("/account/config")[
            "account_config"
        ]

        self._account_config_last_updated[account] = time.time()
        return cfg

    def get_account_global_funcs(self, account):
        """
        Return publicly available account settings
        """
        return self._global_functions[account]


class ImportersLoader(object):
    def __init__(self, account, auth, accounts_manager):
        self._account = account
        self._auth = auth
        self._accounts_manager = accounts_manager
        self._users_syspaths = defaultdict(list)
        self._teams_to_processor = defaultdict(
            set
        )  # this maps to non-lib processor which team has access to
        self._processors_to_users = defaultdict(set)  # users that loaded that processor
        self._teams_to_users = defaultdict(list)
        self._users_namespaces = defaultdict(dict)
        self._processors_data = defaultdict(
            dict
        )  # this is mapping importer_id -> code, loads_proc_obj, teams
        self._lib_to_processor = defaultdict(
            set
        )  # we should reverse index to know which processor we should recursively reload if lib has changed

        self._last_updated_at = None

    @property
    def users_namespaces(self):
        return self._users_namespaces

    def get_user_sys_paths(self, user_id):
        return [os.path.abspath(p) for p in sys.path] + (
            self._users_syspaths.get(user_id) or []
        )

    def load_namespaces(self, full=False):
        """Prepares all user namespaces"""
        now = millis_utc_now()
        con = shooju.Connection(self._account, *self._auth)
        updated_processors_data = defaultdict(
            dict
        )  # collecting all here to update PROCESSORS_DATA atomically
        if full:
            self._last_updated_at = None

        _s = time.time()

        user = con.raw.get("/users/" + self._auth[0])["user"]
        teams = user.get("team", [])
        user_updated_at = user["updated_at"]

        if teams:
            if "_admin" in teams:
                query = ""
            else:
                query = "team_obj.code_editors=({})".format(",".join(teams))
                if self._processors_data:
                    query = "({} OR levels.l3=({}))".format(
                        query, ",".join([p for p in self._processors_data])
                    )

            if self._last_updated_at and (self._last_updated_at > user_updated_at):
                query += " updated_date>{}".format(self._last_updated_at)

            procs = [
                p
                for p in con.raw.get(
                    "/processors",
                    params={
                        "per_page": 10000,
                        "query": query,
                        "fields": "id,team_obj.expression_executors,team_obj.code_editors",
                    },
                )["results"]
            ]

            invalidated_ids = []
            teams = set(teams)
            if "_admin" in teams:
                ids = [
                    it["id"] for it in procs if it.get("team_obj.expression_executors")
                ]
                invalidated_ids = [
                    it["id"]
                    for it in procs
                    if not it.get("team_obj.expression_executors")
                ]
            else:
                ids = []
                for it in procs:
                    code_editors = set(it.get("team_obj.code_editors", []))
                    if not teams & code_editors:
                        invalidated_ids.append(it["id"])
                        continue
                    if it.get("team_obj.expression_executors"):
                        ids.append(it["id"])
                    else:
                        invalidated_ids.append(it["id"])
        else:
            invalidated_ids = self._processors_data.keys()
            ids = []

        # remove invalidated proc ids from all teams
        for team_procs in six.itervalues(self._teams_to_processor):
            team_procs -= set(invalidated_ids)

        for proc_id in invalidated_ids:
            self._processors_data.pop(proc_id, None)

        for proc_id in ids:
            proc_data = con.raw.get(
                "/processors/{}".format(proc_id),
                params={"fields": ",".join(["code", "loads_proc_obj", "team_obj"])},
            )["importer"]

            # None is just to show that this is default revision. otherwise hard to use mapping
            updated_processors_data[proc_id] = proc_data

            # remove all teams from memory
            for team_procs in six.itervalues(self._teams_to_processor):
                team_procs -= {proc_id}

            # then add again, so we actualized permissions
            for t in proc_data.get("team_obj", dict()).get("expression_executors", []):
                self._teams_to_processor[t].add(proc_id)

            # collect lib revisions to load and keep dependencies tree
            for lib in proc_data.get("loads_proc_obj", []):
                self._lib_to_processor[lib["proc_id"]].add(proc_id)

        self._processors_data.update(
            updated_processors_data
        )  # at this point changes applied, we are ready to load users
        all_processors = {k for k in six.iterkeys(self._processors_data)}
        all_processors.update(invalidated_ids)

        _s = time.time()
        # in case there are changed processors we have to check/reload namespaces of all the users
        users_to_update = self._get_updated_user_teams(con)

        # reload users that were affected
        users_affected_by_processor_change = set()
        for proc_id, proc_data in six.iteritems(updated_processors_data):
            users_affected_by_processor_change |= self._processors_to_users[proc_id]
            for t in proc_data.get("team_obj", dict()).get("expression_executors", []):
                for user_id in self._teams_to_users[t]:
                    users_affected_by_processor_change.add(user_id)
        for proc_id in invalidated_ids:
            users_affected_by_processor_change |= self._processors_to_users[proc_id]

        # update set of users that going to have their namespaces reloaded
        for user_id in users_affected_by_processor_change:
            if user_id not in users_to_update and user_id in self._users_namespaces:
                users_to_update[user_id] = self._users_namespaces[user_id]["team"]
        logger.info(
            "loaded {} updated processors and {} updated users".format(
                len(updated_processors_data), len(users_to_update)
            )
        )

        self._write_code_for_autocomplete()

        for user_id, teams in six.iteritems(users_to_update):
            ns = self._users_namespaces.get(user_id, dict()).get("ns", dict())

            processors_to_reload = (
                set()
            )  # on first call this is ALL processors that user has access to

            for proc_id in self._processors_data.keys():
                if (
                    proc_id in self._lib_to_processor
                ):  # this lib has changed, need to reload all procs which are using this
                    processors_to_reload |= self._lib_to_processor[proc_id]
                processors_to_reload.add(proc_id)

            processors_user_has_access_to = set()
            for team in teams:
                for proc_id in self._teams_to_processor.get(team, set()):
                    processors_user_has_access_to.add(proc_id)

            # unload all that user has no access anymore
            processors_to_unload = all_processors - processors_user_has_access_to
            processors_to_reload -= processors_to_unload  # just don't touch below if it is not already loaded

            ns = self._prepare_user_ns(ns, processors_to_reload, processors_to_unload)
            self._users_namespaces[user_id] = {
                "ns": ns,
                "team": teams,
                "loaded_processors": processors_to_reload,
                "caller_user": user_id,
            }
            self._users_syspaths[user_id] = [
                os.path.join(AUTOCOMPLETE_SYSPATH_ROOT, self._account_id_from_link(self._account), p)
                for p in processors_user_has_access_to
            ]

        # reload reverse team->user mapping
        if users_to_update:
            self._processors_to_users.clear()
            self._teams_to_users.clear()
            for user_id, user_data in six.iteritems(self._users_namespaces):
                for t in user_data["team"]:
                    self._teams_to_users[t].append(user_id)
                for proc_id in list(user_data["loaded_processors"]):
                    self._processors_to_users[proc_id].add(user_id)

        self._last_updated_at = now

    def _write_code_for_autocomplete(self):
        # write code into autocomplete dir
        for proc_id, proc_data in self._processors_data.items():
            code = proc_data["code"]
            imports = [
                p["proc_id"].upper() for p in proc_data.get("loads_proc_obj", [])
            ]
            code = u"{}\n{}".format(
                "\n".join(["import {}".format(i) for i in imports]), code
            )
            code_dir = os.path.join(AUTOCOMPLETE_SYSPATH_ROOT,
                                    self._account_id_from_link(self._account),
                                    proc_id)
            os.makedirs(code_dir, exist_ok=True)
            py_file_path = os.path.join(code_dir, "{}.py".format(proc_id.upper()))
            with open(py_file_path, "w") as f:
                f.write(code)

    def _account_id_from_link(self, link: str):
        return link.replace('http://', '') \
            .replace('https://', '') \
            .split('.')[0]

    def _load_processor_namespace(self, global_ns, proc_data, import_path=None):
        """Recursively loads processor with with all depended libs."""
        import_path = import_path or list()
        processor_ns = dict(global_ns)
        loads_proc_obj = proc_data.get("loads_proc_obj")
        if not loads_proc_obj:
            return processor_ns

        proc_ids = [p["proc_id"] for p in loads_proc_obj]

        for lib_id in proc_ids:
            if lib_id in import_path:
                # generating good message to help find recursive import
                msg = '"{}" could not import "{}" - recursive import detected (import chain: {})'.format(
                    import_path[-1], lib_id, " > ".join(import_path + [lib_id])
                )
                raise ImporterException(msg)
            lib_data = self._processors_data[lib_id]
            code = lib_data["code"]
            lib_module = ImportedShoojuProcessor(
                lib_id
            )  # create empty module

            # filling lib namespace
            if lib_data.get("loads_proc_obj"):
                # recursively imports dependencies
                lib_module_ns = self._load_processor_namespace(
                    global_ns, lib_data, import_path=import_path + [lib_id]
                )
            else:
                lib_module_ns = dict(global_ns)
            lib_module.__dict__.update(lib_module_ns)
            importer_code = compile(code, "<{}>".format(lib_id), "exec")

            exec(importer_code, lib_module.__dict__)
            processor_ns[lib_id.upper()] = lib_module
        return processor_ns

    def _get_updated_user_teams(self, con):
        """
        Returns mapping of user id -> teams array. By default returns only recently changed users.

        :param con: Shooju connection
        """
        # we force all in case there are any changed processors. otherwise we just need updated users
        teams_by_user = {}
        params = {"per_page": 10000}

        if self._last_updated_at:
            params["query"] = "updated_at>{}".format(self._last_updated_at)
        for user in con.raw.get("/users", params=params)["results"]:
            teams_by_user[user["id"]] = user.get("team") or []

        return teams_by_user

    def _prepare_user_ns(self, ns, processors_to_load, processors_to_unload):
        """
        Loads into user namespaces processors which this user has access to.
        To support incremental update, function ignores processors which are not in <processors_id>.
        """
        global_ns = {  # this is common scope for all libs
            "Point": shooju.Point,
            "ImporterException": ImporterException,
            "ExpressionException": ExpressionException,
            "tempfile": tempfile,
            # TODO: remove flask.g
            "sjclient": LocalProxy(lambda: g.user_ns_sj),  # lazily instantiate
            "settings": ThreadLocalDict(),
        }
        global_ns["sjutils"] = ExpressionsSjUtils(self._accounts_manager, global_ns)
        for proc_id in processors_to_load:
            proc_data = self._processors_data[proc_id]
            proc_module = ImportedShoojuProcessor(str(proc_id))  # create empty module
            proc_module.__dict__.update(
                self._load_processor_namespace(
                    global_ns, proc_data, import_path=[proc_id]
                )
            )
            proc_code = compile(proc_data["code"], "<{}>".format(proc_id), "exec")
            try:
                exec(proc_code, proc_module.__dict__)
            except Exception:
                logger.exception("Failed to exec processor code")
                continue
            ns[proc_id.upper()] = proc_module

        for proc_id in processors_to_unload:
            proc_id = proc_id.upper()
            if proc_id in ns:
                del ns[proc_id]
        ns.update(global_ns)
        return ns


class Functions(object):
    SUPPORTED_OPERATORS = [
        AggOperator,
        WeightedAverageOperator,
        TrailingOperator,
        LagOperator,
        FilterOperator,
        NoOp,
    ]

    AGG_DTYPE = numpy.dtype([("dates", "i8"), ("values", "f8")])

    def __init__(self, account_settings):
        self._account_settings = account_settings

    def to_dict(self):
        return {"dt": self.dt, "localize": self.localize, "sjop": self.sjop}

    @staticmethod
    def dt(year, month=1, day=1, hour=0, minute=0, second=0, microsecond=0):
        """Returns a GMT datetime."""
        return datetime(year, month, day, hour, minute, second, microsecond)

    @staticmethod
    def localize(*args, **kwargs):
        """
        We don't want to support Localize operator this way (as function), so making an informative error
        """
        raise Exception("localize() is not supported in expressions")

    def sjop(self, points, op):
        """Applies operators on a given points"""
        if not isinstance(op, list):
            ops = [o.strip() for o in op.split("@") if o.strip()]
        else:
            ops = [o.strip().lstrip("@") for o in op]
        op_funcs = operators_to_functions(ops, account_settings=self._account_settings)
        op_funcs = reorder_operators(op_funcs)

        for op in op_funcs:
            if type(op) not in self.SUPPORTED_OPERATORS:
                raise ExpressionException(
                    '"{}" operator is not supported in expressions context'.format(
                        op.name
                    )
                )

        is_pd_series = False
        if isinstance(points, pandas.Series):
            is_pd_series = True
            if hasattr(points, 'fields'):
                fields = points.fields
                points = DataFrameWithFields({points.name: points})
                points.fields = fields
            else:
                points = pandas.DataFrame({points.name: points})

        for func in op_funcs:
            points = self._apply_operator_func(points, func)

        if is_pd_series and hasattr(points, 'fields'):
            fields = points.fields
            points = SeriesWithFields(points[points.columns[0]])
            points.fields = fields
        elif is_pd_series:
            points = points[points.columns[0]]

        return points

    @staticmethod
    def _apply_operator_func(points, func):
        # since operators accept only numpy array we do each series separately
        data = dict()

        if (
            isinstance(func, BaseOperator)
            and func.agg_level is not None
            and func.agg_level > AGGREGATE_LEVELS["h"]
        ):
            tz = UTC
        else:
            tz = points.index.tzinfo or UTC

        if tz is None:
            tz = UTC

        for series_id, ts in six.iteritems(points):
            arr = pandas_series_to_milli_points(ts, tz=tz)
            arr = func(numpy.array(arr, dtype=AGG_DTYPE))
            ts = milli_tuple_naive_to_pandas_tz_aware(
                list(zip(arr["dates"], arr["values"])), tz=tz
            )
            data[
                u"{}@{}".format(series_id, func.op) if series_id is not None else None
            ] = ts

        if hasattr(points, 'fields'):
            fields = {u"{}@{}".format(k, func.op): v for k, v in points.fields.items()}
            df = DataFrameWithFields(data)
            df.fields = fields
            return df
        else:
            return pandas.DataFrame(data)


class ExpressionsSjUtils(BaseSJUtils):

    # we just silently ignore these if applying via @sjutils.expressionable(apply_operators=True)
    IGNORED_OPERATORS = ["localize", "asof", "repdate", "repfdate", "reppdate", "cal"]

    def __init__(self, accounts_manager, *args, **kwargs):
        super(ExpressionsSjUtils, self).__init__(*args, **kwargs)
        self._accounts_manager = accounts_manager

    def expressionable(self, *args, **kwargs):
        """
        By wrapping function in a processor, this decorator makes the function allowed to be used in expressions.
        :param apply_operators: `[default: False]` if True global operators will be applied on expression result
        :return:
        """

        apply_operators = kwargs.get("apply_operators", False)

        def _deco(func):
            def _wrapped(*args, **kwargs):
                res = func(*args, **kwargs)
                if isinstance(res, pandas.Series) and res.__class__ is not SeriesWithFields:
                    res.__class__ = SeriesWithFields  # convert to SeriesWithFields
                    res.fields = dict(self.expression_ctx.fields_by_sids)
                    res.process_fields()
                elif isinstance(res, pandas.DataFrame) and res.__class__ is not DataFrameWithFields:
                    res.__class__ = DataFrameWithFields
                    res.fields = dict(self.expression_ctx.fields_by_sids)
                    res.process_fields()
                elif isinstance(res, (int, numpy.integer)) and res.__class__ is not IntWithFields:
                    res = IntWithFields(res)
                    res.fields = dict(self.expression_ctx.fields_by_sids)
                    res.process_fields()
                elif isinstance(res, (float, numpy.float_)) and  res.__class__ is not FloatWithFields:
                    res = FloatWithFields(res)
                    res.fields = dict(self.expression_ctx.fields_by_sids)
                    res.process_fields()
                self.expression_ctx.fields_by_sids.clear()

                acc_global_funcs = Functions(self.account_config)
                if (
                    apply_operators
                    and self.expression_operators
                    and (
                        isinstance(res, pandas.DataFrame)
                        or isinstance(res, pandas.Series)
                    )
                ):
                    supported_ops = [
                        o
                        for o in self.expression_operators.split("@")
                        if o.strip()
                        and o.lower().split(":")[0] not in self.IGNORED_OPERATORS
                    ]
                    if supported_ops:
                        res = acc_global_funcs.sjop(res, supported_ops)
                return res

            return AllowedExprToCall(_wrapped)

        return _deco(args[0]) if args and callable(args[0]) else _deco

    @property
    def account_config(self):
        """
        Returns account specific settings
        """
        # TODO: remove flask.g
        return self._accounts_manager.get_account_config(g.account)


class AllowedExprToCall(object):
    """
    Processors function wrapper. Only instances of AllowedExprToCall are allowed to call expressions.
    """

    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)


class BaseSeriesReadFunc(object):
    def __init__(self, ctx, series_fields, include_job, id_field, **call_defaults):
        self.ctx = ctx
        self.call_defaults = call_defaults
        self._series_fields = series_fields
        self._include_job = include_job
        self._id_field = id_field

        @memoize
        def instance_cache(func, *args, **kwargs):
            return func(*args, **kwargs)

        self._instance_cache = instance_cache

    def _parse_dt(cls, d, default):
        if d is not None and not isinstance(d, six.string_types):
            d = shooju.to_milli(d)
        elif d is None:
            d = default
        return d

    def __call__(self, query, max_series=None, **kwargs):
        if max_series is not None:
            kwargs["max_series"] = max_series

        for k, v in six.iteritems(self.call_defaults):
            if k not in kwargs:
                kwargs[k] = v

        points, flds, series_ids, max_job_id = self.read_series(query, **kwargs)
        self.ctx.update(query, points, flds, series_ids, max_job_id)

        if isinstance(points, pandas.Series):
            points.__class__ = SeriesWithFields
            points.fields = flds
            return points
        elif isinstance(points, pandas.DataFrame):
            points.__class__ = DataFrameWithFields
            points.fields = flds
            return points
        else:
            raise NotImplementedError()

    def read_series(self, query, **kwargs):
        raise NotImplementedError()

    @staticmethod
    def _extract_timezone(ser):
        if not ser:
            return
        return ser.get("tz", {}).get("timezone")


class SjDf(BaseSeriesReadFunc):
    """Returns a DataFrame based on a Shooju query.

    Parameters
    ----------
    max_series : int, default 100
        Maximum number of series to return as column in the dataframe
    key_field : string, default sid
        Field to use as the column header in the returned DataFrame
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    include_job: bool
        Indicates that point job_id should be included
    sort: str
        Sort order to pass to api /series?query= call
    Returns
    -------
    DataFrame
    """

    def __init__(self, ctx, series_fields, include_job, id_field, **call_defaults):
        super(SjDf, self).__init__(
            ctx, series_fields, include_job, id_field, **call_defaults
        )
        self._cached_scroll_series = partial(
            self._instance_cache, self._cached_scroll_series
        )

    def read_series(
        self,
        query,
        max_series=None,
        key_field="series_id",
        df=None,
        dt=None,
        max_points=10,
        operators=None,
        fields=None,
        sort=None,
        err_if_total_higher=True,
    ):
        _, _, agg_ops = parse_series_query(query)
        if agg_ops:
            raise ExpressionException(
                "@agg operators are not supported inside expression"
            )

        operators = (
            [o.strip() for o in operators.split("@") if o.strip()] if operators else []
        )

        if isinstance(df, (datetime, date)):
            df = self._parse_dt(df, "MIN")

        if isinstance(dt, (datetime, date)):
            dt = self._parse_dt(dt, "MAX")

        key_field = (
            key_field if key_field != "series_id" else None
        )  # no needs to explicitly pull this

        fields_to_fetch = list()
        key_fields = (
            _get_fields_in_key(key_field) if key_field else []
        )
        user_fields = self._series_fields
        df_fields = fields.split(",") if fields else []

        if key_fields:
            fields_to_fetch.extend(key_fields)

        if user_fields:
            for f in user_fields:
                fields_to_fetch.append(f)

        if df_fields:
            for f in df_fields:
                fields_to_fetch.append(f)

        id_field = self._id_field
        if id_field == 'series_id':
            id_field = 'sid'
        only_id_field = False
        if id_field not in fields_to_fetch:
            if len(fields_to_fetch) == 0:
                only_id_field = True
            fields_to_fetch.append(id_field)
        sid_not_requested = False
        if 'sid' not in fields_to_fetch:
            sid_not_requested = True
            fields_to_fetch.append('sid')

        original_query = query
        query, query_operators, query_agg_operators = parse_series_query(query)
        if query and not query.startswith('='):
            query = "({})".format(query)

        operators = operators_chain_rebuild(query_operators, operators)
        query = "@".join([query] + operators + query_agg_operators)
        if not query.startswith('='):  # enable sjdf('=
            query = "not sid:System\\Trash " + query

        params = {
            "query": query,
            "df": df,
            "dt": dt,
            "max_points": max_points,
            "scroll_batch_size": DF_SCROLL_BATCH_SIZE,
            "fields": ",".join(fields_to_fetch) if fields_to_fetch else None,
            "include_job": str(self._include_job).lower(),
            "scroll": "y",
            "debug": "y" if self.ctx.debug else "n",
        }
        if sort:
            params["sort"] = sort
        query_trace = self.ctx.trace.start_query_trace(original_query)

        series = self._scroll_series(
            params,
            max_series=max_series,
            err_if_total_higher=err_if_total_higher,
            query_trace=query_trace,
        )

        # process fields
        fields_by_sid = dict()
        for s in series:
            key = _get_key(key_field, s)

            if id_field == 'sid' or not bool(s.get('fields', {}).get(id_field)):
                underlying_id = s.get('fields', {}).get('sid')
                prefix = 'sid'
            else:
                underlying_id = s.get('fields', {}).get(id_field)
                prefix = id_field

            if s.get('xpr'):
                self.ctx.underlying_series_queries[key] = \
                    s['xpr'].get('series_queries', get_series_query_full_string(prefix, underlying_id))
            else:
                self.ctx.underlying_series_queries[key] = get_series_query_full_string(prefix, underlying_id)

            if sid_not_requested:
                s.get('fields', {}).pop('sid', None)
            if not s.get("fields") or only_id_field:
                s.get('fields', {}).pop(id_field, None)
                continue

            # collect fields that user requested
            flds = {
                k: v
                for k, v in six.iteritems(s["fields"])
                if k in user_fields or "*" in user_fields
            }
            if flds:
                fields_by_sid[key] = flds

            # remove fields if user didn't request it to put into df
            if user_fields and "fields" in s:
                s["fields"] = {
                    k: v for k, v in six.iteritems(s["fields"]) if k in user_fields or k in key_fields
                }
                if not s["fields"]:
                    del s["fields"]

            # remove user requested fields before generate pandas df
            if not key_field:
                s.pop("fields", None)
            else:
                for k, v in list(s.get('fields', {}).items()):
                    if k not in key_fields:
                        s["fields"].pop(k)
                if not s.get('fields', {}):
                    s.pop("fields", None)

        df, max_job_id = self._process_search_request(
            series, key_field=key_field, include_job=self._include_job
        )
        for col in df:  # empty dict if no fields
            if not fields_by_sid.get(col):
                fields_by_sid[col] = {}
        df.name = query
        df.sort_index(inplace=True)

        return df, fields_by_sid, [s["series_id"] for s in series], max_job_id

    def _scroll_series(
        self, params, max_series=100, err_if_total_higher=True, query_trace=None
    ):
        return self._cached_scroll_series(
            tuple(sorted(params.items())), max_series, err_if_total_higher, query_trace
        )

    def _cached_scroll_series(
        self, params, max_series, err_if_total_higher, query_trace
    ):
        params = dict(params)
        with query_trace:
            resp = self.ctx.sjclient.raw.get(
                "/series", params=params, ignore_exceptions=self.ctx.debug
            )
        query_trace.handle_api_response(resp)

        if err_if_total_higher and resp["total"] > max_series:
            raise ExpressionException(
                "max_series is set to {} but got {} results; "
                "set max_series higher or err_if_total_higher to False to ignore".format(
                    max_series, resp["total"]
                )
            )

        series_left = min(resp["total"], max_series)
        series = list()

        while series_left > 0:
            scroll_id = resp.get("scroll_id")
            if not resp["series"]:
                break
            self.ctx.points_scrolled += sum(
                len(s.get("points", [])) for s in resp["series"]
            )

            if self.ctx.points_scrolled > MAX_POINTS_IN_DF:
                raise ExpressionException(
                    "amount of points used in expression exceeded limit of {}".format(
                        MAX_POINTS_IN_DF
                    )
                )

            series.extend(resp["series"])
            series_left -= len(resp["series"])
            if series_left > 0:
                with query_trace:
                    resp = self.ctx.sjclient.raw.get(
                        "/series",
                        params={
                            "scroll_id": scroll_id,
                        },
                        ignore_exceptions=self.ctx.debug,
                    )
                query_trace.handle_api_response(resp)

        return series[:max_series]

    def _process_search_request(self, series, key_field=None, include_job=False):
        if not series:
            return (
                pandas.DataFrame(data=[], index=pandas.DatetimeIndex([], tz=UTC)),
                None,
            )

        has_fields = any(r.get("fields") for r in series) and not key_field
        has_points = any(r.get("points") for r in series)
        data = None
        max_job_id = 0
        if has_points and has_fields:
            data = list()
            fields_by_sid = dict()
            series_by_dates = defaultdict(dict)

            try:
                for s in series:
                    tz = self._extract_timezone(s)
                    if tz is not None:
                        tz = pytz.timezone(tz)
                    fields_by_sid[s["series_id"]] = s.get("fields", dict())
                    pts = s.get("points", [])
                    for p in milli_tuple_naive_to_pandas_tz_aware(pts, tz):
                        series_by_dates[p[0]][s["series_id"]] = p[1]

                        if include_job and len(p) > 2:
                            max_job_id = max(p[2], max_job_id)
            except Exception as e:
                raise type(e)('Error processing series {}: {}'.format(s["series_id"], e))

            for dt_milli, series_values in series_by_dates.items():
                dt = pandas.Timestamp(dt_milli * NANOSECONDS)
                for series_id in fields_by_sid:
                    value = series_by_dates[dt_milli].get(series_id)
                    row = dict(fields_by_sid[series_id])
                    row.update({"date": dt, "points": value, "series_id": series_id})
                    data.append(row)
        elif has_points:
            data = dict()
            if key_field:
                fields_in_key = _get_fields_in_key(key_field)
            try:
                for s in series:
                    tz = self._extract_timezone(s)
                    if tz is not None:
                        tz = pytz.timezone(tz)
                    if key_field:
                        key = _get_column_name(
                            key_field, fields_in_key, s.get("fields", {})
                        )
                    else:
                        key = s["series_id"]
                    if key == 'null':
                        key = s["series_id"]
                    pts = s.get("points", [])
                    for p in s.get("points", []):
                        if include_job and len(p) > 2:
                            max_job_id = max(p[2], max_job_id)
                    data[key] = milli_tuple_naive_to_pandas_tz_aware(pts, tz)
            except Exception as e:
                raise type(e)('Error processing series {}: {}'.format(s["series_id"], e))
        elif has_fields:
            data = list()
            for s in series:
                data.append(dict(series_id=s["series_id"], **s.get("fields", dict())))

        if data:
            return pandas.DataFrame(data=data), max_job_id
        else:
            return (
                pandas.DataFrame(
                    data={
                        _get_key(key_field, s): pandas.Series(
                            data=[],
                            index=pandas.DatetimeIndex(
                                [], tz=self._extract_timezone(s) or UTC
                            ),
                        )
                        for s in series
                    }
                ),
                max_job_id,
            )


class SjOrDf(SjDf):
    """Returns a DataFrame or Series (if is a single series in result) based on a Shooju query.

    Parameters
    ----------
    max_series : int, default 10
        Maximum number of series to return as column in the dataframe
    key_field : string, default sid
        Field to use as the column header in the returned DataFrame
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    include_job: bool
        Indicates that point job_id should be included
    sort: str
        Sort order to pass to api /series?query= call
    Returns
    -------
    DataFrame
    """

    def __init__(self, ctx, series_fields, include_job, id_field, **call_defaults):
        super(SjOrDf, self).__init__(
            ctx, series_fields, include_job, id_field, **call_defaults
        )
        self.call_defaults["err_if_total_higher"] = True

    def read_series(
        self,
        query,
        max_series=None,
        key_field="series_id",
        df=None,
        dt=None,
        max_points=10,
        operators=None,
        fields=None,
        sort=None,
        err_if_total_higher=True,
    ):
        res, fields_by_sid, series_ids, max_job_id = super(SjOrDf, self).read_series(
            query,
            max_series,
            key_field,
            df,
            dt,
            max_points,
            operators,
            fields,
            sort,
            err_if_total_higher,
        )
        if isinstance(res, pandas.DataFrame) and len(res.columns) == 1:
            res = res[res.columns[0]]
        return res, fields_by_sid, series_ids, max_job_id


class Sjs(BaseSeriesReadFunc):
    """Returns a Series based on a Shooju series_id or $-query.

    series_query : string, required
        Series query
    df : datetime() or string, default set on expression level
        Datetime to start the DataFrame; can be MIN, MAX, or datetime()
    dt : datetime() or string, default set on expression level
        Datetime to end the DataFrame; can be MIN, MAX, or datetime()
    max_points : int, default set on expression level
        Maximum number of points to retrieve; use 0 to not retrieve points
    operators: string
        List of series operators
    include_job: bool
        Indicates that point job_id should be included
    Returns
    -------
    Series
    """

    def __init__(self, ctx, series_fields, include_job, id_field, **call_defaults):
        super(Sjs, self).__init__(
            ctx, series_fields, include_job, id_field, **call_defaults
        )
        self._cached_read_series = partial(
            self._instance_cache, self._cached_read_series
        )
        self._ignore_series_not_found = False

    def set_ignore_series_not_found(self, ignore_series_not_found):
        self._ignore_series_not_found = ignore_series_not_found

    def read_series(
        self, series_query, df=None, dt=None, max_points=10, operators=None
    ):
        return self._cached_read_series(series_query, df, dt, max_points, operators)

    def _cached_read_series(self, series_query, df, dt, max_points, operators):
        operators = operators.split("@") if operators else None
        if operators:
            series_query = u"{}@{}".format(series_query, "@".join(operators))

        fields_to_fetch = list()

        fields = self._series_fields
        if fields:
            fields_to_fetch.extend(fields)

        id_field = self._id_field
        if id_field == 'series_id':
            id_field = 'sid'
        only_id_field = False
        if id_field not in fields_to_fetch:
            if len(fields_to_fetch) == 0:
                only_id_field = True
            fields_to_fetch.append(id_field)
        sid_not_requested = False
        if 'sid' not in fields_to_fetch:
            sid_not_requested = True
            fields_to_fetch.append('sid')

        query_trace = self.ctx.trace.start_query_trace(series_query)
        with query_trace:
            resp = self.ctx.sjclient.raw.post(
                "/series",
                data_json={"series_queries": [series_query]},
                params={
                    "df": self._parse_dt(df, "MIN"),
                    "dt": self._parse_dt(dt, "MAX"),
                    "max_points": max_points,
                    "fields": ",".join(fields_to_fetch) if fields_to_fetch else None,
                    "include_job": "y" if self._include_job else None,
                    "debug": "y" if self.ctx.debug else None,
                },
                ignore_exceptions=self.ctx.debug,
            )
        if "error" in resp:
            query_trace.handle_api_response(resp)
        else:
            # check on series level
            try:
                query_trace.handle_series_level_response(
                    resp["series"][0], raise_on_not_found=True
                )
            except shooju.ShoojuApiError as e:
                if self._ignore_series_not_found and e.message == 'series not found':
                    not_found_series = milli_tuple_naive_to_pandas_tz_aware([])
                    not_found_series.name = 'series_not_found'
                    return not_found_series, {'series_not_found': {}}, ['series_not_found'], None
                raise

        ser = resp["series"][0]

        tz = self._extract_timezone(ser)
        if tz is not None:
            tz = pytz.timezone(tz)

        flds = ser.get("fields") or dict()

        if id_field == 'sid' or not bool(ser.get('fields', {}).get(id_field)):
            underlying_id = ser.get('fields', {}).get('sid')
            prefix = 'sid'
        else:
            underlying_id = ser.get('fields', {}).get(id_field)
            prefix = id_field
        if ser.get('xpr'):
            self.ctx.underlying_series_queries[series_query] =\
                ser['xpr'].get('series_queries', get_series_query_full_string(prefix, underlying_id))
        else:
            self.ctx.underlying_series_queries[series_query] = get_series_query_full_string(prefix, underlying_id)
        if sid_not_requested:
            ser.get('fields', {}).pop('sid', None)
        if only_id_field:
            flds.pop(id_field, None)

        pts = ser.get("points") or []
        p_ser = milli_tuple_naive_to_pandas_tz_aware(pts, tz)

        if self._include_job and len(pts) and len(pts[0]) > 2:
            max_job_id = max(p[2] for p in pts)
        else:
            max_job_id = None
        p_ser.name = series_query
        p_ser.sort_index(inplace=True)

        return p_ser, {series_query: flds}, [ser["series_id"]], max_job_id


# All python arithmetic functions, taken from https://docs.python.org/3.7/reference/datamodel.html
arithmetic_methods = ['__add__', '__sub__', '__mul__', '__matmul__', '__truediv__', '__floordiv__', '__mod__',
                      '__divmod__', '__pow__', '__lshift__', '__rshift__', '__and__', '__xor__', '__or__',
                      '__radd__', '__rsub__', '__rmul__', '__rmatmul__', '__rtruediv__', '__rfloordiv__',
                      '__rmod__', '__rdivmod__', '__rpow__', '__rlshift__', '__rrshift__', '__rand__',
                      '__rxor__', '__ror__', '__iadd__', '__isub__', '__imul__', '__imatmul__', '__itruediv__',
                      '__ifloordiv__', '__imod__', '__ipow__', '__ilshift__', '__irshift__', '__iand__',
                      '__ixor__', '__ior__', '__neg__', '__pos__', '__abs__', '__invert__', '__complex__',
                      '__int__', '__float__', '__round__', '__trunc__', '__floor__', '__ceil__']

# Special pandas functions
arithmetic_methods += ['__div__', '__finalize__', '__rdiv__']


def object_with_fields_factory(target_class, *mixins):
    class ObjectWithFields(*mixins, target_class):  # mixins first so we get _internal_names_set
        def __init__(self, *args, **kwargs):
            if not isinstance(self, (int, float)):
                super().__init__(*args, **kwargs)
            else:
                super().__init__()
            self.fields = None  # Make sure that inherited class does not have the same properties
            self.fields_by_sid = None

        @property
        def fields(self):
            return self._fields

        @fields.setter
        def fields(self, value):
            self._fields = value
            self.fields_by_sid = value

        def get_underlying_series(self):
            if self.fields_by_sid is None:
                return None

            if isinstance(self, DataFrameWithFields):
                columns_set = set(self.columns.values)
                fields_set = set(self.fields_by_sid.keys())
                if fields_set.issubset(columns_set) and not bool(fields_set.difference(columns_set)):
                    return {column: [column] for column in self.columns}
                if columns_set.issubset(fields_set) and bool(fields_set.difference(columns_set)):
                    return {column: [column] + list(fields_set.difference(columns_set)) for column in self.columns}
            elif isinstance(self, SeriesWithFields):
                if len(self.fields_by_sid) == 1 and self.name in self.fields_by_sid:
                    return {self.name: [self.name]}
            return {'name_not_available': list(self.fields_by_sid.keys())}

        def process_fields(self):
            if isinstance(self, DataFrameWithFields):
                columns_set = set(self.columns.values)
                fields_set = set(self.fields_by_sid.keys())
                if fields_set.issubset(columns_set) and not bool(fields_set.difference(columns_set)):
                    self._fields = self.fields_by_sid
                    return
                if columns_set.issubset(fields_set) and bool(fields_set.difference(columns_set)):
                    # broadcast operation
                    fields = {}
                    difference_dict = {x: self.fields_by_sid[x] for x in fields_set.difference(columns_set)}
                    for column in self.columns:
                        fields[column] = _resolve_fields_conflicts(
                            {**{column: self.fields_by_sid[column]}, **difference_dict})
                    self._fields = fields
                    return
            elif isinstance(self, SeriesWithFields):
                if len(self.fields_by_sid) == 1 and self.name in self.fields_by_sid:
                    self._fields = self.fields_by_sid
                    return
            elif isinstance(self, (IntWithFields, FloatWithFields)):
                # optimization
                if len(self.fields_by_sid) == 1:
                    self._fields = {'name_not_available': next(iter(self.fields_by_sid.values()))}
                    return
            else:  # other objects should just pass fields ( mostly internal pandas classes )
                self._fields = self.fields_by_sid
                return

            # otherwise merge all
            self._fields = {'name_not_available': _resolve_fields_conflicts(self.fields_by_sid)}

    def wrap_method(f):
        """
        Wrap all methods, do fields logic if necessary and convert S/DF returns to S/DFWithFields
        """
        @wraps(f)
        def wrapper(*args, **kwargs):
            result = f(*args, **kwargs)
            return combine_fields(list(enumerate(args)) + list(kwargs.items()), result)
        return wrapper

    special_methods = [m for m in dir(target_class) if m.startswith('__') and m.endswith('__')]  # __add__, __sub__, etc

    for method_name in special_methods:
        if method_name not in arithmetic_methods:
            continue
        original_method = getattr(target_class, method_name)
        setattr(ObjectWithFields, method_name, wrap_method(original_method))

    for method_name in dir(target_class):
        original_method = getattr(target_class, method_name)
        # method types not really needed for pandas or int,float
        if not method_name.startswith('_') and isinstance(original_method, (types.FunctionType,)):
            setattr(ObjectWithFields, method_name, wrap_method(original_method))

    return ObjectWithFields


def combine_fields(args, result):
    """
    Check is any argument passed to any method a ObjectWithFields
    """

    self = args[0][1] if isinstance(args[0][1], CLASSES_WITH_FIELDS) else None

    fields_by_sid = None
    found_other_fields = False

    for key, value in args:
        # key can be index or name
        if isinstance(value, CLASSES_WITH_FIELDS):
            if value.fields_by_sid is None:
                continue

            if fields_by_sid is None:
                fields_by_sid = value.fields_by_sid
            else:
                found_other_fields = True
                fields_by_sid = copy.deepcopy(fields_by_sid)  # optimization, only deep copy if other fields are found
                fields_by_sid.update(value.fields_by_sid)

    if isinstance(result, pandas.Series):
        result.__class__ = SeriesWithFields
    elif isinstance(result, pandas.DataFrame):
        result.__class__ = DataFrameWithFields
    elif isinstance(result, (float, numpy.float_)):
        result = FloatWithFields(result)
    elif isinstance(result, (int, numpy.integer)) and not isinstance(result, bool):
        result = IntWithFields(result)
    elif isinstance(result, pandas.core.window.Rolling):
        result.__class__ = RollingWithFields
    elif isinstance(result, pandas.core.groupby.generic.SeriesGroupBy):
        result.__class__ = SeriesGroupByWithFields
    elif isinstance(result, pandas.core.groupby.generic.DataFrameGroupBy):
        result.__class__ = DataFrameGroupByWithFields
    elif isinstance(result, pandas.core.window.Expanding):
        result.__class__ = ExpandingWithFields
    elif isinstance(result, pandas.core.window.EWM):
        result.__class__ = EWMWithFields

    if isinstance(result, CLASSES_WITH_FIELDS):
        result.fields_by_sid = fields_by_sid
        if fields_by_sid is not None:
            result.process_fields()

    if (self is not None and
            result is None and
            found_other_fields):  # inplace
        self.fields_by_sid = fields_by_sid
        self.process_fields()

    return result


class _DataFrameMixin:
    _internal_names = pd.DataFrame._internal_names + ["fields", "fields_by_sid", "_fields"]
    _internal_names_set = set(_internal_names)

    @property
    def _constructor(self):
        return DataFrameWithFields

    @property
    def _constructor_sliced(self):
        return SeriesWithFields


class _SeriesMixin:
    _internal_names = pd.Series._internal_names + ["fields", "fields_by_sid", "_fields"]
    _internal_names_set = set(_internal_names)

    @property
    def _constructor(self):
        return SeriesWithFields

    @property
    def _constructor_expanddim(self):
        return DataFrameWithFields


DataFrameWithFields = object_with_fields_factory(pd.DataFrame, _DataFrameMixin)
SeriesWithFields = object_with_fields_factory(pd.Series, _SeriesMixin)
IntWithFields = object_with_fields_factory(int)
FloatWithFields = object_with_fields_factory(float)
# pandas special classes
RollingWithFields = object_with_fields_factory(pandas.core.window.Rolling)
SeriesGroupByWithFields = object_with_fields_factory(pandas.core.groupby.generic.SeriesGroupBy)
DataFrameGroupByWithFields = object_with_fields_factory(pandas.core.groupby.generic.DataFrameGroupBy)
ExpandingWithFields = object_with_fields_factory(pandas.core.window.Expanding)
EWMWithFields = object_with_fields_factory(pandas.core.window.EWM)

CLASSES_WITH_FIELDS = (DataFrameWithFields, SeriesWithFields, IntWithFields, FloatWithFields, RollingWithFields,
                       SeriesGroupByWithFields, DataFrameGroupByWithFields, ExpandingWithFields, EWMWithFields)


def get_series_query_full_string(prefix, query):
    if query and ' ' in query:
        query = f'"{query}"'
    return f'{prefix}={query}'


def _resolve_fields_conflicts(fields_by_sids):
    # optimization
    if len(fields_by_sids) == 1:
        return next(iter(fields_by_sids.values()))
    elif len(fields_by_sids) == 0:
        return {}

    values_by_fields, is_array_field = _group_by_field_name(fields_by_sids)

    fields = dict()
    for k, v in six.iteritems(values_by_fields):
        if is_array_field[k]:  # for these fields we have a bit specific logic
            v = list(map(json.loads, v))
            v = reduce(
                lambda a, b: set(a).intersection(set(b)),
                [
                    list(map(json.dumps, i))
                    if isinstance(i, list)
                    else [json.dumps(i)]
                    for i in v
                ],
            )

            if v:
                fields[k] = list(map(json.loads, v))
        elif v and len(v) == 1:
            fields[k] = json.loads(v.pop())
    return fields


def _group_by_field_name(fields_by_sids):
    values_by_fields = defaultdict(set)
    is_array_field = defaultdict(bool)

    for flds in six.itervalues(fields_by_sids):
        for k, v in six.iteritems(flds):
            if isinstance(v, list):
                is_array_field[k] = True
            values_by_fields[k].add(json.dumps(v))
    return values_by_fields, is_array_field


class ImportedShoojuProcessor(types.ModuleType):
    def __getattribute__(self, name):
        attr = super(ImportedShoojuProcessor, self).__getattribute__(name)
        if name in ('__class__', ):  # isinstance( needs this, type( doesn't
            return attr
        if callable(attr) and not isinstance(attr, AllowedExprToCall):
            raise ExpressionException("{} is not expressionable".format(name))
        return attr


def _get_fields_in_key(key_field):
    if '{' in key_field:
        return KEY_RE.findall(key_field)
    else:
        return [key_field]


def _get_key(key_field, s, fields_in_key=None):
    if key_field:
        if fields_in_key is None:
            fields_in_key = _get_fields_in_key(key_field)
        key = _get_column_name(
            key_field, fields_in_key, s.get("fields", {})
        )
    else:
        key = s["series_id"]
    if key == 'null':
        key = s["series_id"]
    return key


def _get_column_name(key_field, fields_in_key, fields):
    if not fields_in_key:  # no fields were requested, probably user error like {test
        return 'null'

    # one field case, no format, just return the value fo the field
    if '{' not in key_field:
        value = fields.get(key_field)
        value = value if value is not None else 'null'
        return value

    field_dict = {}
    for field, value in fields.items():
        if '.' in field:
            field_sanitized = field.replace('.', '')
            key_field = key_field.replace(field, field_sanitized)
            field = field_sanitized
        field_dict[field] = value

    # fill fields that didn't might not be in the list of fields
    for field in fields_in_key:
        if field not in field_dict:
            field_dict[field] = 'null'

    return key_field.format(**field_dict)