import datetime
import functools
import six
import time
import tempfile
import hashlib
from diskcache import Cache
from sjtoolkit.exceptions import InvalidParametersException

try:
    import cPickle
except ImportError:
    import _pickle as cPickle


def memoize(*args, **kwargs):
    """
    Decorator which makes function cacheable. Since dict is not a hashable object we avoid
    of using kwargs with memoize for better performance

    Accepts optional parameter 'keep' which is function that implements logic to decide which value should be kept in memory.
    If 'keep' function called against accepted params and returned value of decorated function return True, value will be memozied.
    Otherwise not, so on next time will be calculated again.

    If 'keep' is None all values will be memoized.

    """
    keep = kwargs.get('keep')

    def decorator(f):
        class memodict(dict):
            def __getitem__(self, *key):
                return dict.__getitem__(self, key)

            def __missing__(self, key):
                ret = f(*key)
                if keep is None or keep(key, ret):
                    self[key] = ret
                return ret

        return memodict().__getitem__

    return decorator(args[0]) if args and callable(args[0]) else decorator


def retry(*args, **kwargs):
    """Retry calling the decorated function using an exponential backoff.
     This decorator can be called with no keyword arguments, so default values will be used:

    :param times: number of times to try (not retry) before giving up. default 5
    :type times: int
    :param delay: initial delay between retries in seconds.
    this can be either numeric value or function which return numeric value. default 3
    :type delay: int, function
    :param backoff: backoff multiplier e.g. value of 2 will double the delay, default 2
        each retry
    :type backoff: int
    :param exception_to_check: the exception to check. may be a tuple of. default Exception
        exceptions to check
    :type exception_to_check: Exception or tuple
    :type logger: Logger instance to log retrying attempts.
    """
    times, delay, backoff, exception_to_check = kwargs.get('times', 5), kwargs.get('delay', 3), \
                                                kwargs.get('backoff', 2), kwargs.get('exception_to_check', Exception)
    logger = kwargs.get('logger')

    def deco_retry(f):
        @functools.wraps(f)
        def f_retry(*args, **kwargs):
            mtries, mdelay = times, delay
            if callable(mdelay):
                mdelay = mdelay()
            while mtries > 1:
                try:
                    return f(*args, **kwargs)
                except exception_to_check as e:
                    if logger:
                        logger.debug(u"{}: {}, Retrying in {} seconds...".format(
                            type(e).__name__, six.text_type(e), mdelay)
                        )
                    time.sleep(mdelay)
                    mtries -= 1
                    mdelay *= backoff
            return f(*args, **kwargs)
        return f_retry  # true decorator

    # if bool(args) is True  means we passed func as argument, so we called retry with no params (@sjutils.retry)
    return deco_retry(args[0]) if args and callable(args[0]) else deco_retry


def hang(*args, **kwargs):
    """Decorator. Setups periodic task for limited time. Return value is ignored.
    If function execution takes more than `run_every` corresponding schedule will be skipped.

    :param hang_for: hang time in seconds
    :param run_every: task run period in seconds
    """
    hang_for = kwargs.get('hang_for')
    run_every = kwargs.get('run_every')

    if not isinstance(hang_for, (int, float)) or not isinstance(run_every, (int, float)):
        raise InvalidParametersException("hang_for and run_every are required and should be numeric values")

    if hang_for < 0 or run_every < 0:
        raise InvalidParametersException("hang_for and run_every should be greater or equal to 0")

    def decorator(f):
        @functools.wraps(f)
        def wrapped(*args, **kwargs):
            now = datetime.datetime.now()
            finish_at = now + datetime.timedelta(seconds=hang_for)
            next_run_at = now

            while True:
                f(*args, **kwargs)

                now = datetime.datetime.now()

                next_run_at += datetime.timedelta(seconds=run_every)

                if now > finish_at or next_run_at > finish_at:
                    break

                if next_run_at > now:
                    time.sleep((next_run_at - now).total_seconds())

        return wrapped

    return decorator(args[0]) if args and callable(args[0]) else decorator


class OnDiskCacheDecorator(object):
    """
    Decorator that caches function result on disk for <ttl> seconds.

    """
    missing = object()

    def __init__(self, directory=tempfile.gettempdir(), size_mb=1024):
        """
        Cache decorator initiation parameter

        """
        self.cache = Cache(directory, size_limit=size_mb * 1024 * 1024)

    def __call__(self, ttl):
        def _deco(func):
            def _wrapper(*args, **kwargs):
                key = self.build_key(func, *args, **kwargs)
                hit = self.cache.get(key, default=self.missing)
                if hit is not self.missing:
                    return hit
                else:
                    res = func(*args, **kwargs)
                    self.cache.set(key, res, expire=ttl)
                    return res

            return _wrapper

        return _deco

    @staticmethod
    def build_key(func, *args, **kwargs):
        full_name = "{}.{}".format(func.__module__, func.__name__).encode('ascii')
        args_str = cPickle.dumps(args)
        kwargs_str = cPickle.dumps(kwargs)
        return hashlib.md5(full_name + args_str + kwargs_str).hexdigest()

    def clear(self):
        self.cache.clear()

    def reset_settings(self, directory, size_mb):
        self.cache = Cache(directory, size_limit=size_mb * 1024 * 1024)


# prepare cache decorator
cached = OnDiskCacheDecorator()


def invocable(*args, **kwargs):
    """
    Functions that are not wrapped by this decorator cannot be invoked via SJVirt /source/<source>/invoke/<func_name> api.

    :param teams: Users of what teams allowed to invoke the function.
    :type teams: list
    """
    def _deco(f):
        return Invocable(f, teams=kwargs.get('teams'))

    if args and callable(args[0]):
        return _deco(args[0])
    else:
        return _deco


class Invocable(object):
    def __init__(self, func, teams=None):
        self.func = func
        self.teams = teams

        # just to make it look like the underlying function
        self.__name__ = func.__name__
        self.__module__ = func.__module__

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)

    def check_user_teams(self, user_teams):
        if not self.teams:
            return True
        if set(self.teams).intersection(set(user_teams)):
            return True
        return False
