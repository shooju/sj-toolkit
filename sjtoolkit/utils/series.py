import re
from collections import namedtuple, OrderedDict

import jinja2
import jinja2.sandbox
import jmespath
import numpy as np
import six
from sjtoolkit.exceptions import InvalidParametersException

_columns = [('dates', 'i8'), ('values', 'f8'), ('jobs', 'i4'), ('ts', 'u8')]
PTS_DTYPE = np.dtype(_columns)
COLUMN_TYPES = dict(_columns)

PTS_DATES_VALUES = np.dtype(_columns[:2])

FIELD_TYPES = namedtuple('FIELD_TYPES', 'geo num date tree obj text')(
    geo='geo', num='num', date='date', tree='tree', obj='obj', text='text')

DATE_META_FIELDS = ['meta.{}'.format(f) for f in ['updated_at', 'dates_max', 'dates_min']]

NUMERIC_META_FIELDS = ['meta.{}'.format(f) for f in ['points_count', 'points_min',
                                                     'points_max', 'levels', 'job', 'job_changed',
                                                     'job_addedto', 'job_delfrom', 'job_created']]


def field_type(field_name):
    """
    Determines field type by it's name
    """
    for t in six.itervalues(FIELD_TYPES._asdict()):
        if field_name.endswith('_' + t):
            return t
    if field_name in DATE_META_FIELDS:
        return FIELD_TYPES.date
    elif field_name in NUMERIC_META_FIELDS:
        return FIELD_TYPES.num
    return FIELD_TYPES.text


def _fields_jinja_post_processor(field, value):
    env = jinja2.sandbox.SandboxedEnvironment()
    template = env.from_string(field)
    try:
        return template.render(**value)
    except (jinja2.TemplateError, jinja2.sandbox.SecurityError):
        return None


def fields_post_processor(fields):
    """
    Performs series fields post-processing. Changes fields in place.
    Currently only does @ operators:
        * path -- uses jmespath -- user must substitute % for @
    :param fields: fields object
    :return:
    """
    # operators
    for field, value in fields.items():
        if field.startswith('=') and value is not None:
            fields[field] = _fields_jinja_post_processor(field.lstrip('='), value)
            continue
        # this is probably the right way to do it... we should do it this way everywhere
        parts = re.split('@([a-z]+)\:', field)
        core_field = parts[0]
        ops = dict(zip(parts[1:][0::2], parts[1:][1::2]))  # is this slow?
        if 'path' in ops and (core_field.endswith('_obj') or isinstance(value, dict)):
            try:
                fields[field] = jmespath.search(ops['path'], value)
            except Exception as e:
                raise InvalidParametersException('path error: {}'.format(six.text_type(e)))


def _full_name(name, parent='', index=None):
    full = name if not parent else u'{}.{}'.format(parent, name)
    if index is not None:
        full += u'[{}]'.format(index)
    return full


def flattify(fields, parent_name=u'', max_deep=None):
    """
    Serialize series fields into flat mapping structure.
    :param fields: Fields mapping
    :param parent_name: name of parent field
    :return:
    >>> flattify({'field1': {'field': ['hey', 'bye']}})
    >>> {'field1.field[0]': 'hey', 'field1.field[1]': 'bye'}
    """
    flatten = OrderedDict()
    for field_name, val in six.iteritems(fields):
        if val is None:  # that actually can be array or simple type. let's just skip
            continue
        elif isinstance(val, (float, six.integer_types, six.string_types)):
            flatten[_full_name(field_name, parent_name)] = val
        elif isinstance(val, list):
            for i, array_val in enumerate(val):
                if not isinstance(array_val, dict):
                    flatten[_full_name(field_name, parent_name, i)] = array_val
                else:
                    flatten.update(
                        flattify(array_val,
                                 parent_name=u'{}[{}]'.format(_full_name(field_name, parent_name), i))
                    )
        elif isinstance(val, dict):
            flatten.update(flattify(val, parent_name=_full_name(field_name, parent_name)))
    return flatten
