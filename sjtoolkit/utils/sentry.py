import logging
import six

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.logging import LoggingIntegration

_sentry_logging = LoggingIntegration(
    level=logging.INFO,        # Capture info and above as breadcrumbs
    event_level=None,
)


def init_sentry(dsn, before_send=lambda event, hint: event):
    sentry_sdk.init(
        dsn=dsn,
        integrations=[FlaskIntegration(), _sentry_logging],
        before_send=before_send,
    )


def capture_message(message, extra=None):
    extra = extra or {}
    with sentry_sdk.push_scope() as scope:
        for k, v in six.iteritems(extra):
            scope.set_extra(k, v)
        sentry_sdk.capture_message(message)


def capture_exception(extra=None):
    extra = extra or {}
    with sentry_sdk.push_scope() as scope:
        for k, v in six.iteritems(extra):
            scope.set_extra(k, v)
        sentry_sdk.capture_exception()
