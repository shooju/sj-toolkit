import pyformance.meters
from pyformance.registry import MetricsRegistry


class Metrics:
    """
    Metrics collector
    """
    def __init__(self, ):
        self._registry = MetricsRegistry()
        self._common_tags = dict()

    def count(self, metric_name, **tags):
        """
        Increment counter

        :param metric_name: counter name
        :param tags: counter tags
        """
        key = self.__make_key(metric_name, **tags)
        self._registry.meter(key).mark(1)

    def timer(self, metric_name, **tags) -> pyformance.meters.timer.TimerContext:
        """
        Returns timer context for a given metrics

        :param metric_name: metric name
        :param tags: metric tags
        """

        key = self.__make_key(metric_name, **tags)
        return self._registry.timer(key).time()

    def __make_key(self, metric_name, **tags):
        return (
                tuple((('name', metric_name),)) +
                tuple((k, self._common_tags[k]) for k in sorted(self._common_tags)) +
                tuple((k, tags[k]) for k in sorted(tags))
        )

    def set_registry(self, registry: MetricsRegistry):
        """
        Sets current metrics registry
        """
        self._registry = registry

    def set_tags(self, **tags):
        """Sets tags that will be added for all metrics"""
        self._common_tags = tags

metrics = Metrics()