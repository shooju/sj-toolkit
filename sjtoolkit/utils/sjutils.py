import json
from collections import defaultdict
import io
import six
from six.moves import xrange
from six.moves._thread import get_ident
import re
import os
import random

import msgpack
import sjts

from sjtoolkit.exceptions import InvalidParametersException
from sjtoolkit.series.date_params_operators import parse_date_with_operators, BaseSjclientCalendarDateOperator
from sjtoolkit.utils.decorators import memoize, retry, hang
from sjtoolkit.utils.utils import parse_relative_delta


class BaseSJUtils(object):

    def __init__(self, namespace, *args, **kwargs):
        self._account_config = None
        self._account_id = kwargs.get('account_id')
        self.namespace = namespace
        self._ctx_by_thread = dict()
        self._operators_by_thread = defaultdict(six.text_type)
        self._sjs_by_thread = dict()
        self._sjsordf_by_thread = dict()
        self._sjdf_by_thread = dict()
        self._parse_date_operators = None
        super(BaseSJUtils, self).__init__()

    @property
    def memoize(self):
        """Decorator.  Caches function calls based on all *args and **kwargs.

        Useful for wrapping calculation-intensive or I/O intensive operations that yield the same results (e.g. requests to a remote lookup).

        **Usage Example:**

        .. code-block:: python

            @sjutils.memoize
            def somethinghard(param1, param2, ...):
                ...some work...

        """
        return memoize(self)

    def chunker(self, seq, size):
        return (seq[pos:pos + size] for pos in xrange(0, len(seq), size))

    def retry(self, *args, **kwargs):
        """Decorator.  Retries the decorated function using an exponential backoff.

        This decorator can be called without keyword arguments.

        Useful for wrapping steps in an importer that sometimes break because of source issues, like FTP too-many-users errors or unreliable APIs.

        :param times: `[default: 5]` number of times to try before giving up
        :param delay: `[default: 3]` initial delay between retries in seconds
        :param backoff: `[default: 2]` backoff multiplier e.g. value of 2 will double the delay
        :param exception_to_check: `[default: Exception]` the exception to check (Exception or tuple of Exceptions)

        **Usage Example:**

        .. code-block:: python

            @retry
            def something_dangerous():
                ...

        """
        return retry(*args, **kwargs)

    def hang(self, *args, **kwargs):
        """Decorator. Setups periodic task for limited time. Return value is ignored.
        If function execution takes more than `run_every` corresponding schedule will be skipped.

        :param hang_for: hang time in seconds
        :param run_every: task run period in seconds
        """
        return hang(*args, **kwargs)

    def xpr_query(self, sid, query):
        """
        Notifies expression engine that this series was used in expression,
        to make the engine able to correctly generate expression query.

        :param sid: Series id
        :param query: Series query.
        """
        self.expression_ctx.all_series_ids.add(sid)
        self.expression_ctx.query_by_sid[sid] = query

    def xpr_fields(self, sid, fields):
        """
        Notifies expression engine that this series was used in expression,
        to make the engine able to correctly generate expression fields.

        :param sid: Series id
        :param fields: Series fields.
        """
        self.expression_ctx.all_series_ids.add(sid)
        sid_fields = self.expression_ctx.fields_by_sids.setdefault(sid, {})
        sid_fields.update(fields)

    def clear_xpr_fields(self):
        """Clears all collected xpr fields data"""
        self.expression_ctx.all_series_ids.clear()
        self.expression_ctx.fields_by_sids.clear()
        self.expression_ctx.query_by_sid.clear()

    def invoke_sjvirt_source_func(self, func_name, vrt_obj, *args, **kwargs):
        """
        Executes custom source function.

        :param func_name: Source function name
        :param vrt_obj: `vrt` field value
        :param args: Function positional arguments
        :param kwargs: Function keywords arguments
        :return:
        """
        try:
            msgpack.dumps(args)
            msgpack.dumps(kwargs)
        except (ValueError, TypeError) as e:
            raise ValueError(u'func call parameters are not msgpack serializable: %s' % e)

        sjclient = self.namespace['sjclient']

        return sjclient.raw.post('/source/%s/invoke/%s' % (vrt_obj['source'], func_name), data_json={
            'args': args,
            'kwargs': kwargs,
            'vrt': vrt_obj,
        })['result']

    def parse_relative_delta(self, relative_delta):
        """
        Parses relative (from now) delta string. Example format: -10d. Acceptable relative time symbols:

        - y - year
        - M - month
        - w - week
        - d - day
        - h - hour
        - m - minutes
        - s - seconds

        :param relative_delta:
        """
        return parse_relative_delta(relative_delta).dt

    def parse_sj_date(self, d, date_format='iso', relative_to=None):
        """
        Function parses a date with the given format and return millis, if no date is provided it returns the default
        date
        :param d: date to be parsed (can be a string, float, int or None)
        :param date_format: date format of the date to be parsed
        :param relative_to: start point for relative dates. If not passed then relative delta is relative to now().
        :return: :raise DateParseException: time in millis
        """
        if self._parse_date_operators is None:
            self._parse_date_operators = {
                'cal': self._create_parse_date_class(self.namespace['sjclient'])
            }
        return parse_date_with_operators(d, date_format=date_format,
                                         operators_mapping=self._parse_date_operators, relative_to=relative_to)

    def xpr_add_query_for_facets(self, query):
        """
        Notifies expression engine about queries used in expression, to make it able to correctly generate facets.
        :param query: Shooju query
        """
        self.expression_ctx.all_queries.add(query)

    def xpr_add_series_query(self, series_id, query, field='sid'):
        """
        Notifies expression engine about queries used in expression.
        :param series_id: Series Id, key in xpr fields
        :param query: Shooju query
        """
        if len(self.expression_ctx.underlying_series.get(series_id, [])) > 15:  # limit
            return

        def get_series_query_full_string(prefix, q):
            if q and ' ' in q:
                q = f'"{q}"'
            return f'{prefix}={q}'

        if series_id not in self.expression_ctx.underlying_series:
            self.expression_ctx.underlying_series[series_id] = list()
        self.expression_ctx.underlying_series[series_id].append(query)
        self.expression_ctx.underlying_series_queries[query] = get_series_query_full_string(field, query)

    @property
    def expression_operators(self):
        return self._operators_by_thread[self._ident]

    @expression_operators.setter
    def expression_operators(self, operators):
        self._operators_by_thread[self._ident] = operators

    @property
    def sjs(self):
        return self._sjs_by_thread[self._ident]

    @sjs.setter
    def sjs(self, func):
        self._sjs_by_thread[self._ident] = func

    @property
    def sjdf(self):
        return self._sjdf_by_thread[self._ident]

    @sjdf.setter
    def sjdf(self, func):
        self._sjdf_by_thread[self._ident] = func

    @property
    def sjsordf(self):
        return self._sjsordf_by_thread[self._ident]

    @sjsordf.setter
    def sjsordf(self, func):
        self._sjsordf_by_thread[self._ident] = func

    @property
    def expression_ctx(self):
        return self._ctx_by_thread[self._ident]

    @expression_ctx.setter
    def expression_ctx(self, ctx):
        self._ctx_by_thread[self._ident] = ctx

    def _create_parse_date_class(self, sjclient):
        class _CalendarDateOperatorForSpecificSjclient(BaseSjclientCalendarDateOperator):

            @property
            def sjclient(self):
                return sjclient

        return _CalendarDateOperatorForSpecificSjclient

    def expressionable(self, apply_operators=False):
        """
        By wrapping function in a processor, this decorator makes the function allowed to be used in expressions.
        :param apply_operators: If true global operators will be applied on expression result
        :return:
        """
        def _deco(func):
            def _wrapped(*args, **kwargs):
                return func(*args, **kwargs)

            return _wrapped

        return _deco

    @property
    def _ident(self):
        return get_ident()

    def reset(self):
        """Resets sjutils thread state after expression executed"""
        i = self._ident
        self._ctx_by_thread.pop(i, None)
        self._operators_by_thread.pop(i, None)

    def send_email(self, *args, **kwargs):
        """This is supported on Shooju end only"""
        raise NotImplementedError()

    def callable(self, *args, **kwargs):
        """
        Functions that are not wrapped by this decorator cannot be called via /<processor>/call/<func> api.

        :param as_admin: indicates that callable function must be executed using admin user
        :param public: indicates that callable function is allowed to be called by anonymous users
        :param collect_logs: collect and send function call logs to processor's logs
        """

        as_admin = kwargs.pop('as_admin', False)
        public = kwargs.pop('public', False)
        collect_logs = kwargs.pop('collect_logs', False)

        # for now all params are declarative and does nothing at runtime
        def _deco(f):
            return AllowedToCall(f, as_admin=as_admin, public=public, collect_logs=collect_logs)

        if args and callable(args[0]):
            return _deco(args[0])
        else:
            return _deco

    @property
    def account_config(self):
        """
        Returns account specific settings
        """
        if self._account_config is not None:
            return self._account_config
        sjclient = self.namespace['sjclient']
        self._account_config = sjclient.raw.get('/account/config')['account_config']
        return self._account_config

    def lock(self, *args, **kwargs):
        raise NotImplementedError()

    
    def get_useragent(self,exclude_list=None,reg_exp=None):
        """
        Returns a user agent string.
        :param exclude_list `[default: empty]` if not empty list, it will exclude the list values from the list that random.choice executes on in order to return a result
        :reg_exp `[default: empty]` If not empty, the reg_exp will be evaluated against the total list of user agents in order to determine if a user agent should be included or excluded in the random.choice list
        """
        user_agent_list = []
        exclude_list = exclude_list or []
        with open(os.path.join(os.path.dirname(__file__), '../assets/user_agents.txt'),'r') as f:
            for line in f:
                if line not in exclude_list:
                    if (reg_exp is not None and re.match(reg_exp, line)!=None) or reg_exp is None:
                        user_agent_list.append(line.replace("\n",""))
        if not user_agent_list:
            return ""
        return random.choice(user_agent_list)


class AllowedToCall(object):
    """
    Processors function wrapper. Only instances of Callable are allowed to call via /<processor>/call/<func> api.
    """

    def __init__(self, func, as_admin, public, collect_logs):
        self.func = func
        self.as_admin = as_admin
        self.public = public
        self.collect_logs = collect_logs
        self.__name__ = func.__name__
        self.__module__ = func.__module__

    def __call__(self, *args, **kwargs):
        return self.func(*args, **kwargs)


class BytesCallResponse(object):
    """BytesCallResponse respresents a binary response object from a processor's callable function."""
    def __init__(self, payload, headers=None):
        if not isinstance(payload, bytes):
            try:
                if isinstance(payload, str):
                    raise TypeError()

                payload = payload.read()
            except (AttributeError, TypeError):
                raise InvalidParametersException("bytes response must be bytes array or binary file like object")

        self._payload = io.BytesIO(payload)

        self._headers = {
            k: v for k, v in (headers or {}).items()
            if k not in {'Connection', 'Keep-Alive', 'Proxy-Authenticate', 'Proxy-Authorization',
                         'TE', 'Trailer', 'Transfer-Encoding', 'Upgrade'}
        }

    @property
    def payload(self):
        return self._payload

    @property
    def headers(self):
        return self._headers


class ReturnBytesFunc():
    def __init__(self, request_id, callable_post_headers):
        self.request_id = request_id
        self.callable_post_headers = callable_post_headers or dict()

    def __call__(self, payload, encoder=None, **kwargs):
        """
        Creates a binary response object in function code. Used in callable runner.

        :param payload: bytes array or binary file like object or dict (will be encoded using given `encoder`)
        :param encoder: if payload is dict, it will be encoded using this encoder. If not specified, will be used
        one that corresponds to incoming Sj-Receive-Format header (json if header wasn't sent). Must be one of: json, msgpack, sjts.
        :param headers: headers to be returned along with binary to the caller. Not applicable headers are filtered out.
        """
        if isinstance(payload, dict):
            enc = self._get_response_encoder(encoder)
            payload.update({'request_id': self.request_id, 'success': True})

            headers = kwargs.setdefault('headers', {})
            payload = enc.to_bytes(payload)
            enc.update_headers(headers)
        return BytesCallResponse(payload, **kwargs)

    def _get_response_encoder(self, encoder):
        if not encoder:
            encoder = self._from_receive_format_header()

        enc = response_encoders.get(encoder)
        if enc is None:
            raise Exception(
                f'unknown response encoder [{encoder}] must be one of auto, jsom, mgpack'
            )
        return enc

    def _from_receive_format_header(self):
        if 'Sj-Receive-Format' in self.callable_post_headers:
            return self.callable_post_headers['Sj-Receive-Format']

        return 'json'

class _ResponseEncoder:
    """
    Base response payload encoder
    """
    def to_bytes(self, obj: dict):
        """Encodes dict to a specific format"""
        raise NotImplementedError()

    def update_headers(self, headers: dict):
        """Updates headers with specific format headers"""
        raise NotImplementedError()


class _JsonEncoder(_ResponseEncoder):
    """Encodes response to json format"""
    def to_bytes(self, obj: dict):
        return json.dumps(obj).encode('utf-8')

    def update_headers(self, headers):
        headers.update({
            'Content-Type': 'application/json; charset=utf-8',
            'Sj-Send-Format': 'json'
        })


class _SjtsEncoder(_ResponseEncoder):
    """Encodes response to sjts format"""
    def to_bytes(self, obj: dict):
        return sjts.dumps(obj)

    def update_headers(self, headers: dict):
        headers.update({
            'Sj-Send-Format': 'sjts'
        })


class _MsgpEncoder(_ResponseEncoder):
    """Encodes response to msgpack format"""
    def to_bytes(self, obj: dict):
        return msgpack.dumps(obj)

    def update_headers(self, headers: dict):
        headers.update({
            'Sj-Send-Format': 'msgpack'
        })


response_encoders = {
    'sjts': _SjtsEncoder(),
    'msgpack': _MsgpEncoder(),
    'json': _JsonEncoder(),
}
