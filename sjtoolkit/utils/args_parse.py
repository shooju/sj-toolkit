import six
from sjtoolkit.exceptions import InvalidParametersException


def parse_int(val, param_name):
    if isinstance(val, six.string_types):
        val = val.strip()

    if val in [None, '']:
        return

    try:
        return int(val)
    except ValueError:
        raise InvalidParametersException('{} must be an int'.format(param_name))


def parse_list(val):
    return [v.lower().strip() for v in val if v.strip()]


def parse_bool(val):
    """Parses string bool value"""
    return str(val).lower() in ['y', 'yes', '1', 'true']
