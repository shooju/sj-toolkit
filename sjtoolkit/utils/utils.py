import base64
import random
import re
import string

import pytz
import six
import sys
from calendar import timegm
from contextlib import contextmanager
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile
import os
import time

from dataclasses import dataclass

from dateutil.relativedelta import relativedelta
import dateutil.parser
import msgpack
import numpy as np
from typing import List, Optional
from werkzeug.http import parse_authorization_header

from sjtoolkit.exceptions import RelativeDateRangeParseException, DateParseException, ISOParseException, NotAllowedException, InvalidParametersException
from sjtoolkit.utils.decorators import memoize

MIN_LONG = -9223372036854775808
MAX_LONG = 9223372036854775807
MAX_INT = 2147483647
MAX_UNSIGNED_INT = 4294967295

MIN_PYTHON_DATE = -62135596800 * 1000  # datetime(1, 1, 1)
MAX_PYTHON_DATE = 253402214400 * 1000  # datetime(9999, 12, 31)
CRON_FIELD_NAMES = 'second', 'minute', 'hour', 'day', 'month', 'day_of_week', 'timezone'


def series_query_to_sid(query):
    """Adds $ symbol if needed"""
    if query is None:
        return
    query = query.strip()
    if not query.startswith('='):
        return u'${}'.format(query)
    else:
        return query


PERIODS_MAP = {
    'y': relativedelta(years=1),
    'q': relativedelta(months=3),
    'M': relativedelta(months=1),
    'w': relativedelta(weeks=1),
    'd': relativedelta(days=1),
    'h': relativedelta(hours=1),
    'm': relativedelta(minutes=1),
    's': relativedelta(seconds=1),
}

SERIES_STATS_FUNCS = {
    'min': np.min,
    'max': np.max,
    'avg': np.mean,
    'std': np.std,
}


RELATIVE_DELTA_RE = re.compile(r'^(-|\+){0,1}(\d+)(\D+)$')

# YYYY-MM-DD[ HH:MM[:SS[.sss]]] space can be replaced with `T` symbol
ISO_REGEXP = re.compile('^(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})((T|\s)(?P<hour>\d{2}):(?P<minute>\d{2})(:(?P<second>\d{2})(\.((?P<microsecond>\d{4,8})|(?P<millisecond>\d{1,3})))*)*)*$')


def parse_date(d, date_format, default="MIN", operators=None, relative_to=None):
    """
    Function parses a date with the given format and return millis, if no date is provided it returns the default
    date
    :param d: date to be parsed (can be a string, float, int or None)
    :param date_format: date format of the date to be parsed
    :param default: default date it should return if no d is send
    :param operators: list of date params operators to apply
    :return: :raise DateParseException: time in millis
    """
    if date_format == 'milli' and isinstance(d, (float, int)):
        return d

    return _parse_date(d, date_format, default, operators, relative_to)


def _looks_like_numeric_or_special_value(d):
    """Returns True if date value == MIN/MAX or looks like numeric value"""
    if d in ['MIN', 'MAX']:
        return True
    if not isinstance(d, six.string_types) or not d.strip():
        return True
    elif d.isdigit() or (d.startswith('-') and d[1:].isdigit()):
        return True
    return False


def is_relative(d):
    """Determines if date looks like relative date string"""
    if _looks_like_numeric_or_special_value(d):
        return False
    else:
        return bool(all(RELATIVE_DELTA_RE.search(t) for t in d.strip().split()))


def is_iso(d):
    """Check if dates match ISO format"""
    if _looks_like_numeric_or_special_value(d):
        return False
    else:
        return bool(ISO_REGEXP.search(d))


def parse_iso(d):
    m = ISO_REGEXP.search(d)
    if not m:
        raise DateParseException('"{}" date does not match ISO format'.format(d))
    params = {k: int(v) for k, v in six.iteritems(m.groupdict()) if v is not None}
    if 'millisecond' in params:
        params['microsecond'] = params.pop('millisecond') * 1000
    try:
        dt = datetime(**params)
    except ValueError as e:
        raise DateParseException('invalid date: {}'.format(e))
    return parse_date(dt, 'datetime')


def _datetime_utc_now():  # mock can't patch built-in classes, so separate function is just for unit tests
    return datetime.utcnow()


@dataclass
class Explain:
    """Relative delta parse explain"""
    relative_to: bool # initial date - before applying delta
    relative_delta: str # user inputted relative delta
    before_operator_apply: Optional[datetime] # date after applying delta and before applying operators
    result: datetime # final result after applying delta and operators


@dataclass
class RelativeDeltaParseResult:
    """Result of relative delta parsing"""
    dt: datetime # actual result
    explain: List[Explain] # explain


@dataclass
class _delta():
    raw: string
    delta: relativedelta
    begin_flag: string

def parse_relative_delta(val, operators=None, relative_to=None)->RelativeDeltaParseResult:
    """
    Parses relative (from now) delta string. Example format: -10d. Acceptable relative time symbols:

    - y - year
    - M - month
    - w - week
    - d - day
    - h - hour
    - m - minutes
    - s - seconds

    """
    from sjtoolkit.series.suffix_processors import AGGREGATE_LEVELS   # todo need to move this at some better place
    operators = operators or []

    deltas = _parse_deltas(val)
    if not deltas:
        raise RelativeDateRangeParseException('could not parse relative delta')

    current_dt = relative_to or _datetime_utc_now()
    for op in operators:
        current_dt = op.initialize(current_dt)

    explains = list()
    for d in deltas:
        prev_dt = current_dt
        current_dt = prev_dt + d.delta
        if d.begin_flag:
            current_dt = adjust_date(
                current_dt,
                'datetime',
                AGGREGATE_LEVELS[d.begin_flag],
                up=False,
                inclusive=False,
            )
        before_operator_apply = current_dt
        for op in operators:
            current_dt = op.apply(
                delta=d.delta,
                begin=d.begin_flag,
                prev_dt=prev_dt,
                current_dt=current_dt,
            )
        expl = Explain(
            relative_to=prev_dt,
            relative_delta=d.raw,
            result=current_dt,
            before_operator_apply=None,
        )
        if operators:
            expl.before_operator_apply  = before_operator_apply

        explains.append(expl)

    for op in operators:
        current_dt = op.finalize(current_dt)
    return RelativeDeltaParseResult(
        dt=parse_date(current_dt, 'datetime'),
        explain=explains
    )


def _parse_deltas(val)->List[_delta]:
    deltas = list()
    for v in val.strip().split():
        m = RELATIVE_DELTA_RE.search(v)
        if not m:
            raise RelativeDateRangeParseException('{} is invalid relative date range'.format(val))
        sign, length, period = m.groups()
        sign = 1 if sign in (None, '+') else -1
        try:
            length = int(length)
        except (ValueError, TypeError):
            raise RelativeDateRangeParseException('expected relative time integer {}'.format(length))
        period_begin = None
        if period.endswith('b'):
            period = period[:-1]
            period_begin = period

        try:
            period = PERIODS_MAP[period]
        except KeyError:
            raise RelativeDateRangeParseException('unknown relative time symbol {}'.format(period))
        deltas.append(_delta(raw=v, delta=period * length * sign, begin_flag=period_begin))
    return deltas


def _parse_relative_delta(val):
    delta = relativedelta(days=0)
    for d in _parse_deltas(val):
        delta += d.delta
    return delta



def _parse_date(d, date_format, default, operators, relative_to):
    is_rel = is_relative(d)
    if not is_rel and operators:
        raise DateParseException('date operators supported for relative period format only')

    try:
        if d is None:
            d = default
        if d == "MIN":
            return MIN_LONG
        if d == "MAX":
            return MAX_LONG
        if is_iso(d) and date_format != 'iso':   # for date_format=iso we leave old less stricted dateutil parse logic (just avoid breaking some code)
            return parse_iso(d)
        if is_rel:
            return parse_relative_delta(d, operators=operators, relative_to=relative_to).dt
        if date_format == "unix":
            return int(d) * 1000  # int(d.split(".")[0]) if isinstance(d,basestring) else int(d)
        elif date_format == "milli":
            return int(d)
        elif date_format == "iso":
            return timegm(dateutil.parser.parse(d).utctimetuple()) * 1000
        elif date_format == "datetime":
            return timegm(d.utctimetuple()) * 1000 + d.microsecond // 1000
        if d == "MIN":
            return MIN_LONG
        if d == "MAX":
            return MAX_LONG
    except RelativeDateRangeParseException:
        raise RelativeDateRangeParseException('"{}" is invalid relative date range'.format(d))
    except ISOParseException:
        raise ISOParseException('"{}" does not match ISO format'.format(d))
    except Exception:
        raise DateParseException("format %s cannot parse date %s (%s)" % (date_format, d, sys.exc_info()[1]))


_parse_date_memoized = memoize(keep=lambda _, result: result % 3600000 == 0)(_parse_date)  # memoize only hours


def render_date(d, date_format):
    """Takes as input d in milli and outputs in date_format."""
    # no need to memoize unix - cheaper just divide by 1000
    if date_format == "unix":
        return d / 1000.0
    else:
        return _render_date(d, date_format)


@memoize(keep=lambda args, _: args[0] % 3600000 == 0)  # memoize only hours
def _render_date(d, date_format):
    if date_format == "milli":
        return d
    elif date_format == "iso":
        return (datetime(1970, 1, 1) + timedelta(seconds=d / 1000.0)).isoformat()
    elif date_format == "datetime":
        return datetime(1970, 1, 1) + timedelta(seconds=d / 1000.0)


first_cap_re = re.compile('(.)([A-Z][a-z]+)')
all_cap_re = re.compile('([a-z0-9])([A-Z])')


def camel_to_underscore(name):
    """
    Converts ThisStyledString to this_styled_string
    :param name:
    :return:
    """

    s1 = first_cap_re.sub(r'\1_\2', name)
    return all_cap_re.sub(r'\1_\2', s1).lower()


@contextmanager
def content_into_tmp(data):
    """
    Context manager that creates temp file with data passed as parameter.

    :param data: Content of temp file
    :return: absolute path to temp file.
    """
    if data is None:  # do nothing
        yield
        return
    assert isinstance(data, (six.string_types, bytes))
    tmp = NamedTemporaryFile(delete=False)
    tmp.write(data)
    tmp.close()
    yield tmp.name
    try:
        os.remove(tmp.name)
    except (OSError, IOError):
        pass


def millis_utc_now():
    """
    Gets the unix time

    :param rounded: flag indicating if the time should be returned as an int
    :return: unix utc timestamp in milliseconds
    """
    r = time.time() * 1000  # unix time in millis
    return int(r)


def parse_cron(cron_string):
    cron_parts = cron_string.split()
    if len(cron_parts) != len(CRON_FIELD_NAMES):
        raise ValueError("invalid cron schedule, should be in form '%s'" % ' '.join(CRON_FIELD_NAMES))
    all_params = zip(CRON_FIELD_NAMES, cron_parts)
    return {p[0]: (p[1].replace('_', ' ') if p[0] != 'timezone' else p[1])
            for p in all_params if p[1] != '*'}


class MsgPack(object):
    """msgpack package wrapper"""

    @staticmethod
    def dumps(*args, **kwargs):
        if msgpack.version[0] > 0:
            return msgpack.dumps(*args, **kwargs)
        else:
            return msgpack.dumps(*args, encoding='utf-8', **kwargs)

    @staticmethod
    def loads(*args, **kwargs):
        kwargs.pop('encoding', None)
        if msgpack.version[0] > 0:
            return msgpack.loads(*args, **kwargs)
        else:
            return msgpack.loads(*args, encoding='utf-8', **kwargs)


def adjust_date(d_in, date_format, agg_lvl, up=False, inclusive=False, length=1,):
    """

    Adjusts date to the aggregation level, for example, if the aggregation level is monthly, and date is 2013-3-15
    it will return 2013-3-1, and if aggregation level is yearly it will return 2013-1-1

    :param d_in: date to be adjusted
    :param date_format: format of the date to be adjusted
    :param agg_lvl: numeric value of the aggregation level
    :param up: flag indicating if it should go the last day of the aggregation level instead of the first one
    :param inclusive:
    :param length: number of periods to adjust
    :return: :raise ex:
    """
    # TODO all these conversions are a waste...
    from sjtoolkit.series.suffix_processors import AGGREGATE_LEVELS
    d = parse_date(d_in, date_format) / 1000.0
    try:
        d = datetime.utcfromtimestamp(d)
    except ValueError as ex:
        str_ex = six.text_type(ex)
        if 'year' in str_ex and 'is out of range' in str_ex:
            return d_in
        raise ex

    if agg_lvl == AGGREGATE_LEVELS['s']:
        n = d.replace(microsecond=0)
        if up and (n != d or inclusive):
            n = _safe_add_adjust_delta(n, timedelta(seconds=length))
    elif agg_lvl == AGGREGATE_LEVELS['m']:
        n = d.replace(second=0, microsecond=0)
        if up and (n != d or inclusive):
            n = _safe_add_adjust_delta(n, timedelta(minutes=length))
    elif agg_lvl == AGGREGATE_LEVELS['H']:
        n = d.replace(minute=0, second=0, microsecond=0)
        if up and (n != d or inclusive):
            n = _safe_add_adjust_delta(n, timedelta(hours=length))
    elif agg_lvl == AGGREGATE_LEVELS['D']:
        n = d.replace(hour=0, minute=0, second=0, microsecond=0)
        if up and (n != d or inclusive):
            n = _safe_add_adjust_delta(n, timedelta(days=length))
    elif agg_lvl == AGGREGATE_LEVELS['W']:
        n = d.replace(hour=0, minute=0, second=0, microsecond=0) - timedelta(days=d.weekday())
        if up and (n != d or inclusive):
            n = _safe_add_adjust_delta(n, timedelta(days=7 * length))
    elif agg_lvl == AGGREGATE_LEVELS['M']:
        n = d.replace(day=1, hour=0, minute=0, second=0, microsecond=0)
        if up and (n != d or inclusive):
            try:
                n += relativedelta(months=length)
            except ValueError:
                pass
    elif agg_lvl == AGGREGATE_LEVELS['Q']:
        n = d.replace(month=qtom(mtoq(d.month)), day=1, hour=0, minute=0, second=0, microsecond=0)
        if up and (n != d or inclusive):
            try:
                n += relativedelta(months=3 * length)
            except ValueError:
                pass
    elif agg_lvl == AGGREGATE_LEVELS['Y']:
        n = d.replace(month=1, day=1, hour=0, minute=0, second=0, microsecond=0)
        if up and (n != d or inclusive) and n.year + length < 9999:
            try:
                n = n.replace(year=n.year + 1 * length)
            except ValueError:
                pass
    else:
        raise Exception('unreachable')

    milli = parse_date(n, "datetime")
    if inclusive:
        milli -= 1
    return render_date(milli, date_format)


def mtoq(m):
    return (m - 1) // 3 + 1


def qtom(q):
    return q * 3 - 2


def _safe_add_adjust_delta(dt, tdelta):
    """Adds timedelta to datetime if only resulting datetime is not out of python datetime range"""
    milli = parse_date(dt, 'datetime')
    if milli + tdelta.total_seconds() * 1000 <= MAX_PYTHON_DATE:
        dt += tdelta
    return dt


def generate_pts_stats(pts, stats_list):
    stats = dict()
    for stat_name in stats_list:
        stats[stat_name] = SERIES_STATS_FUNCS[stat_name](pts['values'])
    return stats


def get_request_auth(req):
    for header in ['Authorization', 'Sj-Auth']:
        auth = parse_authorization_header(req.headers.get(header, ''))
        if auth:
            return auth.username, auth.password
        elif req.headers.get(header, '').startswith('Bearer '):
            tokens = req.headers.get(header).split()
            username, password = base64.b64decode(tokens[1]).decode().split(':')
            return username, password

    raise NotAllowedException('not authorized')


def extract_fields(fields_set, source, disambiguate_func=None):
    """
    Extracts fields by given set of fields.

    :param fields_set: array of fields names to extract (fieldA.fieldB, '*', fieldA.* etc)
    :param source: mapping with data to extract
    :param disambiguate_func: optional fields disambiguate function
    """
    fields = {}

    if disambiguate_func is not None:
        disambiguated_fields_set = map(lambda f: disambiguate_func(f), fields_set)
    else:
        disambiguated_fields_set = fields_set

    for user_field, field in zip(fields_set, disambiguated_fields_set):
        if user_field == '*':
            user_field = None
        elif user_field.endswith('.*'):
            user_field = user_field[:-2]
        elif user_field.startswith('='):
            val = extract_field(source, 'fields')
            if val is None:
                continue
            if 'series_id' in source:
                val.update({'sid': source['series_id']})
            fields[user_field] = val
            continue

        val = extract_field(source, field)
        if val is not None:
            if user_field is not None:
                fields[user_field] = val
            else:
                fields.update(val)
    return fields


def extract_field(obj, fields):
    """
    Extacts key from dict object. Supports dot notation
    :param obj: Dict object
    :param fields: Field to extract
    :return: field value
    >>> extract_field({'field_obj': {'field1': 1, 'field2': 2}}, 'field_obj')
    {'field1': 1, 'field2': 2}
    >>> extract_field({'field_obj': {'field1': 1, 'field2': 2}}, 'field_obj.field1')
    1
    """
    if not fields:
        return obj

    if fields == '*':
        return obj

    if isinstance(obj, dict):
        field, *remain = fields.split('.', 1)
        val = obj.get(field)
        if val is None:
            return val
        return extract_field(val, '.'.join(remain))
    elif isinstance(obj, list):
        extracted_list = list()
        for val in [extract_field(it, fields) for it in obj]:
            if val is not None:
                extracted_list.append(val)
        return extracted_list
    else:
        return


def unflattify(obj):
    """
    Converts flat mappings like {'level1.level2': 'value} to {'level1: {'level2': 'value'}}}
    """
    res = dict()
    for key, val in six.iteritems(obj):
        parts = key.split('.')
        leaf_obj = res
        leaf_key = parts[-1]
        for p in parts[:-1]:
            leaf_obj = leaf_obj.setdefault(p, dict())
        if isinstance(leaf_obj, list) and isinstance(val, list):
            for l_obj, v in zip(leaf_obj, val):
                l_obj[leaf_key] = v
        elif not isinstance(leaf_obj, list):
            leaf_obj[leaf_key] = val
    return res


def id_generator(size=20, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase, first_char=None):
    """
    Generates random ID
    :param size: length of ID
    :param chars: set of chars that can be used for ID
    :param first_char: set of chars that can be used for the first char if ID
    """
    #TODO can increase performance of this substantially by using something like '%030x' % random.randrange(16**30)
    id = ''.join(random.choice(chars) for _ in range(size))
    return id if first_char is None else random.choice(first_char) + id[1:]


def try_parse_operator_date(param, operator_name):
    '''
    Tries to parse operator timestamp param if it's in format <iso datetime>:<timezone>:<DST flag>.
    Timezone and DST flag are optional
    Returns iso-formated datetime string if parsed, otherwise returns original string.
    '''
    dt, timezone, dst_flag = None, None, None

    param_parts = param.rsplit(':', 2)
    if len(param_parts) == 3:
        dt, timezone, dst_flag = param_parts
        if dst_flag == 'y':
            dst_flag = False
        elif dst_flag == 'n':
            dst_flag = True
        else:
            dst_flag = None

    if dst_flag is None:
        param_parts = param.rsplit(':', 1)
        if len(param_parts) == 2:
            dt, timezone = param_parts

    try:
        timezone = pytz.timezone(timezone)
    except pytz.exceptions.UnknownTimeZoneError:
        timezone = None

    if timezone is None:
        dt = param
    else:
        try:
            dt = timezone.localize(datetime.fromisoformat(dt), is_dst=dst_flag).astimezone(pytz.utc).isoformat()
        except pytz.exceptions.AmbiguousTimeError:
            raise InvalidParametersException('ambiguous time in {} operator parameter'.format(operator_name))
        except ValueError:
            raise InvalidParametersException('failed to parse date with timezone in {} operator parameter'.format(operator_name))

    return dt
