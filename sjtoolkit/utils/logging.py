import json
from logging import Handler
import queue
import threading
import logging
import time
import sys
from shooju import Connection

from sjtoolkit.utils.utils import millis_utc_now

LOG_BATCH_SIZE = 100
TIMEOUT = 30


class BaseShoojuHandler(Handler):

    LOG_TYPE_NOTIFICATION = 'notification'
    LOG_TYPE_WARNING = 'warning'
    LOG_TYPE_ERROR = 'error'
    LOG_TYPE_TECHNICAL = 'technical'

    def __init__(self, sj, processor_id, token, *args, **kwargs):
        """
        Handler to store logs using the Shooju API

        :param sj: shooju.Connection instance that represents api connection 
        :param processor_id: importer id
        """
        super(BaseShoojuHandler, self).__init__()

        self._client = sj
        self._processor_id = processor_id
        self._token = token
        self._api_path = '/processors/{}/logs'.format(processor_id)

    def emit(self, record):
        """
        Saves the record
        """
        # before emitting any message check that the processor can run

        payload = self.prepare_payload(record)
        self.send_log(payload)
        if record.exc_info:
            # if there was an exception, send exception message as error, tb will
            # send above as a technical message
            message = '%s\n%s: %s' % (record.msg, record.exc_info[0].__name__, str(record.exc_info[1]))
            payload = self.prepare_payload(record, message=message, type=self.LOG_TYPE_ERROR)
            self.send_log(payload)

    def _get_shooju_log_type(self, local_type, exc=None):
        """
        Converts log level to Shooju levels
        """
        if local_type == logging.INFO:
            return self.LOG_TYPE_NOTIFICATION
        elif local_type == logging.WARN:
            return self.LOG_TYPE_WARNING
        elif local_type == logging.ERROR and not exc:
            return self.LOG_TYPE_ERROR
        elif local_type == logging.ERROR and exc:
            return self.LOG_TYPE_TECHNICAL
        elif local_type == logging.DEBUG:
            return self.LOG_TYPE_TECHNICAL
        else:
            return self.LOG_TYPE_TECHNICAL

    def prepare_payload(self, record, **kwargs):
        payload = {
            'type': self._get_shooju_log_type(record.levelno, record.exc_info),
            'message': self.format(record),
            'log_date': millis_utc_now(),
        }
        payload.update(kwargs)

        return payload

    def send_log(self, payload):
        try:
            self._client.raw.post(self._api_path, data_json=payload)
        except Exception as e:
            sys.stderr.write('Error sending logs to shooju: {}'.format(str(e)))
            sys.stderr.write(json.dumps(payload))


class CallablesLogHandler(BaseShoojuHandler):

    DONE_FLAG = object()

    def __init__(self, sj: Connection, processor_id, function, request_id, user, token=None):
        self._extra = {
            'func_name': function,
            'request_id': request_id,
            'user': user,
        }

        self._api_path = '/processors/{}/logs'.format(processor_id)

        self._queue = queue.Queue(maxsize=LOG_BATCH_SIZE)
        self.worker = threading.Thread(target=self._process_batch, args=(self._queue,))
        self.worker.setDaemon(True)
        self.worker.start()
        self._record_non_extra_attrs = set(logging.makeLogRecord({'message': ''}).__dict__)

        super(CallablesLogHandler, self).__init__(sj, processor_id, token, **self._extra)

    def prepare_payload(self, record, **kwargs):
        payload = super(CallablesLogHandler, self).prepare_payload(record)
        payload.update(task_type='callable', processor_id=self._processor_id, **self._extra)
        payload.update(kwargs)
        extra_keys = set(record.__dict__) - self._record_non_extra_attrs
        extra = {k: record.__dict__[k] for k in extra_keys}
        if extra:
            payload['custom'] = extra

        return payload

    def send_log(self, payload):
        self._queue.put(payload, True)

    def ensure_logs_sent(self):
        self._queue.put(self.DONE_FLAG)
        self.worker.join()  # making sure we sent all logs

    def _process_batch(self, _queue):
        batch = []
        done = False
        time_remains = TIMEOUT

        while not done:
            _s = time.perf_counter()
            try:
                msg = _queue.get(timeout=time_remains)
                if msg is not self.DONE_FLAG:
                    batch.append(msg)
                else:
                    done = True
            except queue.Empty:
                pass

            time_remains -= time.perf_counter() - _s
            do_flush = done or time_remains <= 0 or len(batch) >= LOG_BATCH_SIZE

            if batch and do_flush:
                self._send_log_batch(batch)
                del batch[:]
            time_remains = time_remains if time_remains > 0 else TIMEOUT

    def _send_log_batch(self, log_batch):
        try:
            payload = dict(log_batch=log_batch, token=self._token)
            self._client.raw.post(self._api_path, data_json=payload)
        except Exception as e:
            sys.stderr.write('Error sending logs to shooju: {}'.format(str(e)))
