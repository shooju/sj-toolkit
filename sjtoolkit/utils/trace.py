import typing
from dataclasses import dataclass

import flask

from sjtoolkit.exceptions import OutOfLimitsException
from sjtoolkit.utils.args_parse import parse_int
from sjtoolkit.utils.utils import millis_utc_now


@dataclass
class Limits:
    """
    Upstream request limits
    """
    max_call_depth: typing.Union[int, None]
    max_duration_seconds: typing.Union[int, None]


class UpstreamRequestContext:
    def __init__(
            self,
            request_id: str,
            call_depth: int,
            start_date_milli: int,
            limits: Limits
    ):
        """
        Initializes upstream request context

        :param request_id: original request id
        :param call_depth: call depth of CURRENT request
        :param start_date_milli: time when original request was created
        :param limits: limits info
        """
        self.request_id = request_id
        self.call_depth = call_depth
        self.start_date_milli = start_date_milli
        self.limits = limits

    @classmethod
    def from_request(
            cls,
            req: flask.Request,
            default_request_id: str = '',
            limits: Limits = None,
    ) -> 'UpstreamRequestContext':
        raw_request_data = req.args.get('upstream_request_context')
        default = UpstreamRequestContext(
            default_request_id,
            call_depth=0,
            start_date_milli=millis_utc_now(),
            limits=limits,
        )
        if not raw_request_data:
            return default

        parts = raw_request_data.split('-')
        if len(parts) != 3:  # probably some legacy
            return default

        request_id, call_depth, start_date_milli = parts
        call_depth = parse_int(call_depth, 'upstream_request_context')
        start_date_milli = parse_int(start_date_milli, 'upstream_request_context')
        return UpstreamRequestContext(request_id, call_depth, start_date_milli, limits)

    def assert_limits_not_exceeded(self):
        if (
                self.limits.max_duration_seconds and
                millis_utc_now() - self.start_date_milli > self.limits.max_duration_seconds * 1000
        ):
            raise OutOfLimitsException('request duration exceeded limit')

        if self.limits.max_call_depth and self.call_depth > self.limits.max_call_depth:
            raise OutOfLimitsException('request call depth exceeded limit')

    @property
    def is_root_request(self) -> bool:
        return self.call_depth == 0

    def child_context(self) -> 'UpstreamRequestContext':
        """
        Generates new raw upstream request context to pass to consequent calls
        """
        return UpstreamRequestContext(
            request_id=self.request_id,
            call_depth=self.call_depth + 1,
            limits=self.limits,
            start_date_milli=self.start_date_milli
        )

    @property
    def raw(self):
        return f'{self.request_id}-{self.call_depth}-{self.start_date_milli}'