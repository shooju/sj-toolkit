# -*- coding: utf-8 -*-
"""
    exceptions
    ~~~~~~~~~~~~~~~


    :copyright: (c) 2018 Shooju
"""
import six


class ShoojuException(Exception):
    # all shooju-sourced exceptions should inherit this class for
    # transparent API exception handling
    details = ""

    if six.PY2:
        def __unicode__(self):
            # if we override __str__ method BaseException will use it to implement __unicode__ instead of args[0] value
            return unicode(self.details) or unicode(self.message)

    def __str__(self):
        return self.details or super(ShoojuException, self).__str__()


class UnexpectedException(Exception):
    """
    All exception classes that inherits that are goes to Sentry
    """
    pass


class InvalidParametersException(ShoojuException):
    pass


class ExpressionException(ShoojuException):

    def __init__(self, msg, trace=None):
        """
        Failed expression error. Accepts optional `trace` object to pop-up
        expression debug info to response (mostly in case of entire request failure).
        """
        self.msg = msg
        self.trace = trace

    def __str__(self):
        return self.msg


class ExpressionSyntaxException(ExpressionException):
    pass


class RelativeDateRangeParseException(ShoojuException):
    """Parse date error."""


class ISOParseException(ShoojuException):
    """Parse date error."""


class BadRequest(ShoojuException):
    pass


class DateParseException(ShoojuException):
    """Parse date error."""


class MustUseSJVirtException(ShoojuException):
    pass


class ImporterException(Exception):
    """
    Processor Exception
    """


class NotAllowedException(ShoojuException):
    pass


class WriteConflictException(ShoojuException):
    pass


class OutOfLimitsException(ShoojuException):
    pass


class SjextensionsException(ShoojuException):
    pass
