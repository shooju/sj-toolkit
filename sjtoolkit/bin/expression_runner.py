from gevent.monkey import patch_all


patch_all()

import pytz
import time
import os
from threading import RLock # it's patched by gevent
import shooju
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
import sjts
import numpy
import pandas
import re
from flask import Flask, request, g
import six
import logging
from sjtoolkit.utils.trace import UpstreamRequestContext, Limits
from sjtoolkit.expressions.calculator import ExpressionCalculator
from sjtoolkit.expressions.client import EXPRESSION_ACCOUNT_HOST_HEADER
from sjtoolkit.expressions.namespace import AccountsManager
from sjtoolkit.exceptions import ExpressionException, NotAllowedException, OutOfLimitsException
from sjtoolkit.expressions.autocomplete import autocomplete
from sjtoolkit.expressions.utils import replace_sid_in_expression, pandas_series_to_milli_points
from sjtoolkit.expressions.trace import (
    XPRTracer,
    TraceApiHook,
    LimitsCheckHook,
    SimpleApiCallMetricsHook,
    XPRQueryTraceDebug,
    XPRQueryTrace
)
from sjtoolkit.series.args_parse import parse_series_id, parse_fields, parse_series_query
from sjtoolkit.utils.args_parse import parse_bool
from sjtoolkit.utils import sentry
from sjtoolkit.utils.utils import (
    camel_to_underscore,
    get_request_auth,
    millis_utc_now,
    parse_cron,
    parse_date,
    MsgPack,
)
from sjtoolkit.utils.metrics import metrics

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger('expression_runner')
logger.setLevel(logging.INFO)

autocomplete_lock = RLock()

CRON_FIELD_NAMES = ('second', 'minute', 'hour', 'day', 'month', 'day_of_week', 'timezone')
UTC = pytz.utc

SERIES_EXPRESSION_MAX_RECURSION_DEPTH = 10
MAX_REQUEST_DURATION_SECONDS = 600
MAX_POINTS_IN_DF = 10 * 1000000
DF_SCROLL_BATCH_SIZE = 200


def read_legacy_config():
    account = os.environ.get('EXP_RUNNER_ACCOUNT')
    user = os.environ.get('EXP_RUNNER_USER')
    api_key = os.environ.get('EXP_RUNNER_API_KEY')
    assert account, 'EXP_RUNNER_ACCOUNT is not set'
    assert all([user, api_key]), 'EXP_RUNNER_USER and EXP_RUNNER_API_KEY is not set'
    account_config = {
        'account_host': account,
        'user': user,
        'api_key': api_key,
    }
    return {account: account_config}, account


def read_config():
    env_n = lambda name, i: os.environ.get("{}_{}".format(name, i))
    accounts_total = int(os.environ.get('EXP_RUNNER_ACCOUNTS_TOTAL', 0))

    if not accounts_total:
        return read_legacy_config()

    config = {}
    default_account = None
    while True:
        i = len(config) + 1
        account_host = env_n('EXP_RUNNER_ACCOUNT', i)
        user = env_n('EXP_RUNNER_USER', i)
        api_key = env_n('EXP_RUNNER_API_KEY', i)
        host_override = env_n('EXP_RUNNER_HOST_OVERRIDE', i)

        if all(it is None for it in [account_host, user, api_key, host_override]):
            break

        assert account_host, "Account host is not set for account #{}".format(i)
        assert user, "User is not set for account #{}".format(i)
        assert api_key, "API key is not set for account #{}".format(account_host)

        data = {
            'account_host': host_override or account_host,
            'user': user,
            'api_key': api_key,
        }
        config[account_host] = data

    assert accounts_total == len(config), "Invalid accounts number found {}, expected {}".format(len(config), accounts_total)

    return config, default_account


RUNNER_CONFIG, DEFAULT_ACCOUNT = read_config()

SENTRY_DSN = os.environ.get('EXP_SENTRY_DSN')


app = Flask(__name__)

if __name__ != '__main__' and SENTRY_DSN:
    sentry.init_sentry(dsn=SENTRY_DSN)


@app.route('/exec/<datatype>', methods=('POST',))
def execute(datatype):
    """Executes Shooju expression.

    args:
        expression -- Shooju expression to execute
        max_points -- provides default for sj* funcs
        df -- provides default for sj* funcs
        dt -- provides default for sj* funcs
        date_format -- only used to parse df, dt parameters. Result is always milli
    """

    # this used for internal communication, so other than sjts is just for debug
    expression = request.json.get('expression')
    pre_operators = request.json.get('pre_operators')
    post_operators = request.json.get('post_operators')
    max_series = request.json.get('max_series') or 100
    sort = request.json.get('sort')

    if sort is None and datatype == 'series':
        sort = 'sid asc'

    if not expression:
        raise ExpressionException('expression parameter is required')
    date_format = request.json.get('date_format')
    orig_df, orig_dt = request.json.get('df'), request.json.get('dt')
    df, dt = (
        parse_date(request.json.get('df'), date_format),
        parse_date(request.json.get('dt'), date_format)
    )
    max_points = int(request.json.get('max_points') or 0)
    g_expression = request.json.get('g_expression')
    fields = request.json.get('fields')
    fields, _ = parse_fields(fields)
    include_job = request.json.get('include_job', False)
    include_timestamp = request.json.get('include_timestamp', False)

    expression_fields = {}
    if '_query' in fields:
        expression_fields['_query'] = '={}'.format(expression)

    start = time.time()
    user_id, _ = get_shooju_con_options(request)
    user_importer = ACCOUNTS_MANAGER.get_account_importers(g.account).users_namespaces.get(user_id)
    if user_importer is not None:
        user_namespace = dict(user_importer['ns'])
        caller_user = user_importer['caller_user']
    else:
        user_namespace = dict()  # todo we can load it right now actually, just a bit slow first expr run
        caller_user = user_id

    g.trace.start(f'={expression}',
                  pre_operators.split('@') if pre_operators else [],
                  post_operators.split('@') if post_operators else [],
                  g_expression,)

    # update global operators by one from expression tail
    ec = ExpressionCalculator(ACCOUNTS_MANAGER)
    pts, query_by_sid, fields_by_sid, \
    used_series_ids, all_queries, max_job_id, underlying_series, underlying_series_queries = ec.calculate(
        expression, df=df, dt=dt, max_points=max_points, user_namespace=user_namespace, caller_user=caller_user,
        g_expression=g_expression, pre_operators=pre_operators, series_fields=fields, include_job=include_job,
        include_timestamp=include_timestamp, max_series=max_series, sort=sort,
    )
    ec.sjutils.reset()

    tz = ec.ctx.get_result_timezone()
    if (
            tz is None
            and isinstance(pts, (pandas.Series, pandas.DataFrame))
            and isinstance(pts.index, pandas.DatetimeIndex)
            and pts.index.tz is not None
            and pts.index.tz.zone != 'UTC'
    ):
        tz = pts.index.tz.zone

    must_be_localized = True if pre_operators and 'localize' in pre_operators else False

    if isinstance(pts, pandas.Series):
        pts = pandas.DataFrame({
            pts.name or u'={}'.format(expression): pts
        })

    try:
        if isinstance(pts, (float, int)):
            has_inf = numpy.isinf(pts)
            has_points = True
        elif hasattr(pts, 'astype'):
            has_inf = pts.astype(float).isin([numpy.inf, -numpy.inf]).to_numpy().any()
            has_points = True
        else:
            raise ExpressionException("Invalid expression result type '{}'".format(type(pts).__name__))
    except (ValueError, TypeError):
        has_inf = False
        has_points = False

    # make sure we got expected datatype
    if datatype == 'series' and not isinstance(pts, (float, int)) and len(pts.columns) > 1:
        raise ExpressionException('too_many_series_returned')

    if isinstance(pts, (pandas.Series, pandas.DataFrame)) and has_inf:
        raise ExpressionException('expression result cannot contain special inf/-inf values')

    returning_df = datatype == 'dataframe'

    series = list()

    include_job = bool(max_job_id and include_job)
    g.trace.finish_timer()

    # prepare response depending on result datatype
    if isinstance(pts, pandas.DataFrame):
        for ix, (series_id, ts) in enumerate(pts.iteritems()):
            is_first_series = ix == 0
            ser = {'series_id': series_id}
            ser['xpr'] = dict()
            # ser['xpr']['series_ids'] = underlying_series.get(
            #     series_id, underlying_series.get(
            #         'name_not_available', used_series_ids))
            ser['xpr']['series_queries'] = sorted(set(underlying_series_queries.get(
                series_id, underlying_series_queries.get(
                    'name_not_available', [parse_series_query(q)[0] for q in all_queries]))))
            if not returning_df or is_first_series:
                ser['xpr']['trace'] = g.trace.to_json()

            if tz:
                ser['tz'] = {'timezone': tz}

            sid_wo_operators = parse_series_id(series_id)[0]
            if sid_wo_operators in query_by_sid:
                ser['xpr']['xpr_query'] = query_by_sid[sid_wo_operators]
            if has_points:
                points = pandas_series_to_milli_points(
                    ts, add_job_id=max_job_id, add_timestamp=millis_utc_now() if include_timestamp else None,
                    tz=tz, must_be_localized=must_be_localized)
                if points:
                    ser['points'] = points
            normalized_expression = replace_sid_in_expression("={}".format(series_id.lstrip('=').strip()))
            if normalized_expression in fields_by_sid:
                ser['fields'] = fields_by_sid[normalized_expression]
            elif sid_wo_operators in fields_by_sid:
                ser['fields'] = fields_by_sid[sid_wo_operators]
            elif len(pts.columns) == 1 and len(fields_by_sid) == 1:
                flds = list(fields_by_sid.values())[0]
                if flds:
                    ser['fields'] = flds
            if expression_fields:
                if 'fields' not in ser:
                    ser['fields'] = {}
                ser['fields'].update(expression_fields)
            series.append(ser)
    elif isinstance(pts, (float, six.integer_types)) or pts is None:
        unset_dates = {'MIN', 'MAX'}
        if not {orig_df, orig_dt}.difference(unset_dates):
            raise ExpressionException('at least df or dt parameter must be explicitly '
                                      'set if expression return non-array value')
        if orig_df in unset_dates:
            df = millis_utc_now()

        if orig_dt in unset_dates:
            dt = millis_utc_now()

        if isinstance(pts, (float, six.integer_types)) and include_job and not max_job_id:
            raise ExpressionException('cannot determine job_id for {} result type'.format(type(pts)))
        jobs_and_ts = []
        if include_job:
            jobs_and_ts.append(max_job_id)
        if include_timestamp:
            jobs_and_ts.append(millis_utc_now())

        if df != dt:
            points = [[df, pts] + jobs_and_ts, [dt, pts] + jobs_and_ts]
        else:
            points = [[df, pts] + jobs_and_ts]
        series_id = u'={}'.format(expression)
        ser = dict(series_id=series_id,
                   points=points,
                   xpr=dict(trace=g.trace.to_json()))
        # ser['xpr']['series_ids'] = underlying_series.get(
        #     series_id, underlying_series.get(
        #         'name_not_available', used_series_ids))
        ser['xpr']['series_queries'] = sorted(set(underlying_series_queries.get(
            series_id, underlying_series_queries.get(
                'name_not_available', [parse_series_query(q)[0] for q in all_queries]))))
        if series_id in query_by_sid:
            ser['xpr']['xpr_query'] = query_by_sid[series_id]
        if series_id in fields_by_sid:
            ser['fields'] = fields_by_sid[series_id]
        if expression_fields:
            if 'fields' not in ser:
                ser['fields'] = {}
            ser['fields'].update(expression_fields)
        series.append(ser)
    else:
        raise ExpressionException('function returned unexpected result')

    logger.debug(u'"{}" expression processed in {:.4}'.format(expression, time.time() - start))

    return sjts.dumps({'total': len(series), 'series': series, 'success': True, 'all_queries': all_queries},
                      include_job,
                      include_timestamp)


@app.route('/autocomplete')
def expression_autocomplete():
    expression = request.values.get('expression')
    line = request.values.get('line') or None
    column = request.values.get('column') or None

    user_id, _ = get_shooju_con_options(request)
    with autocomplete_lock:
        importer_loader = ACCOUNTS_MANAGER.get_account_importers(g.account)
        user_namespace = importer_loader.users_namespaces[user_id]
        sys_paths = importer_loader.get_user_sys_paths(user_id)
        res = autocomplete(expression, line=line, column=column, sys_path=sys_paths,
                           loaded_processors=user_namespace.get('loaded_processors', []))
    res['success'] = True
    return MsgPack.dumps(res)


@app.before_request
def before_request():
    if request.path == "/ping":
        return "OK"

    account = request.headers.get(EXPRESSION_ACCOUNT_HOST_HEADER, DEFAULT_ACCOUNT)
    if account not in RUNNER_CONFIG and not DEFAULT_ACCOUNT:
        raise ExpressionException('Unknown account "{}"'.format(account))
    g.account = RUNNER_CONFIG[account]['account_host'] if account in RUNNER_CONFIG else DEFAULT_ACCOUNT

    # this keeps track of this expression requests. need to handle and interrupt recursive expressions calls
    body = request.json or request.args  # for POST, and for GET
    debug = parse_bool(body.get('debug'))
    g.debug = debug
    g.trace = XPRTracer(xpr_query_class=XPRQueryTraceDebug if debug else XPRQueryTrace, collect_prints=debug)
    g.trace.start_timer()
    parent_upstream_request_context = UpstreamRequestContext.from_request(
        request,
        limits=Limits(
            max_call_depth=SERIES_EXPRESSION_MAX_RECURSION_DEPTH,
            max_duration_seconds=MAX_REQUEST_DURATION_SECONDS,
        )
    )

    g.upstream_request_context = parent_upstream_request_context.child_context()
    g.upstream_request_context.assert_limits_not_exceeded()
    extra_params = {
        'upstream_request_context': g.upstream_request_context.raw,
        'repdate_s3': body.get('repdate_s3', False),
    }

    g.user_ns_sj = shooju.Connection(
        g.account,
        *get_shooju_con_options(request),
        extra_params=extra_params,
        hooks=[
            LimitsCheckHook(g.upstream_request_context),
            TraceApiHook(g.trace),
            SimpleApiCallMetricsHook('app'),
        ],
        retries=2,
        retry_delay=10,
    )
    g.sj = shooju.Connection(
        g.account,
        *get_shooju_con_options(request),
        extra_params=extra_params,
        retries=2,
        retry_delay=10,
        hooks=[SimpleApiCallMetricsHook('app')]
    )
    g.request_timer = metrics.timer('request')

@app.errorhandler(Exception)
def error_handler(err):
    _stop_timer()
    if isinstance(err, (ExpressionException, shooju.ShoojuApiError, OutOfLimitsException)):
        error = camel_to_underscore(ExpressionException.__name__).replace('_exception', '')
    else:
        error = 'unexpected-exception'
        logger.exception('failed to process request: {}'.format(err))
        body = request.get_json() or {}
        extra = {
            'expression': body.get('expression'),
            'user': request.authorization.username if request.authorization else None,
            'account': g.account,
            'context': body.get('context'),
            'g_expression': body.get('g_expression'),
            'main_request_id': body.get('main_request_id'),
            'expression_request_id': body.get('expression_request_id')
        }
        sentry.capture_exception(extra=extra)
    metrics.count('error', error_type="request", error=error)
    serializer = sjts if request.path in ['/exec/series', '/exec/dataframe'] else MsgPack

    if isinstance(err, shooju.ShoojuApiError):  # here we got string like "error_name (..description text..)"
        # taking just description text, otherwise will get long ugly description text on recursive calls
        m = re.search(r'^[a-zA-Z_]+\s+\((.+)\)', six.text_type(err), re.DOTALL)
        # If description is lower than 10 characters, maybe some other () in description text,
        # anyways description is too short, show full length description
        description = m.group(1) if m and len(m.group(1)) > 10 else six.text_type(err)
    else:
        description = six.text_type(err.args[0])
    body = request.get_json() or {}
    resp = {'success': False, 'error': error, 'description': description}
    g.trace.finish_timer()
    if isinstance(err, shooju.ShoojuApiError):
        resp['trace'] = g.trace.to_json()
    else:
        # current expression failed or something went wrong here
        resp['trace'] = dict(expression=f'={body["expression"]}' if 'expression' in body else None,
                             error=description,
                             duration=g.trace.duration)

    return serializer.dumps(resp)

@app.after_request
def stop_timer(resp):
    _stop_timer()
    return resp


def _stop_timer():
    if hasattr(g, 'request_timer') and g.request_timer:
        g.request_timer.stop()
        g.request_timer = None

def get_shooju_con_options(req):
    """Extracts Shooju host and user credentials from Flask.request object"""
    try:
        username, password = get_request_auth(req)
    except NotAllowedException:
        raise ExpressionException('authentication required')

    return username.lower(), password


def serve_app(runner_app=app, port=5015):
    from gevent.pywsgi import WSGIServer
    http_server = WSGIServer(('', port), runner_app)
    logger.info('started expression runner. listening at 0.0.0.0:{}'.format(port))
    http_server.serve_forever()

load_attempt = 0
while True:
    try:
        ACCOUNTS_MANAGER = AccountsManager(list(RUNNER_CONFIG.values()))
        ACCOUNTS_MANAGER.load_namespaces()
        break
    except Exception as e:
        logger.error("Failed to load accounts: {}".format(e))

        # to avoid spamming on some intermittent errors we start alert on 20th (and repeat every 20th)
        if load_attempt and not load_attempt % 20:
            sentry.capture_exception()

        time.sleep(15)
    load_attempt +=1

scheduler = BackgroundScheduler()
# incremental reload
scheduler.add_job(ACCOUNTS_MANAGER.load_namespaces,
                  CronTrigger(**parse_cron('1 */5 * * * * UTC')))

# daily more heavier full reload
scheduler.add_job(lambda: ACCOUNTS_MANAGER.load_namespaces(True),
                  CronTrigger(**parse_cron('0 0 5 */1 * * UTC')))

scheduler.start()
