import re
import json
from logging import getLogger
import six
from datetime import date, datetime

from flask import send_file
import requests
import xlsxwriter

from sjtoolkit.utils.args_parse import parse_bool
from sjtoolkit.utils.utils import content_into_tmp, SERIES_STATS_FUNCS, MIN_LONG, MAX_LONG, render_date
from sjtoolkit.series.args_parse import parse_series_id

from ..exceptions import InvalidParametersException
from .date_params_operators import parse_date_with_operators, parse_date, parse_df_dt_with_operators

QUERY_SPECIAL_MEANING_CHARS = re.compile(r"[,^ :()<>=]")

logger = getLogger('sjtoolkit.charts')

cmp = lambda x, y: int(x > y) - int(x < y)


def excel_num_format(num_fmt_stgs):
    num_fmt_stgs = num_fmt_stgs or dict()
    prefix, suffix = num_fmt_stgs.get('prefix', ''), num_fmt_stgs.get('suffix', '')
    decimals = num_fmt_stgs.get('decimals_num', 2)
    thousands = num_fmt_stgs.get('thousands', True)
    if not thousands:
        fmt = '0'
    else:
        fmt = '#,##0'
    if decimals:
        fmt += '.{}0'.format('#' * (decimals - 1))
    fmt = "{}{}{}".format(prefix, fmt, suffix)
    return fmt


def _clean_ops(s):
    """Removes operators from series id"""
    return parse_series_id(s, skip_validate=True)[0]


def query_from_sid(identifier, id_field='sid'):
    """Makes series_query from series_id"""
    if id_field == 'series_id':
        id_field = 'sid'

    if identifier.startswith('='):
        return identifier

    if not bool(re.search(QUERY_SPECIAL_MEANING_CHARS.pattern + '@', identifier)):
        return u'{}={}'.format(id_field, identifier)

    raw_identifier, ops = parse_series_id(identifier, True)
    if QUERY_SPECIAL_MEANING_CHARS.search(raw_identifier) is None:
        return u'{}={}'.format(id_field, identifier)

    ops_str = u'@{}'.format('@'.join(ops)) if ops else ''
    return u'{}="{}"{}'.format(id_field, raw_identifier, ops_str)


def _chart_value_format_to_excel(val, num_fmt_stgs, date_format):
    """Generates Excel style value format string"""
    if isinstance(val, datetime):
        return date_format
    if isinstance(val, (int, float)):
        return excel_num_format(num_fmt_stgs)


def _remove_query_special_char(series_query):
    if series_query.startswith('$'):
        series_query = series_query[1:]
    return series_query


def get_columns_data(columns, read_series_func, params):
    fields_to_fetch = [it.get('field') for it in columns]
    if any(it is None for it in fields_to_fetch):
        raise InvalidParametersException("All columns should contain \"field\" attribute")

    args = {
        'fields': ','.join(fields_to_fetch),
    }

    args.update({k: v for k, v in six.iteritems(params) if k not in ['query']})
    args.setdefault('sort', 'sid asc')

    query = params.get('query', "")
    if not query.strip():
        raise InvalidParametersException("Query is not provided")
    args['query'] = query

    series_response, addin = read_series_func(args, {}, None)

    res = []

    row = []
    for column in columns:
        row.append((column.get('label', column['field']), None))
    res.append(row)

    for series_data in series_response:
        fields = series_data.get('fields', {})
        row = []
        for column in columns:
            field = column['field']
            row.append((fields.get(field), column.get('style')))
        res.append(row)

    return res


def viz_charts_data(chart_stgs, pts_stgs, table_stgs, query_stgs,
                    read_series_func, max_series, parse_operators_mapping=None):
    """
    Returns /series for chart based on stg=settings for chart.
    """
    # fields to show
    show_stats = False  # how do we know if stats are shown or not?
    table_columns = table_stgs.get('columns') or []
    column_headers = []
    fields_to_fetch = list()
    fields_to_show = []
    label_field = query_stgs.get('label_field')
    for c in table_columns:
        fields_to_show.append(c['field'])
        column_headers.append(c.get('title', c['field']))
    if show_stats:
        fields_to_show.extend([p for p in 'pts.min,pts.max,pts.avg,pts.std'.split(',') if p not in fields_to_show])

    fields_to_fetch = [f for f in fields_to_show if f != '_label']
    if label_field:
        fields_to_fetch.append(label_field)

    fields_to_fetch.append('_query')

    # points to show
    max_points = -1 if table_stgs.get('points') else 0
    numformat_obj = pts_stgs.get('numformat_obj')
    rnumformat_obj = pts_stgs.get('rnumformat_obj')
    series_settings_by_query = {s['query']: s for s in chart_stgs.get('series_settings_obj', [])}
    # the following two are based on series queries always ... never sids
    label_overrides = {k: v['label'] for k, v in six.iteritems(series_settings_by_query) if v.get('label')}
    custom_sort = query_stgs.get('custom_sort', [])
    # api params
    dates_obj = pts_stgs.get('dates_obj', {})
    df = dates_obj.get('left_abs_num', dates_obj.get('left_rel', 'MIN'))
    dt = dates_obj.get('right_abs_num', dates_obj.get('right_rel', 'MAX'))
    df, dt = parse_df_dt_with_operators(df, dt, 'milli', operators_mapping=parse_operators_mapping)

    args = {
        'df': df,
        'dt': dt,
        'max_points': max_points,
        'fields': ','.join(fields_to_fetch),
    }
    if query_stgs.get('global_sort'):
        args['sort'] = query_stgs['global_sort']

    series_query_mode = bool(query_stgs.get('series_queries'))
    series_response, addin = read_series_func(args, query_stgs, query_stgs.get('g_expression'), max_series)

    series_arr = list()
    for i, series in enumerate(series_response):
        fields = series.setdefault('fields', {})
        if series_query_mode: # hacky
            series_query = query_stgs['series_queries'][i]
        else:
            series_query = series['fields']['_query']

        fields['_label'] = label_overrides.get(series_query, fields.get(label_field))
        series['columns'] = [fields.get(f, None) for f in fields_to_show]
        if series_settings_by_query.get(series_query, {}).get('yaxis'):
            num_fmt_stgs = rnumformat_obj
        else:
            num_fmt_stgs = numformat_obj
        series.update({
            'numformat': num_fmt_stgs,
            'excel_numformat': excel_num_format(num_fmt_stgs)
        })
        series_arr.append(series)

    sort = query_stgs.get('sort')
    if sort == 'alpha' and label_field:
        series_arr = sorted(series_arr, key=lambda r: six.text_type(r['fields']['_label']))
    elif sort == 'custom':
        cs = custom_sort
        def _sort_key(r):
            sid_ = r['series_id']
            if sid_ not in cs:
                sid_ = _clean_ops(r['series_id'])
            return cs.index(sid_) if sid_ in cs else -1
        series_arr = sorted(series_arr, key=_sort_key)
    res = {'column_headers': list(column_headers),
           'series': series_arr,
           'date_format': pts_stgs.get('date_format', 'YYYY-MM-DD')}
    res.update(addin or {})

    df = parse_date_with_operators(df, 'milli', operators_mapping=parse_operators_mapping)
    dt = parse_date_with_operators(dt, 'milli', operators_mapping=parse_operators_mapping)
    if df > dt:
        res['points_desc'] = True

    return res, None


def charts_data_based_on_settings(stg, read_series_func, max_points=-1, g_expression=None,
                                  df=None, dt=None, parse_operators_mapping=None,
                                  global_params=None, facets=None,
                                  max_facet_values=None):
    """
    Returns /series for chart based on stg=settings for chart.
    """
    # what to show
    max_points = max_points if stg['value_type'] in ('mixed', 'point') else 0
    show_labels = stg.get('ignore_label_field') != 'y'
    global_params = global_params or dict()
    default_show_stats = parse_bool(stg.get('default_show_stats'))

    value_fields = stg.get('value_fields') or ''
    if isinstance(value_fields, list):  # todo this is temporary for backwards compatibility see https://shooju.atlassian.net/browse/SJP-4383
        value_fields = ','.join(value_fields)

    column_headers = [''] if show_labels else []
    if stg['value_type'] in ('mixed', 'field'):
        if value_fields:
            fields_to_show = [f.strip() for f in value_fields.split(',') if f.strip()]
        else:  # TODO: LEGACY - to remove
            fields_to_show = [f for f in stg.get('value', '').split(',') if f.strip()]
    else:
        fields_to_show = []

    if default_show_stats:
        fields_to_show.extend([p for p in 'pts.min,pts.max,pts.avg,pts.std'.split(',') if p not in fields_to_show])

    operators = '@localize' if stg.get('localize') == 'y' else ''
    operators += stg.get('aggregation') or ''  # date aggregation
    operators += (
        '@AGG:{}:{}'.format(stg['value_aggregation'], stg['label_field']) \
            if stg.get('value_aggregation') and stg.get('label_field') \
            else ''
    )
    for op_name in ['repdate', 'repfdate', 'reppdate', 'asof']:
        if op_name in stg:
            operators += '@%s:%s' % (op_name, stg[op_name])

    if 'filters' in stg and stg['filters']:
        if not isinstance(stg['filters'], list):
            raise InvalidParametersException('"filters" settings must be an array')

        operators += '@filter:%s' % '@filter:'.join(stg['filters'])

    numformat_obj = stg.get('numformat_obj')
    rnumformat_obj = stg.get('rnumformat_obj')
    series_settings_by_query = {s['query']: s for s in stg.get('series_settings_obj', [])}

    # the following two are based on an "id" that is query in series_queries mode and sid in everything else
    label_overrides = {k: v['label'] for k, v in six.iteritems(series_settings_by_query) if v.get('label')}
    custom_sort = stg.get('custom_sort', [])

    if df is not None or dt is not None:
        if df is None or dt is None:
            raise InvalidParametersException('cannot override "dt" and "df" separately')
        # todo apply operators and then re-parse...a bit ugly
        df, dt = parse_df_dt_with_operators(df, dt, 'milli', operators_mapping=parse_operators_mapping)
        df = parse_date(df, 'milli')
        dt = parse_date(dt, 'milli')
        if dt < df:
            raise InvalidParametersException('"dt" must be greater than "df"')

    # api params
    dates_obj = stg.get('dates_obj', {})
    fields_to_fetch = list()
    if stg.get('label_field'):
        fields_to_fetch.append(stg['label_field'])

    if fields_to_show:
        fields_to_fetch.extend(fields_to_show)

    df = df or dates_obj.get('left_abs_num', dates_obj.get('left_rel', 'MIN'))
    dt = dt or dates_obj.get('right_abs_num', dates_obj.get('right_rel', 'MAX'))

    df, dt = parse_df_dt_with_operators(df, dt, 'milli', operators_mapping=parse_operators_mapping)

    args = {
        'df': df,
        'dt': dt,
        'max_points': max_points,
        'fields': ','.join(fields_to_fetch),
        'operators': operators
    }
    if facets is not None:
        args['facets'] = facets
    if max_facet_values is not None:
        args['max_facet_values'] = max_facet_values

    g_expression = stg.get('g_expression') or g_expression

    # column headers
    lu = stg.get('field_labels_obj', [])
    if type(lu) is list:  # this is the new style; legacy stored it as simple dict
        lu = {el['field']: el['label'] for el in lu}

    show_all_fields = '*' in fields_to_show

    series_query_mode = bool(stg.get('series_queries'))

    if not series_query_mode:
        args.update({k: v for k, v in six.iteritems(global_params) if k not in ['query']})
        args.setdefault('sort', 'sid asc')
        query = stg.get('query') or ''
        if not query:
            query = global_params.get('query') or ''
        elif global_params.get('query') and not query.startswith('='):
            query = u'(%s) AND (%s)' % (query, global_params['query'])
        elif global_params.get('query') and query.startswith('='):
            query = query.replace(u'<CURRENT_QUERY>', global_params['query'])
        args['query'] = query

    series_response, addin = read_series_func(args, stg, g_expression)

    # in this case we have to collect all column names
    if show_all_fields:
        fields_to_show = set()
        for ser in series_response:
            if 'fields' not in ser:
                continue
            fields_to_show |= set(ser['fields'])
    column_headers += [lu.get(f, f) for f in fields_to_show]

    series_arr = list()
    for series in series_response:
        fields = series.get('fields', {})
        label = fields.get(stg.get('label_field', None), '')
        if series_query_mode:
            series['series_id'] = series['series_id'][1:] \
                if not series['series_id'].startswith('=') \
                else series['series_id']
        else:
            series['series_id'] = query_from_sid(series['series_id'])

        sid = _remove_query_special_char(series['series_id'])
        clean_sid = _remove_query_special_char(_clean_ops(sid))

        if sid in label_overrides:
            label = label_overrides[sid]
        elif clean_sid in label_overrides:
            label = label_overrides[clean_sid]

        series['label'] = label
        series['columns'] = [series['label']] if show_labels else []
        series['columns'] += [fields.get(f, None) for f in fields_to_show]

        # add prefixes/suffixes
        if _clean_ops(series['series_id']) in series_settings_by_query \
                and series_settings_by_query[_clean_ops(series['series_id'])].get('yaxis'):
            num_fmt_stgs = rnumformat_obj
        else:
            num_fmt_stgs = numformat_obj
        series.update({
            'numformat': num_fmt_stgs,
            'excel_numformat': excel_num_format(num_fmt_stgs)
        })
        series['numformat'] = num_fmt_stgs
        series_arr.append(series)
    sort = stg.get('sort', 'alpha')

    if sort == 'alpha':
        key_parser = int if all(isinstance(it['label'], (six.integer_types, float)) for it in series_arr) else six.text_type
        series_arr = sorted(series_arr, key=lambda r: key_parser(r['label']))
    elif sort == 'field':
        pass  # nothing to do; this was already sorted
    elif sort == 'custom':
        cs = custom_sort

        def _sort_key(r):
            sid_ = r['series_id']
            if sid_ not in cs:
                sid_ = _clean_ops(r['series_id'])
            return cs.index(sid_) if sid_ in cs else -1

        series_arr = sorted(series_arr, key=_sort_key)
    else:
        raise InvalidParametersException('unknown sort: {}'.format(sort))

    res = {'column_headers': list(column_headers),
           'series': series_arr,
           'date_format': stg.get('date_format', 'YYYY-MM-DD')}
    res.update(addin or {})

    df = parse_date_with_operators(df, 'milli', operators_mapping=parse_operators_mapping)
    dt = parse_date_with_operators(dt, 'milli', operators_mapping=parse_operators_mapping)
    if df > dt:
        res['points_desc'] = True

    return res, None


def find_dst_indices(series, indices, reverse=False, compare=cmp):
    res = [-1] * len(series)
    min_date = MAX_LONG if reverse else MIN_LONG
    current_date = None
    i = 0
    for ser in series:
        prev_date = min_date
        if ser.get('points') is not None:
            for j in range(indices[i], len(ser['points'])):
                point = ser['points'][j]
                if compare(point[0], prev_date) <= 0 and (current_date is None or point[0] == current_date):
                    current_date = point[0]
                    res[i] = j
                    break
                prev_date = point[0]

        i += 1

    if current_date is None:
        return res

    i = 0
    for ser in series:
        if res[i] >= 0:
            continue

        if ser.get('points') is not None:
            for j in range(indices[i], len(ser['points'])):
                point = ser['points'][j]
                time_diff = point[0] - current_date
                if reverse:
                    time_diff = -time_diff
                if time_diff >= 3600 * 1000:
                    res[i] = j
                    break

        i += 1

    return res


def iterate_through_series(series, indices, dst_indices, action):
    for i, ser in enumerate(series):
        if ser.get('points') is not None:
            index = indices[i]
            if index >= len(ser['points']):
                continue
            if indices[i] == dst_indices[i]:
                continue

            point = ser['points'][index]
            action(point, i)


def series_pts_to_table(series, reverse):
    global cur_date
    indices = [0] * len(series)
    dst_indices = find_dst_indices(series, indices)
    dst_active = any(d >= 0 for d in dst_indices)
    res = list()

    max_date = MIN_LONG if reverse else MAX_LONG
    compare = cmp if not reverse else lambda *a: cmp(*a) * -1

    while True:
        if dst_active:
            dst_complete = True
            for i in range(len(series)):
                if indices[i] < dst_indices[i]:
                    dst_complete = False

            if dst_complete:
                dst_indices = find_dst_indices(series, indices)
                dst_active = any(d >= 0 for d in dst_indices)

        cur_date = max_date

        def _action(point, i):
            global cur_date
            if compare(point[0], cur_date) < 0:
                cur_date = point[0]

        iterate_through_series(series, indices, dst_indices, _action)

        if cur_date == max_date:
            break

        line = [None] * (len(series) + 1)
        line[0] = cur_date

        def _action(point, i):
            global cur_date
            if compare(point[0], cur_date) > 0:
                return
            line[i + 1] = point[1]
            indices[i] += 1

        iterate_through_series(series, indices, dst_indices, _action)
        res.append(line)
    return res


def series_data_to_chart_table_viz(data):
    series = data['series']
    date_format = data['date_format']
    column_headers = data['column_headers']
    non_point_columns = 0
    for s in series:
        non_point_columns = max(non_point_columns, len(s['columns']))

    # prepare table of expected size filled by empty values
    pts_table = series_pts_to_table(series, bool(data.get('points_desc')))
    table = [[('', None) for _ in range(len(pts_table) + non_point_columns)] for _ in range(len(series) + 1)]

    for row, ser in enumerate(series):
        for col, val in enumerate(ser['columns']):
            table[row + 1][col] = val or None, None
            col_val = column_headers[col]
            table[0][col] = col_val, None

        for col, pt in enumerate(pts_table):
            dt = pt[0]
            val = pt[row + 1]
            try:
                val = float(val)
            except (TypeError, ValueError):
                pass
            dt = datetime.utcfromtimestamp(dt / 1000.)
            table[row + 1][col + non_point_columns] = val, _chart_value_format_to_excel(val, ser['numformat'], None)
            table[0][col + non_point_columns] = dt, _chart_value_format_to_excel(dt, None, date_format)

    # let user know if there are more than `max_series` series
    if data.get('total', len(series)) > len(series) and table:
        table.append(
            [('{} results is not shown'.format(data['total'] - len(series)), None)] + [('', None)] * (len(table[0]) - 1)
        )

    return table


def series_data_to_chart_table(stg, data):
    """Given series data generates matrix of ROWSxCOLS size"""
    # format settings
    # create mapping and collect all dates across all points
    series = data['series']
    date_format = data['date_format']
    column_headers = data['column_headers']
    non_point_columns = 0
    for s in series:
        non_point_columns = max(non_point_columns, len(s['columns']))

    # prepare table of expected size filled by empty values
    pts_table = series_pts_to_table(series, bool(data.get('points_desc')))
    table = [[('', None) for _ in range(len(pts_table) + non_point_columns)] for _ in range(len(series) + 1)]

    value_fields = stg.get('value_fields') or []
    if isinstance(value_fields, six.string_types):
        value_fields = value_fields.split(',')
    default_show_stats = parse_bool(stg.get('default_show_stats'))

    if default_show_stats:
        value_fields.extend([p for p in 'pts.min,pts.max,pts.avg,pts.std'.split(',') if p not in value_fields])

    for row, ser in enumerate(series):
        for col, val in enumerate(ser['columns']):
            table[row + 1][col] = val or None, None
            if value_fields:
                col_val = column_headers[col]
            else:
                col_val = stg.get('value')
            table[0][col] = col_val, None
        for col, pt in enumerate(pts_table):
            dt = pt[0]
            val = pt[row + 1]
            try:
                val = float(val)
            except (TypeError, ValueError):
                pass
            dt = datetime.utcfromtimestamp(dt / 1000.)
            table[row + 1][col + non_point_columns] = val, _chart_value_format_to_excel(val, ser['numformat'], None)
            table[0][col + non_point_columns] = dt, _chart_value_format_to_excel(dt, None, date_format)
    return table


def generate_columns_xlsx_file(table_data):
    return generate_xlsx_file(table_data, {}, False, 1, None, title='export', add_title_page=False)


def generate_xlsx_file(table, stg, transpose, default_tab, logo_url, title='Untitled', add_title_page=True):
    """Put generated data table into xlsx file and send it as response"""
    tmp_file_context = logo_path = None

    fp = six.BytesIO()
    wb = xlsxwriter.Workbook(fp, options={'in_memory': True})

    if isinstance(title, six.string_types) and title.startswith('='):
        title = " %s" % title

    if add_title_page:
        if logo_url:
            if not logo_url.startswith('http'):
                logo_url = 'https:{}'.format(logo_url)
            try:
                log_bin = requests.get(logo_url).content
                tmp_file_context = content_into_tmp(log_bin)
                logo_path = tmp_file_context.__enter__()
            except requests.RequestException:
                logger.exception('failed to fetch excel logo')

        # INTRO SHEET
        ws = wb.add_worksheet('Intro')
        white_bgrd = wb.add_format({'fg_color': 'white', 'pattern': 1})
        ws.set_column(1, 1, 55)
        for r in range(0, 80):
            for c in range(0, 20):
                ws.write(r, c, None, white_bgrd)

        ws.write(7, 1, title, wb.add_format({'font_name': 'Arial',
                                             'bold': True, 'font_size': 15,
                                             'fg_color': 'white',
                                             'pattern': 1}))
        ws.write_datetime(8, 1, date.today(),
                          wb.add_format({'align': 'left', 'font_name': 'Arial', 'bold': True, 'font_size': 10,
                                         'fg_color': 'white', 'pattern': 1, 'num_format': 'YYYY-MM-DD'}))
        ws.write(11, 1, stg.get('notes'),
                 wb.add_format({'font_name': 'Arial', 'bold': False, 'font_size': 10, 'fg_color': 'white',
                                'pattern': 1, 'text_wrap': True}))
        ws.write(16, 1, stg.get('footer'),
                 wb.add_format({'font_name': 'Arial', 'font_size': 10, 'bold': False, 'fg_color': 'white', 'pattern': 1}))
        if logo_path:
            ws.insert_image('B2', logo_path, {'x_scale': 0.7, 'y_scale': 0.7})

    # DATA
    ws = wb.add_worksheet('Data')
    default_style = wb.add_format({})

    formats = dict()

    for r, row in enumerate(table):
        for c, (value, style) in enumerate(row):
            if style and style not in formats:
                formats[style] = wb.add_format({'num_format': style})
            if isinstance(value, list):
                value = u', '.join(map(six.text_type, value))
            if isinstance(value, six.string_types) and value.startswith('='):
                value = " %s" % value  # escape '=' symbol
            elif isinstance(value, dict):
                value = json.dumps(value)
            ws.write(r if not transpose else c,
                     c if not transpose else r,
                     value, formats[style] if style in formats else default_style)

    defaults = {
        'mimetype': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'as_attachment': True,
        'attachment_filename': '{}.xlsx'.format(re.sub('[^0-9a-zA-Z_]+', '', title.replace(' ', '_')))
    }

    for i, worksheet in enumerate(wb.worksheets()):
        if i == default_tab:
            worksheet.select()
            worksheet.activate()

    wb.close()
    fp.seek(0)

    if tmp_file_context:
        tmp_file_context.__exit__(None, None, None)

    return send_file(fp, **defaults)
