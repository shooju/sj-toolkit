import numpy
import abc
from datetime import datetime
import pytz
import six

from ..exceptions import InvalidParametersException
from ..utils.args_parse import parse_int
from ..utils.utils import render_date, parse_date

from shooju import points_serializers


def parse_date_params_operators(date_str, operators_mapping):
    """
    Extracts df/dt params operators. Must provide known operators in operators_mapping. <operator> -> <DateParamOperator subclass>
    """
    if not operators_mapping:
        return date_str, None

    if '@' not in date_str:
        return date_str, None

    date_str, operators_str = date_str.split('@', 1)

    operators = list()
    for op in operators_str.split('@'):
        op_name = op.split(':')[0].lower()
        if op_name not in operators_mapping:
            raise InvalidParametersException('unknown date operator - @%s' % op_name)
        op_class = operators_mapping[op_name]
        operators.append(op_class(op, {}))

    return date_str, operators


def parse_date_with_operators(d, date_format, default="MIN", operators_mapping=None, relative_to=None):
    operators = None
    if isinstance(d, six.string_types):
        d, operators = parse_date_params_operators(d, operators_mapping)
    try:
        return parse_date(d, date_format, default, operators=operators, relative_to=relative_to)
    except Exception as e:
        raise InvalidParametersException(e)


def parse_df_dt_with_operators(df, dt, date_format, default_df='MIN', default_dt='MAX', operators_mapping={}):
    df = df if df is not None else default_df
    dt = dt if dt is not None else default_dt

    def inc_milli(milli, do_inc):
        return milli + 1000 if do_inc else milli - 1000

    def pop_excl(param):
        if param is None or not isinstance(param, six.string_types):
            return param, False
        items = param.split('@')
        value, ops = items[0], items[1:]
        rest_ops = [it for it in ops if it != 'excl']

        return '@'.join([value] + rest_ops), ops != rest_ops

    df, excl_df = pop_excl(df)
    df_milli = parse_date_with_operators(df, date_format, operators_mapping=operators_mapping)

    dt, excl_dt = pop_excl(dt)
    dt_milli = parse_date_with_operators(dt, date_format, operators_mapping=operators_mapping)

    is_forward = df_milli <= dt_milli

    if df not in ['MIN', 'MAX']:
        if excl_df:
            df_milli = inc_milli(df_milli, is_forward)
        df = render_date(df_milli, date_format)

    if dt not in ['MIN', 'MAX']:
        if excl_dt:
            dt_milli = inc_milli(dt_milli, not is_forward)
        dt = render_date(dt_milli, date_format)

    return df, dt


def merge_and_narrow_points_parrams(date_format, max_points_global, df_global, dt_global,
                                    pre_operators, post_operators, is_xpr_series, operators_mapping=None):
    """Merges and calculates narrowes date range for all @pre, @post and url params."""
    operators_mapping = operators_mapping or dict()

    pre_max_points = parse_int(pre_operators.max_points or max_points_global or 0, 'max_points')
    pre_max_points, pre_df, pre_dt = narrow_points_params(pre_max_points,
                                                          date_format,
                                                          df_global,
                                                          dt_global,
                                                          pre_operators.df,
                                                          pre_operators.dt,
                                                          operators_mapping=operators_mapping)
    if pre_operators.df_forced:
        pre_df = pre_operators.df

    if pre_operators.dt_forced:
        pre_dt = pre_operators.dt

    if not is_xpr_series:
        post_max_points = post_operators.max_points or pre_max_points or max_points_global
    else:
        post_max_points = post_operators.max_points or max_points_global

    post_max_points = parse_int(post_max_points, 'max_points')

    op_post_df, op_post_dt = (
        post_operators.df or pre_operators.df,
        post_operators.dt or pre_operators.dt
    )
    op_post_df_forced, op_post_dt_forced = (
            post_operators.df_forced if post_operators.df_forced is not None else pre_operators.df_forced,
            post_operators.dt_forced if post_operators.dt_forced is not None else pre_operators.dt_forced
    )

    if is_xpr_series:
        op_post_df, op_post_dt = post_operators.df, post_operators.dt
        op_post_df_forced, op_post_dt_forced = post_operators.df_forced, post_operators.dt_forced

    _, post_df, post_dt = narrow_points_params(
        post_max_points,
        date_format,
        df_global,
        dt_global,
        op_post_df,
        op_post_dt,
        operators_mapping=operators_mapping
    )
    if op_post_df_forced:
        post_df = op_post_df

    if op_post_dt_forced:
        post_dt = op_post_dt

    if not is_xpr_series:
        post_max_points = post_max_points or pre_max_points
    else:
        # todo do we need cases when post max_points is > 0, but we don't read any points (@pre@max_points=0)
        pre_max_points = pre_max_points or post_max_points
    return pre_max_points, post_max_points, pre_df, pre_dt, post_df, post_dt


def narrow_points_params(max_points, date_format, df_global, dt_global, df_local=None, dt_local=None, operators_mapping={}):
    if df_local is None and dt_local is None:
        return max_points, df_global, dt_global

    df_local = df_local or df_global or 'MIN'
    dt_local = dt_local or dt_global or 'MAX'

    df = parse_date_with_operators(df_global, date_format, operators_mapping=operators_mapping)
    dt = parse_date_with_operators(dt_global, date_format, operators_mapping=operators_mapping)

    df_local = parse_date_with_operators(df_local, date_format, operators_mapping=operators_mapping)
    dt_local = parse_date_with_operators(dt_local, date_format, operators_mapping=operators_mapping)

    is_forward_direction = df_local <= dt_local

    df, dt = sorted([df, dt])
    df_local, dt_local = sorted([df_local, dt_local])

    if df > dt_local or df_local > dt:  # doesn't intersect. Return no points in this case
        return 0, df_global, dt_global

    df = max(df, df_local)
    dt = min(dt, dt_local)

    if not is_forward_direction:
        df, dt = dt, df

    min_milli = parse_date_with_operators('MIN', date_format)
    max_milli = parse_date_with_operators('MAX', date_format)

    def render(value):
        if value == min_milli:
            return 'MIN'
        if value == max_milli:
            return 'MAX'
        return render_date(value, date_format)

    df = render(df)
    dt = render(dt)

    return max_points, df, dt


@six.add_metaclass(abc.ABCMeta)
class DateParamOperator(object):
    """
    Date params operators base class
    """

    def __init__(self, op, account_settings, *args, **kwargs):
        op_parts = op.split(':')
        self.op = op
        self.name = op_parts[0].upper()
        self.op_param = op_parts[1:] if len(op_parts) > 1 else None
        self.account_settings = account_settings

    @abc.abstractmethod
    def initialize(self, current_dt=None):
        pass

    @abc.abstractmethod
    def finalize(self, current_dt=None):
        pass

    @abc.abstractmethod
    def apply(self, delta=None, begin=None, prev_dt=None, current_dt=None):
        """
        The main operator logic

        :param delta: Parsed relativedelta instance
        :param begin: b - suffix period: m, h, d, w, M etc
        :param prev_dt: Previous calculated date in relative deltas chain. now() if this is first rel. delta element.
        :param current_dt: Calculated date after applying relative delta.
        :return:
        """
        pass


@six.add_metaclass(abc.ABCMeta)
class BaseCalendarDateOperator(DateParamOperator):
    def __init__(self, op, account_settings, *args, **kwargs):
        super(BaseCalendarDateOperator, self).__init__(op, account_settings, *args, **kwargs)
        if not self.op_param:
            raise InvalidParametersException('@cal requires calendar id parameter')

        self._calendar = None

    def initialize(self, current_dt=None):
        return current_dt

    def finalize(self, current_dt=None):
        return current_dt

    def apply(self, delta=None, begin=None, prev_dt=None, current_dt=None):
        """

        :param delta: Parsed relativedelta instance
        :param begin: b - suffix period: m, h, d, w, M etc
        :param prev_dt: Previous calculated date in relative deltas chain. now() if this is first rel. delta element.
        :param current_dt: Calculated date after applying relative delta.
        :return:
        """
        calendar_pts = self.fetch_calendar(cal_id=self.op_param[0])
        days_only = not any(getattr(delta, attr) for attr in ["years", "months"]) and not begin
        if not any(getattr(delta, attr) for attr in ["years", "months", "days"]) and delta:
            return datetime(*current_dt.timetuple()[:3])  # just must be today

        # for days we have a bit specific logic
        try:
            if days_only:
                res = self._apply_days_only(delta.days, calendar_pts, prev_dt=prev_dt)
            else:
                res = self._apply_other_periods(calendar_pts, current_dt)
        except IndexError:
            raise InvalidParametersException(
                '@cal offset is outside of the bounds of the calendar %s' % self.op_param[0]
            )

        return render_date(res, 'datetime')

    def _apply_other_periods(self, calendar_pts, dt):
        date_milli = parse_date(datetime(*dt.timetuple()[:3]), 'datetime')

        return calendar_pts[calendar_pts['dates'] <= date_milli]['dates'][-1]

    def _apply_days_only(self, days, calendar_pts, prev_dt):
        # this overrides "relative delta" logic, so we use prev_dt which is date before applying "relative delta"
        prev_dt_milli = parse_date(datetime(*prev_dt.timetuple()[:3]), 'datetime')
        if days > 0:
            return calendar_pts[calendar_pts['dates'] >= prev_dt_milli][days][0]
        else:
            return calendar_pts[calendar_pts['dates'] <= prev_dt_milli][days - 1][0]

    @abc.abstractmethod
    def fetch_calendar(self, cal_id):
        """This must be implemented on app end"""
        pass


class BaseSjclientCalendarDateOperator(BaseCalendarDateOperator):

    def fetch_calendar(self, cal_id):
        if self._calendar is not None:
            return self._calendar

        # todo we actually could have it in self._account_settings
        # But this requires some major refactor to pass acc settings in PARSE_DATE_OPERATORS in app, sjutils and sjvirt

        account_stgs = self.sjclient.raw.get('/account/config')['account_config']
        cal_query = account_stgs.get('calendar_series_query')

        if cal_query:
            series_query = u'(%s) AND (cal_id=%s)' % (cal_query, cal_id)
        else:
            series_query = u'cal_id=%s' % cal_id

        ser = self.sjclient.get_series(series_query, max_points=-1, serializer=points_serializers.milli_tuple)

        if not ser:
            raise InvalidParametersException('calendar %s is not found' % cal_id)

        pts = ser.get('points') or []

        self._calendar = numpy.array(list(map(tuple, pts)), dtype=[('dates', 'i8'), ('values', 'f8'), ])
        self._calendar = self._calendar[self._calendar['values'] == 1]
        return self._calendar

    @abc.abstractproperty
    def sjclient(self):
        pass
