import six
from typing import List
import re

from sjtoolkit.series.date_params_operators import narrow_points_params
from sjtoolkit.series.suffix_processors import validate_suffix_operators
from sjtoolkit.utils.utils import SERIES_STATS_FUNCS, parse_date, render_date, is_relative, try_parse_operator_date
from sjtoolkit.exceptions import InvalidParametersException, DateParseException, RelativeDateRangeParseException

SPLIT_OPERATORS_REGEX = re.compile(r'''(^.+?)(@[^{}'"]*)$''')
JMES_FIELD_REGEX = re.compile('@([a-z]+)\:')


def parse_fields_string(val):
    fields = []
    raw_by_parsed = {}
    raw_current = ''
    current = ''
    in_quotes = False
    for c in val:
        if c == '"':
            in_quotes = not in_quotes
            raw_current += c
        elif c == ',' and not in_quotes:
            fields.append(current)
            raw_by_parsed[raw_current] = current
            current = ''
            raw_current = ''
        else:
            current += c
            raw_current += c
    fields.append(current)
    raw_by_parsed[raw_current] = current

    return fields, raw_by_parsed


def _split_to_regular_fields_and_stats(fields):
    fields = [f.strip() for f in fields]
    # if field starts with '=' it's jinja operator
    fields = [f if f.startswith('=') or JMES_FIELD_REGEX.search(f) else f.lower() for f in fields]
    stats_fields = [f.replace('pts.', '') for f in fields if f and f.startswith('pts.')]
    return (
        [f for f in fields if f and not f.startswith('pts.')],
        [f for f in stats_fields if f in SERIES_STATS_FUNCS]
    )


def parse_fields(val):
    fields = []
    if isinstance(val, six.string_types):  # string and not empty
        fields, _ = parse_fields_string(val)
        return _split_to_regular_fields_and_stats(fields)
    elif isinstance(val, list):
        return _split_to_regular_fields_and_stats(val)
    else:
        return [], []


def parse_stats_list(value):
    stats = [v.strip().lower() for v in value.split(',')]
    unknown = set(stats).difference(set(SERIES_STATS_FUNCS))
    if unknown:
        raise InvalidParametersException('invalid series stats: {}'.format(','.join(unknown)))
    return stats


class ParamsOperators(object):
    """
    Parser for operators that overrides url parameters.
    """
    PARAMS_OPERATORS = ['asof', 'repdate', 'repfdate', 'reppdate',
                        'max_points', 'df', 'dt', 'cached']
    CAL = 'cal'
    SUPPORTS_LOCALIZATION = {'asof', 'repfdate', 'repdate', 'reppdate'}

    def __init__(self, operators: List[str], include_pre=True, include_post=True, pre_is_default=True) -> None:
        self.df_forced = None
        self.dt_forced = None
        for op in self.PARAMS_OPERATORS:
            setattr(self, op, None)

        _is_pre_chain = pre_is_default
        _is_post_chain = not pre_is_default
        prev_op = None

        seen = set()
        for o in operators:
            force_modifier = False
            o_lower = o.lower()
            op_name, op_params = o.split(':', 1) if ':' in o else (o, None)
            if op_name.endswith('!'):
                force_modifier = True
                op_name = op_name.rstrip('!')

            if o_lower in {'pre', 'post'}:
                _is_pre_chain = o_lower == 'pre'
                _is_post_chain = not _is_pre_chain
                prev_op = None
            # we're ignoring all that is explicitly in @pre. We pass them further when reading series.
            if not include_pre and _is_pre_chain:
                continue
            elif not include_post and _is_post_chain:
                continue

            if op_name in self.SUPPORTS_LOCALIZATION:
                op_params = try_parse_operator_date(op_params, op_name)  # convert to UTC iso if possible

            if op_name in self.PARAMS_OPERATORS:
                prev_op = op_name
                setattr(self, op_name, op_params)
                setattr(self, f'{op_name}_forced', force_modifier)
                if op_name in seen:
                    InvalidParametersException('@{} operator cannot be used more than once'.format(op_name))

                seen.add(op_name)
            elif op_name == self.CAL \
                    and prev_op in self.SUPPORTS_LOCALIZATION \
                    and is_relative(getattr(self, prev_op)):
                setattr(self, prev_op, '{}@cal:{}'.format(getattr(self, prev_op), op_params))
                prev_op = None

    def __bool__(self):
        return any(getattr(self, p) for p in self.PARAMS_OPERATORS)


def parse_operators(series_id=None, query=None,
                    global_operators=None, skip_validate=False):
    operators = list()
    operators_str = ''
    agg_operators = list()
    if series_id and not series_id.startswith('='):
        _, operators = parse_series_id(series_id, skip_validate=skip_validate)
        operators_str = '@'.join(operators)
    elif query and not query.startswith('='):
        q, operators, agg_operators = parse_series_query(query)
        operators_str = '@'.join(operators)

    # append global request operators
    if global_operators:
        operators_str += global_operators
        global_operators = [o.strip() for o in global_operators.split('@') if o]
        operators.extend([o for o in global_operators if not o.lower().startswith('agg')])
        agg_operators.extend(parse_and_validate_agg_operators(
            [o for o in global_operators if o.lower().startswith('agg')])
        )

    if not skip_validate:
        validate_suffix_operators(operators)

    return operators_str, operators, agg_operators


def parse_series_params_operators(series_params_operators, date_format):
    # we can't use these more than once
    for op in ['repdate', 'repfdate', 'reppdate', 'asof']:
        if len({o for o in series_params_operators if o.lower().startswith(op)}) > 1:
            raise InvalidParametersException('@{} operator cannot be used more than once'.format(op))
    params = dict()

    for op in series_params_operators:
        parts = op.split(':')
        op_name, op_params = parts[0], parts[1:]
        if op_name == 'asof':
            params.update(_parse_asof_operator_params(op_params, date_format))
        elif op_name in ['repdate', 'repfdate', 'reppdate']:
            params.update(_parse_rep_date_operator_params(op_params))
    return params


def _parse_rep_date_operator_params(params_str):
    params = dict()
    param = ':'.join(params_str)  # do like this b/c we got iso with a lot of ':'
    try:
        params['reported_date'] = parse_date(param, 'iso')
    except DateParseException:
        raise InvalidParametersException('@repdate parameter must be iso formatted date')
    return params


def _parse_asof_operator_params(params_str, date_format):
    params = dict()
    snapshot_dt = None
    param = ':'.join(params_str)
    if param.startswith('j'):
        try:
            params['snapshot_job_id'] = int(param[1:])
        except (ValueError, TypeError):
            raise InvalidParametersException('snapshot job must be integer')
    else:
        try:
            snapshot_dt = int(param)
        except (ValueError, TypeError):
            try:
                snapshot_dt = parse_date(param, 'iso')
            except (DateParseException, RelativeDateRangeParseException):
                raise InvalidParametersException('not able to parse snapshot date')
    if snapshot_dt is not None:
        # TODO all dates df/dt/reported_date/snapshot_date must be milli. doesn't make sense to parse it each time
        params['snapshot_date'] = render_date(snapshot_dt, date_format)
    return params


def parse_series_query(query, base_operators=''):
    """Extracts and validates operators from series query. Appends global request operators at the end."""
    query, operators = split_operators(query)
    if base_operators:
        operators = u'{}@{}'.format(operators, base_operators) if operators else base_operators

    agg_operators = []
    if operators:
        operators = [o.strip() for o in operators.split('@') if o.strip()]
        agg_operators = [op for op in operators if op.lower().startswith('agg:')]
        operators = [op for op in operators if not op.lower().startswith('agg:')]
    else:
        operators = list()
    agg_operators = parse_and_validate_agg_operators(agg_operators)
    return query, operators, agg_operators


def split_operators(string):
    """Splits query/sid with operators to query/sid and operators strings"""
    m = SPLIT_OPERATORS_REGEX.search(string)
    if not m:
        return string, ''
    else:
        return m.group(1), m.group(2) or ''


def parse_and_validate_agg_operators(agg_operators):
    """Parses and validates @agg operators string"""
    try:
        agg_operators = [a.split(':') for a in agg_operators if a]
        if agg_operators:
            assert all(a[0].lower() == 'agg' for a in agg_operators)
            assert all(len(a) == 3 for a in agg_operators)
        agg_operators = [a[1:] for a in agg_operators]
    except AssertionError:
        raise InvalidParametersException('agg operator must be in format @agg:<S|A|C>:<field>')
    return agg_operators


def parse_series_id(series_id, skip_validate=False):
    """
    Splits series id in series id and operators. If operators are present, it validates them
    raising a InvalidParametersException if the operator in invalid.

    :param series_id: series id with operators
    :param skip_validate: if True doesn't raise exception if got unknown operator
    :return: :raise InvalidParametersException: a tuple of series_id, operators
    """
    series_id, operators = split_operators(series_id)
    operators = [o.strip() for o in operators.split('@') if o.strip()]
    if not skip_validate:
        validate_suffix_operators(operators)
    return series_id, operators
