import collections
import typing
from collections import defaultdict, namedtuple
from datetime import datetime, timedelta
import re
import numpy as np
import math
import time
import calendar
from itertools import groupby
import itertools
import six
import weakref
from six.moves import xrange

import pytz
from pytz import UnknownTimeZoneError
from dateutil.relativedelta import relativedelta
import pandas as pd
from shooju.utils.convert import milli_tuple_naive_to_pandas_tz_aware

from sjtoolkit.exceptions import InvalidParametersException
from sjtoolkit.utils.series import PTS_DTYPE, COLUMN_TYPES, field_type
from sjtoolkit.utils.utils import (adjust_date, parse_date, render_date, _parse_relative_delta,
                                   RELATIVE_DELTA_RE, PERIODS_MAP, MIN_PYTHON_DATE, MAX_PYTHON_DATE)

NANOSECONDS = 1000000  # nanoseconds in milli
MAX_PANDAS_DATE = parse_date('2262-01-01', 'iso')
MIN_PANDAS_DATE = parse_date('1700-01-01', 'iso')


SPLIT_OPERATORS_REGEXP = re.compile(r'@(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)')
DEFAULT_AUTO_AGG_SIZE = 1000
NO_AGG_NEEDED = False  # this is to tell user somehow that auto agg operator did nothing
DAY_MILLI = 24 * 60 * 60 * 1000


EMPTY_POINTS = np.array([], dtype=PTS_DTYPE)


class OperatorsRegistry(object):

    def __init__(self):
        self._operators = dict()

    def register(self, name, operator_class):
        self._operators[name] = operator_class

    def get_by_name(self, name):
        if name not in self._operators:
            raise InvalidParametersException('operator %s is not supported' % name)

        return self._operators[name]

    @property
    def registered_operators(self):
        return [k.upper() for k in self._operators.keys()]

    def is_registered(self, name):
        return name in self._operators


registry = OperatorsRegistry()


def get_delta_from_time_str(time_str, sub_one=False):
    time_formats = {
        'y': 'years',
        'm': 'months',
        'w': 'weeks',
        'd': 'days',
        'h': 'hours',
    }
    op = time_str[-1].lower()
    if op in time_formats:
        delta_int = int(time_str[:-1])-(1 if sub_one else 0)
        return relativedelta(**{time_formats[op.lower()]:delta_int})
    else:
        raise InvalidParametersException('trailing times must be expressed using one of: {}'.format(', '.join(time_formats)))


def add_delta(d_in, date_format, time_delta, fallback=None):
    #TODO this code repeats and these calculations are a waste
    d = parse_date(d_in, date_format) / 1000.0
    try:
        d = datetime.utcfromtimestamp(d)
    except ValueError as ex:
        str_ex = six.text_type(ex)
        # todo msg depends on python version. will remove when switched to py3
        if 'year' in str_ex and 'is out of range' in str_ex:
            return d_in
        raise ex
    try:
        d -= time_delta
    except (OverflowError, ValueError) as ex:
        str_ex = six.text_type(ex)
        # todo msg depends on python version. will remove when switched to py3
        if isinstance(ex, ValueError) and ('year' not in str_ex or 'is out of range' not in str_ex):
            raise
        if not fallback:
            raise InvalidParametersException('year is out of range')
        else:
            return fallback
    milli = parse_date(d, "datetime")
    # if inclusive:
    #     milli -= 1
    return render_date(milli, date_format)


#TODO assume that all dates in and out are milli unless otherwise...
#TODO will need to convert default dates to macro in future

FIRST_DAY_OF_WEEK = 1
AGGREGATE_LEVELS = {
    'A': 1,
    's': 1,
    'm': 2,
    'h': 3,
    'H': 3,
    'd': 4,
    'D': 4,
    'w': 5,
    'W': 5,
    'M': 6,
    'q': 7,
    'Q': 7,
    'y': 8,
    'Y': 8
}


AGGREGATE_AS = list(AGGREGATE_LEVELS.keys())
AGGREGATE_BS = ['S', 'WA', 'A', 'C', 'H', 'L', 'E', 'B']
AGGREGATE_OPERATORS = []
for a in AGGREGATE_AS:
    for b in AGGREGATE_BS:
        op = "%s%s" % (a, b)
        if op not in ['WWA', 'MWA', 'TWA']:
            AGGREGATE_OPERATORS.append(op)

AGG_LEVELS_MILLIS_LENGTH = (
    ('y', 31536000000),
    ('Y', 31536000000),
    ('q', 7776000000),
    ('Q', 7776000000),
    ('M', 2419200000),
    ('w', 604800000),
    ('W', 604800000),
    ('d', 86400000),
    ('D', 86400000),
    ('h', 3600000),
    ('H', 3600000),
    ('m', 60000),
    ('s', 1000),
    ('A', 1000),  # auto agg. leave same as s
)

AGG_LEVELS_MILLIS_LENGTH_BY_NAME = dict(AGG_LEVELS_MILLIS_LENGTH)  ## todo ???


def last(values):
    return values[-1]


def first(values):
    return values[0]


def count(values):
    return len(values)


AGG_OP_FUNCS = {
    'S': np.sum,
    'A': np.mean,
    'C': count,
    'E': last,
    'B': first,
    'H': np.max,
    'L': np.min,
}


def _milli_to_week(milli):
    dt = datetime.utcfromtimestamp(milli / 1000.)
    dt = dt.date() - timedelta(days=dt.weekday())
    return dt


def week(d, every=1):
    """Weekly grouper"""
    res = _week_to_bucket(_milli_to_week(d[0]), every)
    return res.year, res.month, res.day


def week_end(d, every=1):
    c_week = _week_to_bucket(_milli_to_week(d[0]), every)
    p_week = _week_to_bucket(_milli_to_week(d[0] - 1), every)
    if c_week != p_week:
        res = p_week
    else:
        res = c_week
    return res.year, res.month, res.day


def _week_to_bucket(d, bucket_len):
    wk = d.isocalendar()[1]
    round_to_week = wk // bucket_len * bucket_len
    d = d + timedelta(days=(round_to_week - wk) * 7)
    return d


def month(d, every=1):
    """Monthly grouper"""
    res = list(time.gmtime(d[0] / 1000.)[:2])
    res[1] = (res[1] - 1) // every * every + 1
    return tuple(res)


def year(d, every=1):
    """Year grouper"""
    yr = time.gmtime(d[0] / 1000.)[:1][0]
    return yr // every * every,


class _AggGrouper(object):
    MINUTE_MILLI = 60 * 1000
    HOUR_MILLI = 60 * MINUTE_MILLI
    DAY_MILLI = 24 * HOUR_MILLI
    WEEK_MILLI = 7 * DAY_MILLI

    def __init__(self, every):
        self._every = every
        self._last_time_tuple = None
        self._last_milli = None
        self._current_min_milli = float('-inf')
        self._current_max_milli = float('-inf')

    def _timegm(self, components):
        if self._last_time_tuple == components:
            return self._last_milli
        dt = tuple(components) + DATE_ZEROS[len(components):]
        milli = calendar.timegm(dt) * 1000

        self._last_time_tuple = components
        self._last_milli = milli
        return milli


class _Minute(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        dt = list(time.gmtime(d / 1000.)[:5])
        dt[4] = dt[4] // every * every

        milli = self._timegm(dt)
        self._current_min_milli = milli
        self._current_max_milli = milli + self.MINUTE_MILLI * every
        return milli


class _Hour(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        dt = list(time.gmtime(d / 1000.)[:4])
        dt[3] = dt[3] // every * every

        milli = self._timegm(dt)
        self._current_min_milli = milli
        self._current_max_milli = milli + self.HOUR_MILLI * every
        return milli


class _Day(_AggGrouper):
    """Day grouper"""

    def __call__(self, point):
        every = self._every
        d = point[0]
        if every == 1:
            return d // self.DAY_MILLI * self.DAY_MILLI

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        y, m, d = time.gmtime(d / 1000.)[:3]
        d = (d - 1) // every * every + 1

        milli = self._timegm((y, m, d))
        self._current_min_milli = milli
        self._current_max_milli = milli + self.DAY_MILLI * every
        return milli


class _Week(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        week = _week_to_bucket(_milli_to_week(d), every)
        dt = week.year, week.month, week.day

        milli = self._timegm(dt)
        self._current_min_milli = milli
        self._current_max_milli = milli + self.WEEK_MILLI * every
        return milli


class _Month(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        date_begin = list(time.gmtime(d / 1000.)[:2])
        date_end = list(date_begin)
        date_end[1] = (date_end[1] % 12) + 1

        date_begin[1] = (date_begin[1] - 1) // every * every + 1
        date_end[1] = (date_end[1] - 1) // every * every + 1

        dt_begin = tuple(date_begin)
        dt_end = tuple(date_end)

        self._current_min_milli = self._timegm(dt_begin)
        self._current_max_milli = self._timegm(dt_end)
        return self._current_min_milli


class _Quarter(_Month):
    def __init__(self, every):
        super(_Quarter, self).__init__(3 * every)


class _Year(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli <= d < self._current_max_milli:
            return self._current_min_milli

        year_begin = time.gmtime(d / 1000.)[:1][0]
        year_begin = year_begin // every * every
        year_end = year_begin + every

        self._current_min_milli = self._timegm([year_begin])
        self._current_max_milli = self._timegm([year_end])
        return self._current_min_milli


class _MinuteEnd(_AggGrouper):
    def __init__(self, every):
        super(_MinuteEnd, self).__init__(every)
        self._per = self.MINUTE_MILLI * every

    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_max_milli

        per = self._per
        if d % per:
            dt = list(time.gmtime((d + per) / 1000)[:5])
        else:
            dt = list(time.gmtime(d / 1000)[:5])
        dt[4] = dt[4] // every * every

        milli = self._timegm(tuple(dt))
        self._current_min_milli = milli - self.MINUTE_MILLI * every
        self._current_max_milli = milli
        return milli


class _HourEnd(_AggGrouper):
    def __init__(self, every):
        super(_HourEnd, self).__init__(every)
        self._per = self.HOUR_MILLI * every

    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_max_milli

        per = self._per
        if d % per:
            dt = list(time.gmtime((d + per) / 1000)[:4])
        else:
            dt = list(time.gmtime(d / 1000)[:4])
        dt[3] = dt[3] // every * every

        milli = self._timegm(tuple(dt))
        self._current_min_milli = milli - self.HOUR_MILLI * every
        self._current_max_milli = milli
        return milli


class _DayEnd(_AggGrouper):
    def __init__(self, every):
        super(_DayEnd, self).__init__(every)
        self._per = self.DAY_MILLI * every

    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_min_milli

        per = self._per
        if (d - self.DAY_MILLI) % per:
            dt = list(time.gmtime((d + per) / 1000)[:3])
        else:
            dt = list(time.gmtime(d / 1000)[:3])
        dt[2] = dt[2] // every * every

        milli = self._timegm(tuple(dt))
        self._current_min_milli = milli - self.DAY_MILLI * every
        self._current_max_milli = milli
        return self._current_min_milli


class _WeekEnd(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_max_milli

        c_week = _week_to_bucket(_milli_to_week(d), every)
        p_week = _week_to_bucket(_milli_to_week(d - 1), every)
        if c_week != p_week:
            week = p_week
        else:
            week = c_week
        dt = week.year, week.month, week.day

        milli = self._timegm(dt)
        self._current_min_milli = milli - self.WEEK_MILLI * every
        self._current_max_milli = milli
        return milli


class _MonthEnd(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_max_milli

        c_month = list(time.gmtime(d / 1000)[:2])
        p_month = list(time.gmtime(d / 1000 - 1)[:2])
        c_month[1] = (c_month[1] - 1) // every * every + 1
        p_month[1] = (p_month[1] - 1) // every * every + 1

        if c_month != p_month:
            date_end = tuple(p_month)
            d = datetime(*(list(p_month) + [1])) - relativedelta(months=every)
            date_begin = d.year, d.month
        else:
            date_end = tuple(c_month)
            date_begin = tuple(p_month)

        self._current_min_milli = self._timegm(date_begin)
        self._current_max_milli = self._timegm(date_end)
        return self._current_max_milli


class _QuarterEnd(_MonthEnd):
    def __init__(self, every):
        super(_QuarterEnd, self).__init__(3 * every)


class _YearEnd(_AggGrouper):
    def __call__(self, point):
        every = self._every
        d = point[0]

        if self._current_min_milli < d <= self._current_max_milli:
            return self._current_max_milli

        c_year = time.gmtime(d / 1000)[0] // every * every
        p_year = time.gmtime(d / 1000 - 1)[0] // every * every
        if c_year != p_year:
            year_end = p_year
            year_begin = p_year - every
        else:
            year_begin = p_year
            year_end = c_year

        self._current_min_milli = self._timegm([year_begin])
        self._current_max_milli = self._timegm([year_end])
        return self._current_max_milli


AGG_PERIODS_DEPRECATED = {
    'D': _Day(1),
    'Y': _Year(1),
    'Q': _Quarter(1),
    'M': _Month(1),
    'W': _Week(1),
    'H': _Hour(1),
    'MIN': _Minute(1)
}

AGG_PERIODS = {
    'd': _Day,
    'y': _Year,
    'q': _Quarter,
    'M': _Month,
    'w': _Week,
    'h': _Hour,
    'm': _Minute
}

AGG_END_PERIODS = {
    'd': _DayEnd,
    'y': _YearEnd,
    'q': _QuarterEnd,
    'M': _MonthEnd,
    'w': _WeekEnd,
    'h': _HourEnd,
    'm': _MinuteEnd
}

AGG_BUCKET_LENGTH_VALIDATORS = {
    'd': lambda x: x > 0,
    'y': lambda x: x > 0,
    'q': lambda x: 4 % x == 0 and x > 0,
    'M': lambda x: 12 % x == 0 and x > 0,
    'w': lambda x: 55 > x > 0,
    'h': lambda x: 24 % x == 0 and x > 0,
    'm': lambda x: 60 % x == 0 and x > 0
}

AGG_OPERATORS = dict()


class BaseOperator(object):
    def __init__(self, op, body, account_settings, operators_chain, is_forced=False):
        operators_chain = weakref.ref(operators_chain)
        self.op = op
        self.name, self.op_params = BaseOperator.split_op(op)
        self.body = body
        self.account_settings = account_settings or {}
        self.period_end = False
        self.operators_chain = operators_chain
        self.is_forced = is_forced

    def validate(self):
        pass

    @property
    def length(self):
        return 1

    @property
    def period(self):
        return self.op[0].upper()

    @property
    def agg_level(self):
        if self.period not in AGGREGATE_LEVELS:
            return 1
        return AGGREGATE_LEVELS[self.period]

    @property
    def length_milli(self):
        if self.period not in AGG_LEVELS_MILLIS_LENGTH_BY_NAME:
            return
        return AGG_LEVELS_MILLIS_LENGTH_BY_NAME[self.period] * self.length

    @property
    def relative_delta(self):
        pass

    @property
    def agg_func(self):
        return AGG_OP_FUNCS[self.name[-1].upper()]

    def adjust_date(self, df, dt):
        if self.agg_level is None or self.agg_level <= 0:
            return df, dt

        normal = df <= dt
        if df not in ["MAX", "MIN"]:
            if normal:
                if not self.period_end:
                    df = adjust_date(df, 'milli', self.agg_level, up=True, inclusive=False, length=self.length)
                else:
                    df = adjust_date(df - 1, 'milli', self.agg_level, up=False, inclusive=False, length=self.length) + 1
            else:
                # Both ( not period_end - period beginning ) and ( period_end ) fetch the next day.
                # If period_end, include 00:00 ( append 1 ),
                # since in adjust_date there is a - 1 calculation if inclusive=True
                df = adjust_date(df, 'milli', self.agg_level, up=True, inclusive=True, length=self.length) + \
                     (1 if self.period_end else 0)
        if dt not in ["MAX", "MIN"]:
            if normal:
                dt = adjust_date(dt, 'milli', self.agg_level, up=True, inclusive=True, length=self.length) + \
                     (1 if self.period_end else 0)
            else:
                if not self.period_end:
                    dt = adjust_date(dt, 'milli', self.agg_level, up=True, inclusive=False, length=self.length)
                else:
                    dt = adjust_date(dt - 1, 'milli', self.agg_level, up=False, inclusive=False, length=self.length) + 1
        if (df <= dt) != normal:
            raise InvalidParametersException("date range is not wide enough for the suffix operator")
        return df, dt

    # sjvirt
    def __str__(self):
        return '@{}'.format(self.op) if not self.is_forced else '@{}!'.format(self.op)

    def get_timezone(self):
        """
        Depending on relative @localize position it would either None (if not localized yet), or timezone name
        """
        operators_chain = self.operators_chain()
        current_op_pos = operators_chain.index(self)

        for o in operators_chain[:current_op_pos]:
            if not isinstance(o, LocalizeOperator):
                continue
            return o.get_timezone()

    @staticmethod
    def split_op(op):
        """Parses and splits into operator name and a list of its params"""
        op_parts = op.split(':')
        return op_parts[0].upper(), op_parts[1:] if len(op_parts) > 1 else None


def extract_years(dates):
    return np.array(list(map(lambda d: year([d])[0], dates)))


def extract_qrts(dates):
    def _qrt(dt):
        st = time.gmtime(dt / 1000.)
        return (st.tm_mon - 1) // 3 + 1

    return np.array(list(map(_qrt, dates)))


def extract_months(dates):
    return np.array(list(map(lambda d: month([d])[1], dates)))


def extract_weeks(dates):
    def _week(dt):
        dt = datetime.utcfromtimestamp(dt / 1000.)
        return dt.date().isocalendar()[1]
    return np.array(list(map(_week, dates)))


def extract_days(dates):
    def _day(dt):
        st = time.gmtime(dt / 1000.)
        return st.tm_mday
    return np.array(list(map(_day, dates)))


def extract_hours(dates):
    return (dates // 1000 // 60 // 60) % 24


def extract_minutes(dates):
    return (dates // 1000 // 60) % 60


def extract_seconds(dates):
    return (dates // 1000) % 60


def extract_week_days(dates):
    def _week_day(dt):
        st = time.gmtime(dt / 1000.)
        return st.tm_wday + 1
    return np.array(list(map(_week_day, dates)))


PERIODS_FILTERS = {
    'y': extract_years,
    'q': extract_qrts,
    'M': extract_months,
    'w': extract_weeks,
    'd': extract_days,
    'h': extract_hours,
    'm': extract_minutes,
    's': extract_seconds,
    'wd': extract_week_days
}


class FilterOperator(BaseOperator):
    POINTS_OPERATORS = [
            ['!=', '__ne__'],
            ['>=', '__ge__'],
            ['<=', '__le__'],
            ['=', '__eq__'],
            ['>', '__gt__'],
            ['<', '__lt__'],
    ]

    def __init__(self, *args, **kwargs):
        super(FilterOperator, self).__init__(*args, **kwargs)
        self.name, self.op_params = self.split_op(self.op)

    def __call__(self, points):
        if not len(points):
            return points
        if any(self.op_params[0].startswith(op[0]) for op in self.POINTS_OPERATORS):
            return self._filter_by_points(points)
        elif self.op_params[0].startswith('job'):
            return self._filter_by_job_id(points)
        elif self.op_params[0].startswith('ts'):
            return self._filter_by_ts(points)
        elif re.search('^[A-Z_]+$', self.op_params[0]):
            return self._filter_by_calendar(points)
        else:
            return self._filter_by_manual_ranges(points)

    def _filter_by_points(self, points):
        method_name, value = self._parse_points_filter_operator(self.op_params[0], float)
        method = getattr(points['values'], method_name)
        return points[method(value)]

    def _filter_by_manual_ranges(self, points):
        ranges, per = self._parse_filter_ranges()
        dates = per(points['dates'])

        flags = np.zeros(len(dates), dtype=bool)

        for r in ranges:
            if isinstance(r, tuple):
                f, t = r
                range_flags = (dates >= f)
                range_flags = (range_flags & (dates <= t))
                flags = (flags | range_flags)
            else:
                flags = (flags | (dates == r))

        return points[flags]

    def _filter_by_calendar(self, points):
        if not hasattr(self, 'fetch_calendar'):  # should not happen but possible due to bad implementations
            raise InvalidParametersException('filtering by calendar name is not supported')
        calendar_pts = self.fetch_calendar(self.op_params[0])
        return points[np.isin(points['dates'], calendar_pts['dates'])]

    def validate(self):
        if not self.op_params:
            raise InvalidParametersException('@filter operator requires an argument')

        if any(self.op_params[0].startswith(op[0]) for op in self.POINTS_OPERATORS):
            self._parse_points_filter_operator(self.op_params[0], float)
        elif self.op_params[0].startswith('job'):
            self._parse_points_filter_operator(self.op_params[0][len('job'):], int)
            self._assert_no_aggs_before_filter()
        elif self.op_params[0].startswith('ts'):
            self._parse_points_filter_operator(
                self.op_params[0][len('ts'):],
                lambda val: parse_date(val, 'iso')
            )
            self._assert_no_aggs_before_filter()
        elif not re.search('^[A-Z_]+$', self.op_params[0]):
            self._parse_filter_ranges()
        else:
            if not hasattr(self, 'fetch_calendar'):  # should not happen but possible due to bad implementations
                raise InvalidParametersException('filtering by calendar name is not supported')
            self.fetch_calendar(self.op_params[0])

    def _assert_no_aggs_before_filter(self):
        for o in self.operators_chain():
            if isinstance(o, AggOperator):
                raise InvalidParametersException(
                    'cannot use agg operators before @filter when filtering by job or timestamp'
                )
            if o is self:
                break

    def _filter_by_job_id(self, points):
        if 'jobs' not in points.dtype.names:
            raise InvalidParametersException('cannot filter on job because it is not available; include_job parameter must be used')

        param = self.op_params[0][len('job'):] # >10
        method_name, value = self._parse_points_filter_operator(param, int)
        method = getattr(points['jobs'], method_name)
        return points[method(value)]

    def _filter_by_ts(self, points):
        if 'ts' not in points.dtype.names:
            raise InvalidParametersException('cannot filter on timestamp because it is not available; include_timestamp parameter must be used')

        param = self.op_params[0][len('ts'):] # >10
        method_name, value = self._parse_points_filter_operator(param, lambda val: parse_date(val, 'iso'))
        method = getattr(points['ts'], method_name)
        return points[method(value)]

    def _parse_points_filter_operator(self, param, parse_val_func):
        for op, method_name in self.POINTS_OPERATORS:
            if param.startswith(op):
                value = parse_val_func(param[len(op):])
                return method_name, value

        raise InvalidParametersException('invalid @filter argument format')
    def _parse_filter_ranges(self):
        if not self.op_params:
            raise InvalidParametersException('@filter operator requires an argument')
        # extract period
        m = re.search('(\D+)$', self.op_params[0])
        if not m:
            raise InvalidParametersException('invalid @filter argument format')

        per = m.group(1)

        if per not in PERIODS_FILTERS:
            raise InvalidParametersException('"%s" is invalid @filter period' % per)

        ranges = list()
        for r in self.op_params[0][:-len(per)].split(','):
            if not r:
                raise InvalidParametersException('invalid @filter argument')
            try:
                if '-' in r:
                    ranges.append(tuple(map(int, r.split('-'))))
                else:
                    ranges.append(int(r))
            except ValueError:
                raise InvalidParametersException('invalid @filter argument')

        return ranges, PERIODS_FILTERS[per]

    @staticmethod
    def split_op(op):
        """Parses and splits into operator name and a list of its params"""
        op_parts = op.split(':', 1) # split by first ":" b/c of iso datetime
        return op_parts[0].upper(), op_parts[1:] if len(op_parts) > 1 else None

registry.register('FILTER',  FilterOperator)


class AggOperator(BaseOperator):
    PERIOD_BEGIN_SUFFIX = 'b'
    PERIOD_END_SUFFIX = 'e'

    def __init__(self, op, body, account_settings, operators_chain, is_forced=False):
        super(AggOperator, self).__init__(op, body, account_settings, operators_chain, is_forced=is_forced)
        self._parse_and_set_period()
        self._rel_delta = None
        self._rel_last_delta = None

    @property
    def length(self):
        # we already did validate so can be sure we can extract int from here
        rel = self.op_params[0]
        if not RELATIVE_DELTA_RE.search(rel):
            rel = '1{}'.format(rel)
        return int(RELATIVE_DELTA_RE.search(rel).groups()[1])

    def validate(self):
        if not self.op_params:
            raise InvalidParametersException('operator requires an argument')
        if not RELATIVE_DELTA_RE.search(self.op_params[0]) and self.op_params[0] not in PERIODS_MAP:
            raise InvalidParametersException(
                'the {} operator param must be in a relative delta format'.format(self.name))

        elif '-' in self.op_params[0] or '+' in self.op_params[0]:
            raise InvalidParametersException('negative and positive signs are not allowed')

        if ' ' in self.op_params[0]:
            raise InvalidParametersException(
                'multiple relative deltas are not supported in the {} operator'.format(self.name))

        if len(self.op_params) > 1 and not RELATIVE_DELTA_RE.search(self.op_params[1]) and self.op_params[1] not in PERIODS_MAP:
            raise InvalidParametersException(
                'the {} operator final point period param must be in a relative delta format'.format(self.name))

        if not AGG_BUCKET_LENGTH_VALIDATORS[self.period](self.length):
            raise InvalidParametersException(
                'aggregation to {} is not supported'.format(self.length))

    @property
    def period(self):
        return self.op_params[0][-1]

    def __call__(self, points):
        points = points[~np.isnan(points['values'])]
        if not len(points):
            return points
        data = list()
        grouper_class = AGG_PERIODS[self.period] if not self.period_end else AGG_END_PERIODS[self.period]
        grouper = grouper_class(self.length)

        for d, iter_ in groupby(points, key=grouper):
            vals = np.array(list(iter_))['values']
            agg = self.agg_func(vals)
            data.append((d, agg))
        return np.array(data, dtype=AGG_DTYPE)

    @property
    def relative_delta(self):
        if self._rel_delta is not None:  # check if cached
            return self._rel_delta
        rel = self.op_params[0]
        if not RELATIVE_DELTA_RE.search(rel):
            rel = '1{}'.format(rel)
        self._rel_delta = _parse_relative_delta(rel)
        return self._rel_delta

    @property
    def final_point_period(self):
        if len(self.op_params) < 2:
            return None

        if self._rel_last_delta is not None:  # check if cached
            return self._rel_last_delta

        rel = self.op_params[1]
        if not RELATIVE_DELTA_RE.search(rel):
            rel = '1{}'.format(rel)

        self._rel_last_delta = _parse_relative_delta(rel)
        return self._rel_last_delta

    @property
    def agg_func(self):
        return AGG_OP_FUNCS[self.name.upper()]

    def _parse_and_set_period(self):
        if not self.op_params:
            return
        full_period_name = self.op_params[0]  # e.g:  1Me, Mb, 6M
        maybe_end_or_begin_suffix = full_period_name[-1] if full_period_name else None

        if maybe_end_or_begin_suffix in [self.PERIOD_END_SUFFIX, self.PERIOD_BEGIN_SUFFIX]:
            self.period_end = maybe_end_or_begin_suffix == self.PERIOD_END_SUFFIX
            self.op_params[0] = full_period_name[:-1]  # removing suffix 1Me -> 1M
        else:
            aggs_default_period_end = self.account_settings.get('aggs_default_e') or []
            self.period_end = maybe_end_or_begin_suffix in aggs_default_period_end


for a in AGG_OP_FUNCS:
    registry.register(a, AggOperator)


DATE_ZEROS = 1900, 1, 1, 0, 0, 0

AGG_DTYPE = np.dtype([('dates', 'i8'), ('values', 'f8')])


class DeprecatedAggOperator(BaseOperator):

    def __call__(self, points):
        if not len(points):
            return points
        data = list()
        for milli, iter_ in groupby(points, key=AGG_PERIODS_DEPRECATED[self.period]):
            vals = np.array(list(iter_))['values']
            agg = self.agg_func(vals)
            data.append((milli, agg))
        return np.array(data, dtype=AGG_DTYPE)


for period, agg in itertools.product(AGG_PERIODS_DEPRECATED, AGG_OP_FUNCS):
    registry.register('{}{}'.format(period, agg), DeprecatedAggOperator)

Bucket = namedtuple('Bucket', 'dt milli values dates points')


class WeightedAverageOperator(AggOperator):

    def __call__(self, points):
        if not len(points):
            return points
        # period `being` and `ending` moved to separate functions w/o
        # extra abstractions for efficiency
        backward = points[0][0] > points[-1][0]

        if not self.period_end:
            res = self._weighted_avg_period_begin(points, backward)
        else:
            res = self._weighted_avg_period_end(points, backward)

        if backward:
            res = list(reversed(res))
        return np.array(res, dtype=AGG_DTYPE)

    def _weighted_avg_period_begin(self, points, backward):
        prev_bucket_dt = None
        prev_bucket_value = None

        result = list()
        grouper = AGG_PERIODS[self.period]
        if backward:
            points = reversed(points)

        processed_points = 0
        for bckt in self._iter_buckets(points, grouper):
            weights = list()

            processed_points += len(bckt.points)
            if self.final_point_period and processed_points == len(points):
                last_point_dt = datetime.utcfromtimestamp(bckt.dates[-1]/1000)
                effective_till_dt = bckt.dt + self.final_point_period

                while last_point_dt >= effective_till_dt:
                    effective_till_dt += self.final_point_period

            else:
                effective_till_dt = bckt.dt + self.relative_delta

            effective_till_milli = calendar.timegm(effective_till_dt.timetuple()) * 1000

            if prev_bucket_dt is not None \
                    and bckt.dt - self.relative_delta <= prev_bucket_dt:
                # points from prev bucket must be weighted
                bckt.dates.insert(0, bckt.milli)
                bckt.values.insert(0, prev_bucket_value)

            for d1, d2 in zip(bckt.dates, bckt.dates[1:] + [effective_till_milli]):
                weights.append(d2 - d1)
            result.append((bckt.milli, self._weight_values(bckt, weights)))
            prev_bucket_value = bckt.values[-1]
            prev_bucket_dt = bckt.dt
        return result

    def _weighted_avg_period_end(self, points, backward):
        result = list()
        prev_bucket_dt = None
        prev_bucket_value = None
        grouper = AGG_END_PERIODS[self.period]

        # when aggregating to period end for daily and above we use period-begin as a bucket
        bucketize_to_begin = self.agg_level > AGGREGATE_LEVELS['h']
        if not backward:
            points = reversed(points)

        for bckt in self._iter_buckets(points, grouper):
            weights = list()

            effective_since = bckt.dt if bucketize_to_begin else bckt.dt - self.relative_delta
            effective_since_milli = calendar.timegm(effective_since.timetuple()) * 1000
            effective_till_dt = bckt.dt + self.relative_delta if bucketize_to_begin else bckt.dt
            effective_till_milli = calendar.timegm(effective_till_dt.timetuple()) * 1000

            if prev_bucket_dt is not None and effective_till_dt + self.relative_delta >= prev_bucket_dt:
                # points from prev bucket must be weighted
                bckt.dates.insert(0, effective_till_milli)
                bckt.values.insert(0, prev_bucket_value)

            for d1, d2 in zip(bckt.dates, bckt.dates[1:] + [effective_since_milli]):
                weights.append(d1 - d2)

            # pts are reversed, so we insert, not appending
            result.append((bckt.milli, self._weight_values(bckt, weights)))
            prev_bucket_value = bckt.values[-1]
            prev_bucket_dt = bckt.dt

        return list(reversed(result))

    @staticmethod
    def _weight_values(bucket, weights):
        if len(weights) > 1:
            return np.average(np.array(bucket.values), weights=weights)
        else:
            return bucket.points[0][1]

    def _iter_buckets(self, points, grouper):
        actual_length = self.length // 2  # we making it look x2 wider for adjust_dates()
        for milli, iter_ in groupby(points, grouper(actual_length)):
            bucket_pts = list(iter_)
            values = [p[1] for p in bucket_pts]
            dates = [p[0] for p in bucket_pts]

            dt = datetime.utcfromtimestamp(milli/1000)
            yield Bucket(dt, milli, values, dates, bucket_pts)

    @property
    def length(self):
        # we need to pull points from neighbour buckets b/c they may affect period we aggregating
        # so for adjust_date() function we make this look wider
        return super(WeightedAverageOperator, self).length * 2


class DeprecatedWeightedAverageOperator(WeightedAverageOperator):
    FROM_DEPRECATED_MAPPING = {
        'YWA': 'WGA:1y',
        'QWA': 'WGA:1q',
    }

    def __init__(self, op, body, account_settings, operators_chain, is_forced=False):
        op = self.FROM_DEPRECATED_MAPPING[op]
        super(DeprecatedWeightedAverageOperator, self).__init__(
            op, body, account_settings, operators_chain, is_forced=is_forced,
        )


for op_name in ['YWA', 'QWA']:
    registry.register(op_name, DeprecatedWeightedAverageOperator)

registry.register('WGA', WeightedAverageOperator)

class ForceTzOperator(BaseOperator):
    """
    This is flag-like operator. It does nothing, but contains info about timezone.
    """
    def __call__(self, points):
        return points

    @property
    def agg_func(self):
        return None

    @property
    def agg_level(self):
        return

    def adjust_date(self, df, dt):
        return df, dt

    def get_timezone(self):
        try:
            tz = pytz.timezone(self.op_params[-1])
        except UnknownTimeZoneError:
            raise InvalidParametersException('{} is unknown timezone'.format(self.op_params[-1]))
        return tz

class LocalizeOperator(BaseOperator):
    def __call__(self, points):
        timezone = self.get_timezone()
        if timezone is None:  # autonofail (does nothing)
            return points
        convered = pd.DatetimeIndex(points['dates'] * NANOSECONDS).tz_localize(pytz.utc).tz_convert(timezone).tz_localize(None)
        points['dates'] = convered.values.astype(int) // NANOSECONDS   # back to milli
        return points

    # @memoize  # todo
    def get_timezone(self):
        series_body = self.body or dict()
        check_timezone_field = False
        if self.op_params and self.op_params[-1].upper() not in ['AUTOFAIL', 'AUTONOFAIL']:
            tz = self.op_params[-1]
        else:
            if self.op_params:
                check_timezone_field = self.op_params[-1].upper() == 'AUTOFAIL'

            if not self.account_settings \
                    or not self.account_settings.get('localize_default_field') \
                            and check_timezone_field:
                raise InvalidParametersException('account timezone field is not set')
            tz = series_body.get('fields', {}).get(self.account_settings.get('localize_default_field', 'timezone'))
        if tz is None and check_timezone_field:
            raise InvalidParametersException('localize operator can not be used with out timezone')
        elif tz is None and not check_timezone_field:
            return tz
        try:
            tz = pytz.timezone(tz)
        except UnknownTimeZoneError:
            raise InvalidParametersException('{} is unknown timezone'.format(tz))
        return tz

    def validate(self):
        self.get_timezone()  # raises exception if invalid timezone

    @property
    def agg_func(self):
        return None

    @property
    def agg_level(self):
        return

    def adjust_date(self, df, dt):
        tz = self.get_timezone()
        if tz is None:
            return df, dt
        if df <= dt:
            df -= 24 * 3600 * 1000
            dt += 24 * 3600 * 1000
            df = max(df, MIN_PYTHON_DATE + 24 * 3600 * 1000)
            dt = min(dt, MAX_PYTHON_DATE - 24 * 3600 * 1000)
        else:
            df += 24 * 3600 * 1000
            dt -= 24 * 3600 * 1000
            df = min(df, MAX_PYTHON_DATE - 24 * 3600 * 1000)
            dt = max(dt, MIN_PYTHON_DATE + 24 * 3600 * 1000)

        return df, dt


registry.register('LOCALIZE', LocalizeOperator)
registry.register('FORCETZ', ForceTzOperator)

TRAILING_OPERATORS = set()   # just a names


class BaseTrailingOperator(BaseOperator):

    def __call__(self, points):
        if not len(points):
            return points
        time_delta = self.relative_delta_non_sub_one
        reverse = points[0][0] > points[-1][0]
        res = []
        for p in sorted(points, key=lambda pt: pt[0], reverse=True):
            date, val = p['dates'], p['values']
            for i in xrange(len(res) - 1, -1, -1):
                if date > res[i]['min']:
                    res[i]['vals'].append(val)
                else:
                    break
            res.append({'dt': date, 'min': add_delta(date, 'milli', time_delta), 'vals': [val]})
        res = [(r['dt'], self.agg_func(r['vals'])) for r in res]
        res = res if reverse else list(reversed(res))  # because it's reversed by default now
        return np.array(res, dtype=AGG_DTYPE)


class TrailingOperator(BaseTrailingOperator):

    @property
    def agg_func(self):
        return AGG_OP_FUNCS[self.op_params[0].upper()]

    @property
    def relative_delta(self):
        rel = self.op_params[-1]
        if not RELATIVE_DELTA_RE.search(rel):
            rel = '1{}'.format(rel)
        m = RELATIVE_DELTA_RE.search(rel)
        length, period = m.group(2), m.group(3)
        return _parse_relative_delta('{}{}'.format(int(length) - 1, period))

    @property
    def relative_delta_non_sub_one(self):
        rel = self.op_params[-1]
        if not RELATIVE_DELTA_RE.search(rel):
            rel = '1{}'.format(rel)

        return _parse_relative_delta(rel)

    def adjust_date(self, df, dt):
        delta = self.relative_delta
        if df <= dt:
            df = add_delta(df, 'milli', delta)
        else:
            dt = add_delta(dt, 'milli', delta)
        return df, dt


class OldTrailingOperator(BaseTrailingOperator):

    @property
    def relative_delta(self):
        delta = get_delta_from_time_str(get_trailing_or_auto_agg_param(self.op), True)
        return delta

    @property
    def relative_delta_non_sub_one(self):
        delta = get_delta_from_time_str(get_trailing_or_auto_agg_param(self.op))
        return delta

    def adjust_date(self, df, dt):
        delta = self.relative_delta
        if df <= dt:
            df = add_delta(df, 'milli', delta)
        else:
            dt = add_delta(dt, 'milli', delta)
        return df, dt


registry.register('T', TrailingOperator)
TRAILING_OPERATORS.add('T')

for agg in ['S', 'A', 'C', 'E', 'B']:
    name = 'T{}'.format(agg)
    registry.register(name, OldTrailingOperator)
    TRAILING_OPERATORS.add(name)


class LagOperator(BaseOperator):

    def __call__(self, points):
        dtype = [(c, COLUMN_TYPES[c]) for c in points.dtype.names]
        tz = self.get_timezone()

        s = milli_tuple_naive_to_pandas_tz_aware(list(zip(points['dates'], points['values'])), tz=tz)
        s.index = pd.DatetimeIndex(s.index.to_pydatetime() + self.relative_delta)
        dates = s.index.tz_localize(None).astype(np.int64) / 1000000
        res = np.zeros(len(points), dtype=dtype)
        res['dates'] = dates
        res['values'] = s.values
        if 'jobs' in points.dtype.names:
            res['jobs'] = points['jobs']

        if 'ts' in points.dtype.names:
            res['ts'] = points['ts']

        return res

    @property
    def length(self):
        return 0

    @property
    def relative_delta(self):
        return _parse_relative_delta(self.op_params[0])

    @property
    def agg_level(self):
        return

    @property
    def agg_func(self):
        return None

    def validate(self):
        if not self.op_params:
            raise InvalidParametersException('operator requires an argument')
        if not all(RELATIVE_DELTA_RE.search(o) for o in self.op_params[0].split()):
            raise InvalidParametersException(
                'the {} operator param must be in a relative delta format'.format(self.name))

    def adjust_date(self, df, dt):
        delta = self.relative_delta
        if self._is_negative_rel(delta):
            delta = delta * -1
        if df <= dt:
            df = add_delta(df, 'milli', delta, fallback=MIN_PANDAS_DATE)
            dt = add_delta(dt, 'milli', delta * -1, fallback=MAX_PANDAS_DATE)
        else:
            df = add_delta(df, 'milli', delta * -1, fallback=MAX_PANDAS_DATE)
            dt = add_delta(dt, 'milli', delta, fallback=MIN_PANDAS_DATE)
        return df, dt

    @staticmethod
    def _is_negative_rel(rd):
        ''' Check whether a relativedelta object is negative'''
        try:
            datetime.min + rd
            return False
        except (OverflowError, ValueError):
            return True


registry.register('LAG', LagOperator)


class DummyOperator(BaseOperator):
    def __call__(self, *args, **kwargs):
        return

    @property
    def period(self):
        return 'A'


class NoOp(BaseOperator):
    """
    These are special operators and just being ignored
    by the general operators logic
    """

    def __call__(self, points):
        return points

    def adjust_date(self, df, dt):
        return df, dt

    @property
    def agg_level(self):
        return None


# these are handled separately
registry.register('EXCL', NoOp)
registry.register('PRE', NoOp)
registry.register('POST', NoOp)
registry.register('CACHED', NoOp)
registry.register('DF', NoOp)
registry.register('DT', NoOp)
registry.register('MAX_POINTS', NoOp)
registry.register('CAL', NoOp)
registry.register('ASOF', NoOp)
registry.register('REPDATE', NoOp)
registry.register('REPFDATE', NoOp)
registry.register('REPPDATE', NoOp)

# this mapping is for auto aggs
for agg in AGG_OP_FUNCS:
    registry.register('A{}'.format(agg), DummyOperator)


def validate_suffix_operators(operators):

    if len({o for o in operators if o.startswith('localize')}) > 1:
        raise InvalidParametersException('can not localize more than once')

    for operator in operators:
        op = operator.upper().split(':')[0]
        if (not registry.is_registered(op)
                and not get_trailing_or_auto_agg_param(op)
                and not (op.startswith('A') and op[1] in AGGREGATE_LEVELS)
                and not op.startswith('LOCALIZE')
                and not op.startswith('ASOF')
                and not op.startswith('CAL')
                and not op.startswith('REPDATE')
                and not op.startswith('REPPDATE')
                and not op.startswith('REPFDATE')
                and not op.startswith('MAX_POINTS')
                and not op.startswith('DF')
                and not op.startswith('DT')
                and not op.startswith('FORCETZ')):
            raise InvalidParametersException('operator %s is not supported' % op)


def get_operator_aggregate_level(operators):
    try:
        operators = _operator_to_function_if_needed(operators)
    except InvalidParametersException:
        return 0, 1
    if len(operators) == 1 and operators[0].name in TRAILING_OPERATORS:
        return 1, 1  # special for trailing
    agg_level, step = 0, 1
    sorted_by_milli_length = sorted((op for op in operators if op.name.upper() in registry.registered_operators),
                                    key=lambda o: o.length_milli if o.length_milli is not None else float('-inf'),
                                    reverse=True)

    if not sorted_by_milli_length:
        return agg_level, step
    wider_op = sorted_by_milli_length[0]
    return wider_op.agg_level, wider_op.length


def get_trailing_or_auto_agg_param(operator):
    if operator and operator[0] in ['T', 'A'] and len(operator) >= 4 and operator[2] == ':':
        return operator[3:]


def adjust_dates_for_suffix(operators, df, dt):
    operators = _operator_to_function_if_needed(operators)  # TODO remove it with old api
    if not operators:
        return df, dt

    if len(operators) == 1 and operators[0].name in TRAILING_OPERATORS:
        return operators[0].adjust_date(df, dt)
    elif len(operators) == 1 and operators[0].name == 'LAG':
        return operators[0].adjust_date(df, dt)

    for op in operators:
        df, dt = op.adjust_date(df, dt)
    return df, dt


def reorder_operators(operators):
    """
    Moves @localize into proper position so that we
    apply @localize AFTER any daily (or higher) aggregation.
    """
    # we should put @localize before first daily or greater agg h m d local
    localize_operators = [o for o in operators if isinstance(o, LocalizeOperator)]
    non_localize_operators = [o for o in operators if not isinstance(o, LocalizeOperator)]

    # we leave order as is if @filter or @lag used. Also if there is no @localize.
    if not localize_operators \
            or any(isinstance(o, (FilterOperator, LagOperator)) for o in non_localize_operators):
        return operators

    operators = non_localize_operators
    new_localize_idx = 0
    for idx, o in enumerate(operators):
        if not isinstance(o, AggOperator):
            continue

        if o.agg_level > AGGREGATE_LEVELS['h']:
            break
        new_localize_idx = idx + 1
    if localize_operators:  # we made sure we don't localize to different timezones. so can take only first operator.
        operators.insert(new_localize_idx, localize_operators[0])

    return operators


def apply_suffix_operators(operators, points, freq=None):
    """

    Applies a list of suffixes to the points. Operators are applied in a first in first applied order.

    :param operators: list of operators
    :param points: list of points
    :param date_format: format of date in points
    :param freq: determined freq of data
    :return: list of points
    """
    operators = _operator_to_function_if_needed(operators)  # TODO remove it with old api
    operators = reorder_operators(operators)

    for op in operators:
        if isinstance(op, NoOp):
            continue

        if op.agg_level and op.agg_level > 1 and freq in AGGREGATE_LEVELS and op.length == 1:
            op_agg = op.name[0]
            if op_agg != 'W' and op.agg_level <= AGGREGATE_LEVELS[freq]:
                if op.agg_func is AGG_OP_FUNCS['C']:  # a bit dirty maybe. this is count agg
                    points['values'] = 1.
                continue  # nothing to do
        try:
            points = op(points)
        except Exception as e:
            # just changing error text a bit to let user know which operator exactly caused this
            raise type(e)(u'{} operator caused unexpected error: {}'.format(op, e))
    return points


def _operator_to_function_if_needed(operators):  # TODO remove it with old api
    if not operators:
        return []
    if operators and isinstance(operators[0], six.string_types):
        return operators_to_functions(operators)
    return operators or []


class OperatorsChain(collections.UserList):
    """Essentially an ordered list of operators"""

    def append(self, item: BaseOperator):
        """Add operator to the chain"""
        super().append(item)

    def get_by_name(self, name: str) -> typing.Union[typing.List[BaseOperator], None]:
        """Return operator by its name"""
        res = list()
        for op in self:
            if op.name.lower() == name.lower():
                res.append(op)
        return res

def operators_to_functions(operators, body=None, account_settings=None):
    """
    Generates list of operators functions from it's names and series body
    :param operators: List of operators
    :param body: Series body
    :param account_settings: Account settings that may affect operators behavior
    :return:
    """

    chain = OperatorsChain()
    for op in operators:
        op_parts = op.split(':')
        op_name = op_parts[0].upper()
        is_forced = op_name.endswith('!')
        op_name = op_name.rstrip('!')
        if len(op_parts) > 1 and op_name == 'WA':
            op_name = 'WGA'  # weighted average. conflicting with old WA
        op_class = registry.get_by_name(op_name)
        operator = op_class(
            op,
            body=body,
            account_settings=account_settings,
            operators_chain=chain,
            is_forced=is_forced,
        )
        operator.validate()
        chain.append(operator)
    return chain


def determine_auto_agg(operators, points, body=None, account_settings=None):
    """
    Generates list of operators functions, determines auto agg by points array.
    """
    if not operators:
        return OperatorsChain(), None
    operators = _operator_to_function_if_needed(operators) # TODO remove it with old api
    auto_agg = None
    new_operators = OperatorsChain()
    for op in operators:
        operator = op.name.upper()
        if isinstance(op, DummyOperator):
            max_points = DEFAULT_AUTO_AGG_SIZE
            if op.op_params:
                try:
                    max_points = int(op.op_params[0])
                except (TypeError, ValueError):
                    raise InvalidParametersException('auto aggregation operator parameter must be an integer')
            number_of_points = max_points
            if len(points) <= number_of_points:
                auto_agg = NO_AGG_NEEDED
                continue
            operator = '{}{}'.format(_determine_agg_level(points, number_of_points), operator.split(':')[0][1:])
            auto_agg = operator.upper()
            new_operators.extend(operators_to_functions([operator]))
        else:
            new_operators.append(op)
    return new_operators, auto_agg


def _determine_agg_level(points, number_of_points):
    """
    Determines best aggregation level to fit maximum <number_of_points>
    """

    if not len(points):
        return AGG_LEVELS_MILLIS_LENGTH[-1][0]  # highest frequency
    s = points['dates'].min()
    f = points['dates'].max()
    m = abs(f - s) // number_of_points
    ranges = [r for r in AGG_LEVELS_MILLIS_LENGTH if r[1] >= m]
    if not ranges:
        return AGG_LEVELS_MILLIS_LENGTH[0][0]  # lowest frequency
    else:
        return ranges[-1][0]


def adjust_dates(date_from, date_to, operators, date_format):
    """
    Function takes a date from and a date to in any supported format, coverts it to milliseconds
    and adjusts the dates if needed for the operators to    be applied.

    :param str, float, int date_from: date from
    :param str, float, int date_to: date to
    :param list operators: list of operators
    :param str date_format: format of dates passed to the function
    :return: a tuple of date from and date to adjusted using the operators and converted to milli
    :rtype : tuple
    """
    date_from = parse_date(date_from, date_format)
    date_to = parse_date(date_to, date_format)
    if not operators:
        return date_from, date_to
    return adjust_dates_for_suffix(operators, date_from, date_to)


def sample_points(size, points):
    if not size or not len(points):
        return points
    chunk_len = int(math.ceil(len(points) / float(size)))
    sample = list()
    first_pt = points[0]
    last_pt = points[-1]
    split_idx = [(i + 1) * chunk_len for i in xrange(size)]

    dtype = [(c, COLUMN_TYPES[c]) for c in points.dtype.names]
    if len(points) <= size:
        return points
    for chunk in np.split(points, split_idx):
        if not len(chunk):
            continue
        vals_chunk = chunk['values']
        min_ix = vals_chunk.argmin()
        max_ix = vals_chunk.argmax()
        min_pt = chunk[min_ix]
        max_pt = chunk[max_ix]
        min_dt = min_pt[0]
        max_dt = max_pt[0]

        if min_dt < max_dt:
            sample.append(min_pt)
            sample.append(max_pt)
        elif min_dt > max_dt:
            sample.append(max_pt)
            sample.append(min_pt)
        else:
            sample.append(min_pt)
    # add first and last points if nee
    if first_pt[0] != sample[0][0]:
        sample.insert(0, first_pt)
    if last_pt[0] != sample[-1][0]:
        sample.append(last_pt)
    return np.array(sample, dtype=dtype)


def render_points(points, date_format, include_timestamp=False):
    """
    Renders points dates to <date_format>
    """
    if date_format == 'milli' or not len(points):
        return points
    desc_by_name = dict(points.dtype.fields)
    names = points.dtype.names

    object_dtype =[(n, desc_by_name[n][0]) if n not in ['ts', 'dates'] else (n, object) for n in names]
    points = np.array(points, dtype=object_dtype)
    _render_date = np.vectorize(lambda x: render_date(x, date_format))
    points['dates'] = _render_date(points['dates'])
    if include_timestamp and 'ts' in points.dtype.names:
        points['ts'] = _render_date(points['ts'])
    return points


def post_process_points(points, operators, date_format, include_timestamp=False,
        include_job=False, df='MIN', dt='MAX', freq=None, sample_points_size=None):
    """
    Function post processes the points, applying operators, and converting dates into the
    final data format

    :param PointsGenerator points: list of points (represented as a list)
    :param list operators: list of operators
    :param str date_format: date format of final output
    :param include_timestamp: flag indicating if points timestamp should be included
    :param include_job: flag indicating if job_id should be included
    :param df: minimum date to pull points
    :param dt: maximum date to pull points
    :param freq: determined freq of points
    :return: list of processed points (points are list where first element is the date, second element is the value) and dict with stats
    :rtype: tuple
    """
    stats = {}
    if not len(points):
        return EMPTY_POINTS, stats

    # calculate range bounds
    df_adj, dt_adj = adjust_dates(df, dt, operators, date_format)
    df_orig, dt_orig = adjust_dates(df, dt, [], date_format)

    min_date = min(df_adj, dt_adj, df_orig, dt_orig)
    max_date = max(df_adj, dt_adj, df_orig, dt_orig)
    points = points[(min_date <= points['dates']) & (points['dates'] <= max_date)]  # repeats below

    if operators:
        points = apply_suffix_operators(operators, points, freq=freq)

    higher = max(df_orig, dt_orig)  # this is filter after operators
    lower = min(df_orig, dt_orig)

    if df_orig > dt_orig and len(points) and points[0][0] < points[-1][0]:
        points = points[points['dates'].argsort(axis=0)[::-1]]
    elif df_orig < dt_orig and len(points) and points[0][0] > points[-1][0]:  # got desc but need asc
        points = points[points['dates'].argsort(axis=0)]

    points = points[(lower <= points['dates']) & (points['dates'] <= higher)]

    if sample_points_size:
        original_size = len(points)
        points = sample_points(sample_points_size, points)
        if len(points) < original_size:
            stats['has_sampled_points'] = True

    if date_format != 'milli' and len(points):  # have to change array type in this case
        points = render_points(points, date_format, include_timestamp=include_timestamp)
    return points, stats


def agg_by_field(data, agg_field, agg_type):
    """
    Calculates @AGG aggregation on a given series data, field and aggregation type.

    :param data: Array of series objects
    :param agg_field: Field name to aggregate on
    :param agg_type: Aggregation type
    """
    fields_by_field = defaultdict(list)
    points_by_field = defaultdict(list)

    numify = lambda name, value: float(value) if name.endswith('_num') else value

    dtype = None
    for s in data:
        val = s.get('fields', {}).get(agg_field)
        if val is None:
            continue
        if not isinstance(val, list):
            val = [val]
        for v in val:
            v = numify(agg_field, v)
            s_fields = [(f_name, numify(f_name, f_val)) for f_name, f_val in six.iteritems(s.get('fields', {}))]
            fields_by_field[v].extend(s_fields)
            pts = s.get('points')
            if pts is not None:
                points_by_field[v].extend(pts)

                if dtype is None:  # will take dtype of first pts array
                    if hasattr(pts, 'dtype'):
                        dtype = pts.dtype

    op = AGG_OP_FUNCS[agg_type]
    if dtype is None:
        dtype = [(c, COLUMN_TYPES[c]) for c in ['dates', 'values']]

    for agg_field_val in fields_by_field.keys():
        aggregated_fields = dict()
        aggregated_points = list()
        for f, f_group in itertools.groupby(sorted(fields_by_field[agg_field_val], key=lambda x: x[0]),
                                            key=lambda x: x[0]):
            do_field_agg = f != agg_field and f.endswith('_num')
            values = [x[1] for x in f_group]
            if not do_field_agg and field_type(f, ) not in ['obj', 'geo']:  # these are not sortable
                values = sorted(set(itertools.chain(*[[v] if not isinstance(v, list) else v for v in values])))

            if len(values) == 1 and not do_field_agg:
                aggregated_fields[f] = values[0]
            elif do_field_agg:
                aggregated_fields[f] = op(values)
            else:
                aggregated_fields[f] = None

        for d, p_group in itertools.groupby(sorted(points_by_field[agg_field_val], key=lambda x: x[0]),
                                            key=lambda x: x[0]):
            values = [x[1] for x in p_group]
            aggregated_points.append((d, op(values)))
        ser = {'series_id': agg_field_val,
               'fields': aggregated_fields}
        if aggregated_points:
            ser['points'] = np.array(aggregated_points, dtype=dtype)

        yield ser
