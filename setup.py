from setuptools import setup, find_packages

setup(
    name="sjtoolkit",
    version="20250106",
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['assets/user_agents.txt']},
)
